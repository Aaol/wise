import {Role} from '../enums/role.enum';

export class User {
  username: string;
  jwt: string;
}
