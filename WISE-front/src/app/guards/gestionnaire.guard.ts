import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {JwtService} from '../services/jwt.service';
import {Role} from '../enums/role.enum';


@Injectable({ providedIn: 'root' })
export class GestionnaireGuard implements CanActivate {
  constructor(
    private router: Router,
    private jwtService: JwtService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.jwtService.getJwt().role === Role.GESTINNAIRE ) {
      // logged in so return true
      return true;
    }
    //
    //   // not logged in so redirect to login page with the return url
    //   this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }
}
