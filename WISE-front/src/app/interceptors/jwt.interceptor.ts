import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import {JwtService} from '../services/jwt.service';


@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private jwtService: JwtService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    /* Ajoute le jwt a la requète si il y en a un */
    if (this.jwtService.getJwtRaw() != null ) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.jwtService.getJwtRaw()}`
        }
      });
    }

    return next.handle(request);
  }
}
