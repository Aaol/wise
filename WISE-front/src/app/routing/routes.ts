import {Routes} from '@angular/router';
import {AppComponent} from '../app.component';
import {AdminGuard} from '../guards/admin.guard';
import {GestionnaireGuard} from '../guards/gestionnaire.guard';

export const routes: Routes = [
  { path: 'admin', component: AppComponent, canActivate: [AdminGuard] },
  { path: 'gestionnaire', component: AppComponent, canActivate: [GestionnaireGuard] }
]
