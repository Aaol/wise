import { Component } from '@angular/core';
import {JwtService} from './services/jwt.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'UXPERIMENT';

  constructor(jwtService: JwtService) {
    console.log(jwtService.getJwt().role);
  }

}
