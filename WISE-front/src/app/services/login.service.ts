import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Credentials} from '../models/credentials';
import {environment} from '../../environments/environment';
import {JwtService} from './jwt.service';
import {NotifierService} from 'angular-notifier';
import {User} from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  ENDPOINT_URL = environment.apiEndpoint + '/login';

  constructor(private http: HttpClient, private jwtService: JwtService, private notifier: NotifierService) {
  }

  async login(credentials: Credentials) {
    await this.http.post(this.ENDPOINT_URL, credentials).toPromise().then((res: User) => {
      this.jwtService.setJwt(res.jwt);
    }).catch(e => {
      console.log(e);
      this.notifier.notify('error', 'Une erreur est survenue');
    });
  }

  async logout() {
    await this.jwtService.removeJwt();
  }
}
