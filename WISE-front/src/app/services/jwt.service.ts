import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Jwtpayload} from '../models/jwtpayload';

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  jwtHelperService = new JwtHelperService();

  constructor() {
  }

  getJwtRaw(): string {
    return localStorage.getItem('jwt');
  }

  getJwt(): Jwtpayload {
    return this.jwtHelperService.decodeToken(localStorage.getItem('jwt'));
  }

  setJwt(jwt: string) {
    localStorage.setItem('jwt', jwt);
  }

  removeJwt() {
    localStorage.removeItem('jwt');
  }

  isTokenExpired(): boolean {
    return this.jwtHelperService.isTokenExpired(localStorage.getItem('jwt'));
  }

}
