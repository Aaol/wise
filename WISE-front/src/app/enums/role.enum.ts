export enum Role {
  ADMIN = 'Administrateur',
  GESTINNAIRE = 'Gestionnaire',
  PUBLIC = 'Public',
  PARTENAIRE = 'Partenaire',
}
