# UXPERIMENT

## UXPERIMENT FRONT

*Ce projet à été généré avec  [Angular CLI](https://github.com/angular/angular-cli) version 7.2.2.*

### Prérequis

- Installer [NodeJs LTS](https://nodejs.org/fr/)
- Avec le terminal NodeJS, lancer `npm install npm@latest -g`

### Installation du projet

Avec le terminal NodeJS, aller dans le dossier WISE-front et lancer `npm install`

### Lancement du client en local

Lancer `npm run start` et aller sur `http://localhost:4200/`

## UXPERIMENT BACK

### Prérequis

- Installer [Symfony Server](https://symfony.com/download).
- Avoir une version de PHP (7.2 minimum) renseignée en variable d'environnement.
- Installer [Composer](https://getcomposer.org/download/).

### Installation du projet 

Avec le terminal, lancer `composer install` dans le path du dossier projet.

### Lancement du projet

Lancer `symfony server:start`