<?php

namespace App\Controller;

use App\Entity\Offers;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class OffersController extends AbstractController
{
    /**
     * @Route("/offers", methods={"GET"})
     */
    public function getOffers()
    {
        $em = $this->getDoctrine()->getManager();

        $offersRepository = $em->getRepository('App:Offers');
      $offers = $offersRepository->findAll();

      $jsonResponse = [];

      foreach($offers as $offer) {
          $offerArray = [
              'id' => $offer->getId(),
              'nom' => $offer->getNom()
          ];

          array_push($jsonResponse,$offerArray);
      }

      return new Response(json_encode($jsonResponse));
    }

    /**
     * @Route("/offer/{id}", methods={"GET"})
     */
    public function getOfferById($id)
    {
        $em = $this->getDoctrine()->getManager();

        $offersRepository = $em->getRepository('App:Offers');
        $offer = $offersRepository->findOneById($id);

            $offerArray = [
                'id' => $offer->getId(),
                'nom' => $offer->getNom()
            ];


        return new Response(json_encode($offerArray));
    }

    /**
     * @Route("/offer", methods={"POST"})
     */
    public function addOffer(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $offer = new Offers();
        $offer->setNom($request->request->get('nom'));

        $entityManager->persist($offer);
        $entityManager->flush();

        return new Response('Saved : ' . $offer->getNom() . ' with id ' . $offer->getId());
    }

    /**
     * @Route("/offer/{id}", methods={"DELETE"})
     */
    public function deleteOffer($id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $offerRepository = $entityManager->getRepository('App:Offers');
        $offer = $offerRepository->findOneById($id);

        $entityManager->remove($offer);
        $entityManager->flush();


        return new Response('Removed offer with id ' . $id);
    }


    /**
     * @Route("/offer/{id}", methods={"PUT"})
     */
    public function updateOffer($id, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $offerRepository = $entityManager->getRepository('App:Offers');
        $offer = $offerRepository->findOneById($id);

        if($request->request->get('nom') !== null){
            $offer->setNom($request->request->get('nom'));
        }

        $entityManager->persist($offer);
        $entityManager->flush();


        return new Response('Updated offer with id ' . $id);
    }

}
