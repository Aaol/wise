<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints\Email as EmailConstraint;

class UserController extends AbstractController
{
    /**
     * @Route("/users", methods={"GET"})
     */
    public function getUsers()
    {
        $entityManager = $this->getDoctrine()->getManager();
    
        $userRepository = $entityManager->getRepository('App:User');
        $users = $userRepository->findAll();

        $jsonResponse = [];
        foreach($users as $user){
            $userArray = [
                'id' => $user->getId(),
                'email' => $user->getEmail(),
                'roles' => $user->getRoles()
            ];
            array_push($jsonResponse, $userArray);
        }
    
        return new Response(json_encode($jsonResponse));
    }

    /**
     * @Route("/user/{id}", methods={"GET"})
     */
    public function getUserById($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
    
        $userRepository = $entityManager->getRepository('App:User');
        $user = $userRepository->findOneById($id);
        if($user !== null){
            $userArray = [
                'id' => $user->getId(),
                'email' => $user->getEmail(),
                'roles' => $user->getRoles()
            ];
            return new Response(json_encode($userArray));
        } else {
            $response = new Response("L'ID de l'utilisateur n'existe pas.");
            $response->setStatusCode(Response::HTTP_FORBIDDEN);
            return $response;
        }
    
    }

    /**
     * @Route("/user", methods={"POST"})
     */
    public function addUser(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $user = new \App\Entity\User();
        $userRepository = $entityManager->getRepository('App:User');

        if($request->request->get('email') !== null && $request->request->get('password') !== null){
            if($request->request->get('roleUser') === 'ROLE_ADMIN'){
                $users = $userRepository->findOneByEmail($request->request->get('email'));
                if($users == null){

                    $emailConstraint = new EmailConstraint();
                    $emailConstraint->message = "Email non valide";

                    $errors = $this->get('validator')->validateValue(
                        $request->request->get('email'),
                        $emailConstraint 
                    );

                    if($errors === ""){
                        $user->setEmail($request->request->get('email'));
                        $encoded = $encoder->encodePassword($user,$request->request->get('password'));
                        $user->setPassword($encoded);

                        if($request->request->get('role') !== null){
                            $user->setRoles($request->request->get('role'));
                        } else {
                            $user->setRoles('ROLE_USER');
                        }

                        $entityManager->persist($user);
                        $entityManager->flush();

                        return new Response('Saved new user with id '.$user->getId());
                    } else {
                        $response = new Response("L'email renseigné n'est pas valide.");
                        $response->setStatusCode(Response::HTTP_FORBIDDEN);
                        return $response;
                    }
                } else {
                    $response = new Response("L'email de l'utilisateur existe déjà");
                    $response->setStatusCode(Response::HTTP_FORBIDDEN);
                    return $response;
                }
            } else {
                $response = new Response("Vous n'avez pas la permission.");
                $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
                return $response;
            }
        } else {
            $response = new Response("Tous les champs n'ont pas été renseignés.");
            $response->setStatusCode(Response::HTTP_FORBIDDEN);
            return $response;
        }

    }

    /**
     * @Route("/user/{id}", methods={"DELETE"})
     */
    public function deleteUser($id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $userRepository = $entityManager->getRepository('App:User');
        $user = $userRepository->findOneById($id);

        if($user !== null){
            if($request->request->get('roleUser') === 'ROLE_ADMIN'){
                $entityManager->remove($user);
                $entityManager->flush();

                return new Response('Removed user with id ' . $id);
            } else {
                $response = new Response("Vous n'avez pas la permission.");
                $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
                return $response;
            }
        } else {
            $response = new Response("L'ID de l'utilisateur n'existe pas.");
            $response->setStatusCode(Response::HTTP_FORBIDDEN);
            return $response;
        }


    }

    /**
     * @Route("/user/password/{id}", methods={"PUT"})
     */
    public function updateUserPassword($id, Request $request, UserPasswordEncoderInterface $encoder)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $userRepository = $entityManager->getRepository('App:User');
        $user = $userRepository->findOneById($id);

        if($user !== null){
            if($request->request->get('newPassword1') !== null && $request->request->get('newPassword2') !== null && $request->request->get('oldPassword') !== null){
                if($request->request->get('newPassword1') === $request->request->get('newPassword2')){
                    $decoded = $encoder->isPasswordValid($user,$request->request->get('oldPassword'));
                    if($decoded){
                        $encoded = $encoder->encodePassword($user,$request->request->get('newPassword1'));
                        $user->setPassword($encoded);

                        $entityManager->persist($user);
                        $entityManager->flush();

                        return new Response('Updated user with id ' . $id);
                    } else {
                        $response = new Response("Mauvais mot de passe.");
                        $response->setStatusCode(Response::HTTP_FORBIDDEN);
                        return $response;
                    }
                } else {
                    $response = new Response("Les mots de passe renseignés sont différents.");
                    $response->setStatusCode(Response::HTTP_FORBIDDEN);
                    return $response;
                }
            } else {
                $response = new Response("Aucun mot de passe n'a été renseigné.");
                $response->setStatusCode(Response::HTTP_FORBIDDEN);
                return $response;
            }

        } else {
            $response = new Response("L'ID de l'utilisateur n'existe pas.");
            $response->setStatusCode(Response::HTTP_FORBIDDEN);
            return $response;
        }
    }

    /**
     * @Route("/user/roles/{id}", methods={"PUT"})
     */
    public function updateUserRole($id, Request $request, UserPasswordEncoderInterface $encoder)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $userRepository = $entityManager->getRepository('App:User');
        $user = $userRepository->findOneById($id);

        if($user !== null){
            if($request->request->get('newRole') !== null){
                if($request->request->get('roleUser') === 'ROLE_ADMIN'){
                    $user->setRoles($request->request->get('newRole'));
                    $entityManager->persist($user);
                    $entityManager->flush();

                    return new Response('Updated user with id ' . $id);
                } else {
                    $response = new Response("Vous n'avez pas la permission.");
                    $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
                    return $response;
                }
            } else {
                $response = new Response("Aucun champ n'a été renseigné.");
                $response->setStatusCode(Response::HTTP_FORBIDDEN);
                return $response;
            }
        } else {
            $response = new Response("L'ID de l'utilisateur n'existe pas.");
            $response->setStatusCode(Response::HTTP_FORBIDDEN);
            return $response;
        }
    }

    /**
     * @Route("/user/login", methods={"POST"})
     */
    public function loginUser(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $userRepository = $entityManager->getRepository('App:User');
        $email = $request->request->get('email');
        $password = $request->request->get('password');

        if($request->request->get('email') !== null){
            if($request->request->get('password') !== null){
                $user = $userRepository->findOneByEmail($email);
                if($user !== null){
                    $decoded = $encoder->isPasswordValid($user,$request->request->get('password'));
                    if($decoded){
                        $userArray = [
                            'id' => $user->getId(),
                            'email' => $user->getEmail(),
                            'roles' => $user->getRoles()
                        ];
                    
                        return new Response(json_encode($userArray));
                    } else {
                        $response = new Response('Le mot de passe ou l\'adresse email est erroné.');
                        $response->setStatusCode(Response::HTTP_FORBIDDEN);
                        return $response;
                    }
                } else {
                    $response = new Response('Cet email n\'existe pas.');
                    $response->setStatusCode(Response::HTTP_FORBIDDEN);
                    return $response;
                }
            } else {
                $response = new Response('Le mot de passe n\'a pas été transmis.');
                $response->setStatusCode(Response::HTTP_FORBIDDEN);
                return $response;
            }
        } else {
            $response = new Response('L\'email n\'a pas été transmis.');
            $response->setStatusCode(Response::HTTP_FORBIDDEN);
            return $response;
        }
    }
}
