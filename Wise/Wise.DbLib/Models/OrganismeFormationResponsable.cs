﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class OrganismeFormationResponsable
    {
        public OrganismeFormationResponsable()
        {
            Extra = new HashSet<Extra>();
            Formation = new HashSet<Formation>();
        }

        public int IdOrganismeFormationResponsable { get; set; }
        public string NumeroActivite { get; set; }
        public int IdSiret { get; set; }
        public string NomOrganisme { get; set; }
        public string RaisonSociale { get; set; }
        public int IdCoordonneeOrganisme { get; set; }
        public int IdContactOrganisme { get; set; }
        public string RenseignementsSpecifiques { get; set; }
        public int? IdPotentiel { get; set; }
        public string AgreementDatadock { get; set; }

        public virtual ContactOrganisme IdContactOrganismeNavigation { get; set; }
        public virtual CoordonneeOrganisme IdCoordonneeOrganismeNavigation { get; set; }
        public virtual Potentiel IdPotentielNavigation { get; set; }
        public virtual Siret IdSiretNavigation { get; set; }
        public virtual ICollection<Extra> Extra { get; set; }
        public virtual ICollection<Formation> Formation { get; set; }
    }
}
