using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Wise.DbLib.Models
{
    /// <summary>
    ///     Classe indiquant les domaines 
    /// </summary>
    public class AuthorizedDomains
    {
        /// <summary>
        ///     Identifiant du domain
        /// </summary>
        [Key]
        public int IdAuthorizedDomains { get; set; }

        /// <summary>
        ///     Nom de domaine autorisé.
        /// </summary>
        public string Domain { get; set; }

        public AuthorizedDomains()
        {

        }
    }
}
