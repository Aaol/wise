﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class DomaineFormationCodeRome
    {
        public int IdDomaineFormationCodeRome { get; set; }
        public int IdDomaineFormation { get; set; }
        public int IdCodeRome { get; set; }

        public virtual CodeRome IdCodeRomeNavigation { get; set; }
        public virtual DomaineFormation IdDomaineFormationNavigation { get; set; }
    }
}
