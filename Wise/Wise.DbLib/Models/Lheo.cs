﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Lheo
    {
        public Lheo()
        {
            Extra = new HashSet<Extra>();
        }

        public int IdLheo { get; set; }
        public int IdOffre { get; set; }

        public virtual Offre IdOffreNavigation { get; set; }
        public virtual ICollection<Extra> Extra { get; set; }
    }
}
