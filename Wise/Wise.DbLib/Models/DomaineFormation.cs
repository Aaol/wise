﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class DomaineFormation
    {
        public DomaineFormation()
        {
            DomaineFormationCodeFormacode = new HashSet<DomaineFormationCodeFormacode>();
            DomaineFormationCodeNsf = new HashSet<DomaineFormationCodeNsf>();
            DomaineFormationCodeRome = new HashSet<DomaineFormationCodeRome>();
            Extra = new HashSet<Extra>();
            Formation = new HashSet<Formation>();
        }

        public int IdDomaineFormation { get; set; }

        public virtual ICollection<DomaineFormationCodeFormacode> DomaineFormationCodeFormacode { get; set; }
        public virtual ICollection<DomaineFormationCodeNsf> DomaineFormationCodeNsf { get; set; }
        public virtual ICollection<DomaineFormationCodeRome> DomaineFormationCodeRome { get; set; }
        public virtual ICollection<Extra> Extra { get; set; }
        public virtual ICollection<Formation> Formation { get; set; }
    }
}
