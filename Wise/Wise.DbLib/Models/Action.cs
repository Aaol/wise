﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Action
    {
        public Action()
        {
            ActionOrganismeFinanceur = new HashSet<ActionOrganismeFinanceur>();
            CodePublicVise = new HashSet<CodePublicVise>();
            DateInformation = new HashSet<DateInformation>();
            Extra = new HashSet<Extra>();
            FormationAction = new HashSet<FormationAction>();
            ModalitePedagogique = new HashSet<ModalitePedagogique>();
            Session = new HashSet<Session>();
        }

        public int IdAction { get; set; }
        public string RythmeFormation { get; set; }
        public string InfoPublicVise { get; set; }
        public bool NiveauEntreeObligatoire { get; set; }
        public string ModaliteAlternance { get; set; }
        public int IdModaliteEnseignement { get; set; }
        public string ConditionSpecifiques { get; set; }
        public bool PriseEnChargeFraisPossible { get; set; }
        public int IdLieuDeFormation { get; set; }
        public bool ModalitesEntreesSorties { get; set; }
        public int? IdUrlAction { get; set; }
        public int? IdAdresseInformation { get; set; }
        public string Restauration { get; set; }
        public string Hebergement { get; set; }
        public string Transport { get; set; }
        public string AccesHandicape { get; set; }
        public string LangeFormation { get; set; }
        public string ModaliteRecrutement { get; set; }
        public string ModalitePedagogiques { get; set; }
        public string FraisRestant { get; set; }
        public int? CodePerimetreRecrutement { get; set; }
        public string InfoPerimetreRecrutement { get; set; }
        public decimal? PrixHoraireTtc { get; set; }
        public decimal? PrixTotalTtc { get; set; }
        public string DureeIndicative { get; set; }
        public int? NombreHeuresCentre { get; set; }
        public int? NombreHeuresEnterprise { get; set; }
        public int? NombreHeuresTotal { get; set; }
        public string DetailConditionPriseEnCharge { get; set; }
        public bool? Conventionnement { get; set; }
        public int? DureeConventionee { get; set; }
        public int? IdOrganismeFormateur { get; set; }
        public string FinancementFormation { get; set; }
        public int? NbPlaces { get; set; }
        public string MoyensPedagogiques { get; set; }
        public int? ResponsableEnseignement { get; set; }
        public int? NombreHeuresCm { get; set; }
        public int? NombreHeuresTd { get; set; }
        public int? NombreHeuresTpTuteure { get; set; }
        public int? NombreHeuresTpNonTuteure { get; set; }
        public int? NombreHeuresPersonnel { get; set; }

        public virtual PerimetreRecrutement CodePerimetreRecrutementNavigation { get; set; }
        public virtual AdresseInformation IdAdresseInformationNavigation { get; set; }
        public virtual LieuDeFormation IdLieuDeFormationNavigation { get; set; }
        public virtual ModaliteEnseignement IdModaliteEnseignementNavigation { get; set; }
        public virtual OrganismeFormateur IdOrganismeFormateurNavigation { get; set; }
        public virtual Web IdUrlActionNavigation { get; set; }
        public virtual Coordonnee ResponsableEnseignementNavigation { get; set; }
        public virtual ICollection<ActionOrganismeFinanceur> ActionOrganismeFinanceur { get; set; }
        public virtual ICollection<CodePublicVise> CodePublicVise { get; set; }
        public virtual ICollection<DateInformation> DateInformation { get; set; }
        public virtual ICollection<Extra> Extra { get; set; }
        public virtual ICollection<FormationAction> FormationAction { get; set; }
        public virtual ICollection<ModalitePedagogique> ModalitePedagogique { get; set; }
        public virtual ICollection<Session> Session { get; set; }
    }
}
