using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Wise.DbLib.Models
{
    public partial class WiseNetworkContext : DbContext
    {
        public WiseNetworkContext()
        {
        }

        public WiseNetworkContext(DbContextOptions<WiseNetworkContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Action> Action { get; set; }
        public virtual DbSet<ActionOrganismeFinanceur> ActionOrganismeFinanceur { get; set; }
        public virtual DbSet<Adresse> Adresse { get; set; }
        public virtual DbSet<AdresseInformation> AdresseInformation { get; set; }
        public virtual DbSet<AdresseInscription> AdresseInscription { get; set; }
        public virtual DbSet<AdresseLigne> AdresseLigne { get; set; }
        public virtual DbSet<Certification> Certification { get; set; }
        public virtual DbSet<CodeFormacode> CodeFormacode { get; set; }
        public virtual DbSet<CodeNsf> CodeNsf { get; set; }
        public virtual DbSet<CodePublicVise> CodePublicVise { get; set; }
        public virtual DbSet<CodeRome> CodeRome { get; set; }
        public virtual DbSet<ContactFormateur> ContactFormateur { get; set; }
        public virtual DbSet<ContactFormation> ContactFormation { get; set; }
        public virtual DbSet<ContactOrganisme> ContactOrganisme { get; set; }
        public virtual DbSet<ContactTel> ContactTel { get; set; }
        public virtual DbSet<Coordonnee> Coordonnee { get; set; }
        public virtual DbSet<CoordonneeLigne> CoordonneeLigne { get; set; }
        public virtual DbSet<CoordonneeOrganisme> CoordonneeOrganisme { get; set; }
        public virtual DbSet<DateInformation> DateInformation { get; set; }
        public virtual DbSet<DomaineFormation> DomaineFormation { get; set; }
        public virtual DbSet<DomaineFormationCodeFormacode> DomaineFormationCodeFormacode { get; set; }
        public virtual DbSet<DomaineFormationCodeNsf> DomaineFormationCodeNsf { get; set; }
        public virtual DbSet<DomaineFormationCodeRome> DomaineFormationCodeRome { get; set; }
        public virtual DbSet<EtatRecrutement> EtatRecrutement { get; set; }
        public virtual DbSet<Extra> Extra { get; set; }
        public virtual DbSet<Formation> Formation { get; set; }
        public virtual DbSet<FormationAction> FormationAction { get; set; }
        public virtual DbSet<FormationCertification> FormationCertification { get; set; }
        public virtual DbSet<Geolocalisation> Geolocalisation { get; set; }
        public virtual DbSet<Lheo> Lheo { get; set; }
        public virtual DbSet<LieuDeFormation> LieuDeFormation { get; set; }
        public virtual DbSet<Ligne> Ligne { get; set; }
        public virtual DbSet<ModaliteEnseignement> ModaliteEnseignement { get; set; }
        public virtual DbSet<ModalitePedagogique> ModalitePedagogique { get; set; }
        public virtual DbSet<Module> Module { get; set; }
        public virtual DbSet<ModulePrerequis> ModulePrerequis { get; set; }
        public virtual DbSet<ModulePrerequisReferenceModule> ModulePrerequisReferenceModule { get; set; }
        public virtual DbSet<Niveaux> Niveaux { get; set; }
        public virtual DbSet<Numtel> Numtel { get; set; }
        public virtual DbSet<ObjectifGeneralFormation> ObjectifGeneralFormation { get; set; }
        public virtual DbSet<Offre> Offre { get; set; }
        public virtual DbSet<OffreFormation> OffreFormation { get; set; }
        public virtual DbSet<OrganismeFinanceur> OrganismeFinanceur { get; set; }
        public virtual DbSet<OrganismeFormateur> OrganismeFormateur { get; set; }
        public virtual DbSet<OrganismeFormationResponsable> OrganismeFormationResponsable { get; set; }
        public virtual DbSet<ParcoursDeFormation> ParcoursDeFormation { get; set; }
        public virtual DbSet<PerimetreRecrutement> PerimetreRecrutement { get; set; }
        public virtual DbSet<Periode> Periode { get; set; }
        public virtual DbSet<Positionnement> Positionnement { get; set; }
        public virtual DbSet<Potentiel> Potentiel { get; set; }
        public virtual DbSet<PotentielCodeFormacode> PotentielCodeFormacode { get; set; }
        public virtual DbSet<ReferenceModule> ReferenceModule { get; set; }
        public virtual DbSet<Session> Session { get; set; }
        public virtual DbSet<Siret> Siret { get; set; }
        public virtual DbSet<SousModule> SousModule { get; set; }
        public virtual DbSet<SousModuleModule> SousModuleModule { get; set; }
        public virtual DbSet<TypeModule> TypeModule { get; set; }
        public virtual DbSet<UrlAction> UrlAction { get; set; }
        public virtual DbSet<UrlActionUrlWeb> UrlActionUrlWeb { get; set; }
        public virtual DbSet<UrlFormation> UrlFormation { get; set; }
        public virtual DbSet<UrlFormationUrlWeb> UrlFormationUrlWeb { get; set; }
        public virtual DbSet<UrlWeb> UrlWeb { get; set; }
        public virtual DbSet<Web> Web { get; set; }
        public virtual DbSet<WebUrlWeb> WebUrlWeb { get; set; }
        public virtual DbSet<User> Users { get; set; }

        public virtual DbSet<AuthorizedDomains> AuthorizedDomains { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                AppConfig appConfig = new AppConfig();
                optionsBuilder.UseMySql(appConfig.ConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<Action>(entity =>
            {
                entity.HasKey(e => e.IdAction)
                    .HasName("PK___Action__7887BBB843695383");

                entity.ToTable("_Action");

                entity.Property(e => e.IdAction).HasColumnName("id_action");

                entity.Property(e => e.AccesHandicape)
                    .HasColumnName("acces_handicape")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CodePerimetreRecrutement).HasColumnName("code_perimetre_recrutement");

                entity.Property(e => e.ConditionSpecifiques)
                    .IsRequired()
                    .HasColumnName("condition_specifiques")
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.Property(e => e.Conventionnement).HasColumnName("conventionnement");

                entity.Property(e => e.DetailConditionPriseEnCharge)
                    .HasColumnName("detail_condition_prise_en_charge")
                    .HasMaxLength(600)
                    .IsUnicode(false);

                entity.Property(e => e.DureeConventionee).HasColumnName("duree_conventionee");

                entity.Property(e => e.DureeIndicative)
                    .HasColumnName("duree_indicative")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FinancementFormation)
                    .IsRequired()
                    .HasColumnName("financement_formation")
                    .HasMaxLength(600)
                    .IsUnicode(false);

                entity.Property(e => e.FraisRestant)
                    .HasColumnName("frais_restant")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Hebergement)
                    .HasColumnName("hebergement")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.IdAdresseInformation).HasColumnName("id_adresse_information");

                entity.Property(e => e.IdLieuDeFormation).HasColumnName("id_lieu_de_formation");

                entity.Property(e => e.IdModaliteEnseignement).HasColumnName("id_modalite_enseignement");

                entity.Property(e => e.IdOrganismeFormateur).HasColumnName("id_organisme_formateur");

                entity.Property(e => e.IdUrlAction).HasColumnName("id_url_action");

                entity.Property(e => e.InfoPerimetreRecrutement)
                    .HasColumnName("info_perimetre_recrutement")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InfoPublicVise)
                    .HasColumnName("info_public_vise")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.LangeFormation)
                    .HasColumnName("lange_formation")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ModaliteAlternance)
                    .HasColumnName("modalite_alternance")
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.Property(e => e.ModalitePedagogiques)
                    .HasColumnName("modalite_pedagogiques")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ModaliteRecrutement)
                    .HasColumnName("modalite_recrutement")
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.Property(e => e.ModalitesEntreesSorties).HasColumnName("modalites_entrees_sorties");

                entity.Property(e => e.MoyensPedagogiques)
                    .IsRequired()
                    .HasColumnName("moyens_pedagogiques")
                    .HasMaxLength(600)
                    .IsUnicode(false);

                entity.Property(e => e.NbPlaces).HasColumnName("nb_places");

                entity.Property(e => e.NiveauEntreeObligatoire).HasColumnName("niveau_entree_obligatoire");

                entity.Property(e => e.NombreHeuresCentre).HasColumnName("nombre_heures_centre");

                entity.Property(e => e.NombreHeuresCm).HasColumnName("nombre_heures_cm");

                entity.Property(e => e.NombreHeuresEnterprise).HasColumnName("nombre_heures_enterprise");

                entity.Property(e => e.NombreHeuresPersonnel).HasColumnName("nombre_heures_personnel");

                entity.Property(e => e.NombreHeuresTd).HasColumnName("nombre_heures_td");

                entity.Property(e => e.NombreHeuresTotal).HasColumnName("nombre_heures_total");

                entity.Property(e => e.NombreHeuresTpNonTuteure).HasColumnName("nombre_heures_tp_non_tuteure");

                entity.Property(e => e.NombreHeuresTpTuteure).HasColumnName("nombre_heures_tp_tuteure");

                entity.Property(e => e.PriseEnChargeFraisPossible).HasColumnName("prise_en_charge_frais_possible");

                entity.Property(e => e.PrixHoraireTtc)
                    .HasColumnName("prix_horaire_ttc")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PrixTotalTtc)
                    .HasColumnName("prix_total_ttc")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ResponsableEnseignement).HasColumnName("responsable_enseignement");

                entity.Property(e => e.Restauration)
                    .HasColumnName("restauration")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.RythmeFormation)
                    .IsRequired()
                    .HasColumnName("rythme_formation")
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.Property(e => e.Transport)
                    .HasColumnName("transport")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.HasOne(d => d.CodePerimetreRecrutementNavigation)
                    .WithMany(p => p.Action)
                    .HasPrincipalKey(p => p.Cle)
                    .HasForeignKey(d => d.CodePerimetreRecrutement)
                    .HasConstraintName("fk_code_perimetre_recrutement_action");

                entity.HasOne(d => d.IdAdresseInformationNavigation)
                    .WithMany(p => p.Action)
                    .HasForeignKey(d => d.IdAdresseInformation)
                    .HasConstraintName("fk_action_id_adresse_information");

                entity.HasOne(d => d.IdLieuDeFormationNavigation)
                    .WithMany(p => p.Action)
                    .HasForeignKey(d => d.IdLieuDeFormation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_lieu_de_formation_action");

                entity.HasOne(d => d.IdModaliteEnseignementNavigation)
                    .WithMany(p => p.Action)
                    .HasForeignKey(d => d.IdModaliteEnseignement)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_modalite_enseignement_action");

                entity.HasOne(d => d.IdOrganismeFormateurNavigation)
                    .WithMany(p => p.Action)
                    .HasForeignKey(d => d.IdOrganismeFormateur)
                    .HasConstraintName("fk_id_organisme_formateur_action");

                entity.HasOne(d => d.IdUrlActionNavigation)
                    .WithMany(p => p.Action)
                    .HasForeignKey(d => d.IdUrlAction)
                    .HasConstraintName("fk_id_url_action_action");

                entity.HasOne(d => d.ResponsableEnseignementNavigation)
                    .WithMany(p => p.Action)
                    .HasForeignKey(d => d.ResponsableEnseignement)
                    .HasConstraintName("fk_responsable_enseignement_action");
            });

            modelBuilder.Entity<ActionOrganismeFinanceur>(entity =>
            {
                entity.HasKey(e => e.IdActionOrganismeFinanceur)
                    .HasName("PK__ActionOr__DFCF008991F752BB");

                entity.Property(e => e.IdActionOrganismeFinanceur).HasColumnName("id_action_organisme_financeur");

                entity.Property(e => e.IdAction).HasColumnName("id_action");

                entity.Property(e => e.IdOrganismeFinanceur).HasColumnName("id_organisme_financeur");

                entity.HasOne(d => d.IdActionNavigation)
                    .WithMany(p => p.ActionOrganismeFinanceur)
                    .HasForeignKey(d => d.IdAction)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_action_id_action");

                entity.HasOne(d => d.IdOrganismeFinanceurNavigation)
                    .WithMany(p => p.ActionOrganismeFinanceur)
                    .HasForeignKey(d => d.IdOrganismeFinanceur)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_action_id_organisme_financeur");
            });

            modelBuilder.Entity<Adresse>(entity =>
            {
                entity.HasKey(e => e.IdAdresse)
                    .HasName("PK__Adresse__05B3E6DC4344A34C");

                entity.Property(e => e.IdAdresse).HasColumnName("id_adresse");

                entity.Property(e => e.CodeInseeCanton)
                    .HasColumnName("code_INSEE_canton")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.CodeInseeCommune)
                    .HasColumnName("code_INSEE_commune")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Codepostal)
                    .IsRequired()
                    .HasColumnName("codepostal")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Departement)
                    .HasColumnName("departement")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.IdGeolocalisation).HasColumnName("id_geolocalisation");

                entity.Property(e => e.Pays)
                    .HasColumnName("pays")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Region)
                    .HasColumnName("region")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Ville)
                    .IsRequired()
                    .HasColumnName("ville")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdGeolocalisationNavigation)
                    .WithMany(p => p.Adresse)
                    .HasForeignKey(d => d.IdGeolocalisation)
                    .HasConstraintName("fk_id_geolocalisation_adresse");
            });

            modelBuilder.Entity<AdresseInformation>(entity =>
            {
                entity.HasKey(e => e.IdAdresseInformation)
                    .HasName("PK__AdresseI__C61EEE43EA77F9F7");

                entity.Property(e => e.IdAdresseInformation).HasColumnName("id_adresse_information");

                entity.Property(e => e.IdAdresse).HasColumnName("id_adresse");

                entity.HasOne(d => d.IdAdresseNavigation)
                    .WithMany(p => p.AdresseInformation)
                    .HasForeignKey(d => d.IdAdresse)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_adresse_information_id_adresse");
            });

            modelBuilder.Entity<AdresseInscription>(entity =>
            {
                entity.HasKey(e => e.IdAdresseInscription)
                    .HasName("PK__AdresseI__4A1FF2CF6DEFAF0F");

                entity.Property(e => e.IdAdresseInscription).HasColumnName("id_adresse_inscription");

                entity.Property(e => e.IdAdresse).HasColumnName("id_adresse");

                entity.HasOne(d => d.IdAdresseNavigation)
                    .WithMany(p => p.AdresseInscription)
                    .HasForeignKey(d => d.IdAdresse)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_adresse_adresse_inscription");
            });

            modelBuilder.Entity<AdresseLigne>(entity =>
            {
                entity.HasKey(e => e.IdAdresseLigne)
                    .HasName("PK__AdresseL__C9625507715965F7");

                entity.Property(e => e.IdAdresseLigne).HasColumnName("id_adresse_ligne");

                entity.Property(e => e.IdAdresse).HasColumnName("id_adresse");

                entity.Property(e => e.IdLigne).HasColumnName("id_ligne");

                entity.HasOne(d => d.IdAdresseNavigation)
                    .WithMany(p => p.AdresseLigne)
                    .HasForeignKey(d => d.IdAdresse)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_adresse_adresse_ligne");

                entity.HasOne(d => d.IdLigneNavigation)
                    .WithMany(p => p.AdresseLigne)
                    .HasForeignKey(d => d.IdLigne)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_ligne_adresse_ligne");
            });

            modelBuilder.Entity<Certification>(entity =>
            {
                entity.HasKey(e => e.IdCertification)
                    .HasName("PK__Certific__5476C1F4B1D65EC6");

                entity.Property(e => e.IdCertification).HasColumnName("id_certification");

                entity.Property(e => e.CodeCertifinfo)
                    .HasColumnName("code_CERTIFINFO")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.CodeRncp)
                    .HasColumnName("code_RNCP")
                    .HasMaxLength(6)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CodeFormacode>(entity =>
            {
                entity.HasKey(e => e.IdCodeFormacode)
                    .HasName("PK__CodeFORM__BD446C4251A5980C");

                entity.ToTable("CodeFORMACODE");

                entity.Property(e => e.IdCodeFormacode).HasColumnName("id_code_FORMACODE");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CodeNsf>(entity =>
            {
                entity.HasKey(e => e.IdCodeNsf)
                    .HasName("PK__CodeNSF__C16CF64635FBB9B8");

                entity.ToTable("CodeNSF");

                entity.Property(e => e.IdCodeNsf).HasColumnName("id_code_NSF");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(3)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CodePublicVise>(entity =>
            {
                entity.HasKey(e => e.IdCodePublicVise)
                    .HasName("PK__CodePubl__2C6CB22F65A60F31");

                entity.Property(e => e.IdCodePublicVise).HasColumnName("id_code_public_vise");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.IdAction).HasColumnName("id_action");

                entity.HasOne(d => d.IdActionNavigation)
                    .WithMany(p => p.CodePublicVise)
                    .HasForeignKey(d => d.IdAction)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_code_public_vise_id_action");
            });

            modelBuilder.Entity<CodeRome>(entity =>
            {
                entity.HasKey(e => e.IdCodeRome)
                    .HasName("PK__CodeROME__D6BFC2E41DCAB306");

                entity.ToTable("CodeROME");

                entity.Property(e => e.IdCodeRome).HasColumnName("id_code_ROME");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ContactFormateur>(entity =>
            {
                entity.HasKey(e => e.IdContactFormateur)
                    .HasName("PK__ContactF__13BA644DA6F04B2F");

                entity.Property(e => e.IdContactFormateur).HasColumnName("id_contact_formateur");

                entity.Property(e => e.IdCoordonnee).HasColumnName("id_coordonnee");

                entity.HasOne(d => d.IdCoordonneeNavigation)
                    .WithMany(p => p.ContactFormateur)
                    .HasForeignKey(d => d.IdCoordonnee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_coordonnee_contact_formateur");
            });

            modelBuilder.Entity<ContactFormation>(entity =>
            {
                entity.HasKey(e => e.IdContactFormation)
                    .HasName("PK__ContactF__18C00F21D3DDC2B7");

                entity.Property(e => e.IdContactFormation).HasColumnName("id_contact_formation");

                entity.Property(e => e.IdCoordonnee).HasColumnName("id_coordonnee");

                entity.HasOne(d => d.IdCoordonneeNavigation)
                    .WithMany(p => p.ContactFormation)
                    .HasForeignKey(d => d.IdCoordonnee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_coordonnee_contact_formation");
            });

            modelBuilder.Entity<ContactOrganisme>(entity =>
            {
                entity.HasKey(e => e.IdContactOrganisme)
                    .HasName("PK__ContactO__7530B76D04F6FF92");

                entity.Property(e => e.IdContactOrganisme).HasColumnName("id_contact_organisme");

                entity.Property(e => e.IdCoordonnee).HasColumnName("id_coordonnee");

                entity.HasOne(d => d.IdCoordonneeNavigation)
                    .WithMany(p => p.ContactOrganisme)
                    .HasForeignKey(d => d.IdCoordonnee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_coordonnee_contact_organisme");
            });

            modelBuilder.Entity<ContactTel>(entity =>
            {
                entity.HasKey(e => e.IdContactTel)
                    .HasName("PK__ContactT__E8D745748BC770D2");

                entity.Property(e => e.IdContactTel).HasColumnName("id_contact_tel");

                entity.Property(e => e.IdNumtel).HasColumnName("id_numtel");

                entity.HasOne(d => d.IdNumtelNavigation)
                    .WithMany(p => p.ContactTel)
                    .HasForeignKey(d => d.IdNumtel)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_numtel_contact_tel");
            });

            modelBuilder.Entity<Coordonnee>(entity =>
            {
                entity.HasKey(e => e.IdCoordonnee)
                    .HasName("PK__Coordonn__DD52265B6316F225");

                entity.Property(e => e.IdCoordonnee).HasColumnName("id_coordonnee");

                entity.Property(e => e.Civilite)
                    .HasColumnName("civilite")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Courriel)
                    .HasColumnName("courriel")
                    .HasMaxLength(160)
                    .IsUnicode(false);

                entity.Property(e => e.IdAdresse).HasColumnName("id_adresse");

                entity.Property(e => e.IdFax).HasColumnName("id_fax");

                entity.Property(e => e.IdPortable).HasColumnName("id_portable");

                entity.Property(e => e.IdTelfixe).HasColumnName("id_telfixe");

                entity.Property(e => e.IdWeb).HasColumnName("id_web");

                entity.Property(e => e.Nom)
                    .HasColumnName("nom")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Prenom)
                    .HasColumnName("prenom")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAdresseNavigation)
                    .WithMany(p => p.Coordonnee)
                    .HasForeignKey(d => d.IdAdresse)
                    .HasConstraintName("fk_id_adresse_coordonnee");

                entity.HasOne(d => d.IdFaxNavigation)
                    .WithMany(p => p.CoordonneeIdFaxNavigation)
                    .HasForeignKey(d => d.IdFax)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_fax_coordonnee");

                entity.HasOne(d => d.IdPortableNavigation)
                    .WithMany(p => p.CoordonneeIdPortableNavigation)
                    .HasForeignKey(d => d.IdPortable)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_portable_coordonnee");

                entity.HasOne(d => d.IdTelfixeNavigation)
                    .WithMany(p => p.CoordonneeIdTelfixeNavigation)
                    .HasForeignKey(d => d.IdTelfixe)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_telfixe_coordonnee");

                entity.HasOne(d => d.IdWebNavigation)
                    .WithMany(p => p.Coordonnee)
                    .HasForeignKey(d => d.IdWeb)
                    .HasConstraintName("fk_id_web_coordonnee");
            });

            modelBuilder.Entity<CoordonneeLigne>(entity =>
            {
                entity.HasKey(e => e.IdCoordonneeLigne)
                    .HasName("PK__Coordonn__A9E5D85929C2A2B1");

                entity.Property(e => e.IdCoordonneeLigne).HasColumnName("id_coordonnee_ligne");

                entity.Property(e => e.IdCoordonnee).HasColumnName("id_coordonnee");

                entity.Property(e => e.IdLigne).HasColumnName("id_ligne");

                entity.HasOne(d => d.IdCoordonneeNavigation)
                    .WithMany(p => p.CoordonneeLigne)
                    .HasForeignKey(d => d.IdCoordonnee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_coordonnee_coordonnee_ligne");

                entity.HasOne(d => d.IdLigneNavigation)
                    .WithMany(p => p.CoordonneeLigne)
                    .HasForeignKey(d => d.IdLigne)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_ligne_coordonnee_ligne");
            });

            modelBuilder.Entity<CoordonneeOrganisme>(entity =>
            {
                entity.HasKey(e => e.IdCoordonneeOrganisme)
                    .HasName("PK__Coordonn__2193A858D1444CD5");

                entity.Property(e => e.IdCoordonneeOrganisme).HasColumnName("id_coordonnee_organisme");

                entity.Property(e => e.IdCoordonnee).HasColumnName("id_coordonnee");

                entity.HasOne(d => d.IdCoordonneeNavigation)
                    .WithMany(p => p.CoordonneeOrganisme)
                    .HasForeignKey(d => d.IdCoordonnee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_coordonnee_coordonnee_organisme_coordonne");
            });

            modelBuilder.Entity<DateInformation>(entity =>
            {
                entity.HasKey(e => e.IdDateInformation)
                    .HasName("PK__DateInfo__86DD22D550A15AA7");

                entity.Property(e => e.IdDateInformation).HasColumnName("id_date_information");

                entity.Property(e => e.DateInformation1)
                    .HasColumnName("date_information")
                    .HasColumnType("date");

                entity.Property(e => e.IdAction).HasColumnName("id_action");

                entity.HasOne(d => d.IdActionNavigation)
                    .WithMany(p => p.DateInformation)
                    .HasForeignKey(d => d.IdAction)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_date_information_id_action");
            });

            modelBuilder.Entity<DomaineFormation>(entity =>
            {
                entity.HasKey(e => e.IdDomaineFormation)
                    .HasName("PK__DomaineF__3F492C6A5C76DCEC");

                entity.Property(e => e.IdDomaineFormation).HasColumnName("id_domaine_formation");
            });

            modelBuilder.Entity<DomaineFormationCodeFormacode>(entity =>
            {
                entity.HasKey(e => e.IdDomaineFormationCodeFormacode)
                    .HasName("PK__DomaineF__089B4C1C894CF4F4");

                entity.ToTable("DomaineFormationCodeFORMACODE");

                entity.Property(e => e.IdDomaineFormationCodeFormacode).HasColumnName("id_domaine_formation_code_FORMACODE");

                entity.Property(e => e.IdCodeFormacode).HasColumnName("id_code_FORMACODE");

                entity.Property(e => e.IdDomaineFormation).HasColumnName("id_domaine_formation");

                entity.HasOne(d => d.IdCodeFormacodeNavigation)
                    .WithMany(p => p.DomaineFormationCodeFormacode)
                    .HasForeignKey(d => d.IdCodeFormacode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_code_FORMACODE_domaine_formation_code_FORMACODE");

                entity.HasOne(d => d.IdDomaineFormationNavigation)
                    .WithMany(p => p.DomaineFormationCodeFormacode)
                    .HasForeignKey(d => d.IdDomaineFormation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_domaine_formation_domaine_formation_code_FORMACODE");
            });

            modelBuilder.Entity<DomaineFormationCodeNsf>(entity =>
            {
                entity.HasKey(e => e.IdDomaineFormationCodeNsf)
                    .HasName("PK__DomaineF__A612B75B2AD25A3E");

                entity.ToTable("DomaineFormationCodeNSF");

                entity.Property(e => e.IdDomaineFormationCodeNsf).HasColumnName("id_domaine_formation_code_NSF");

                entity.Property(e => e.IdCodeNsf).HasColumnName("id_code_NSF");

                entity.Property(e => e.IdDomaineFormation).HasColumnName("id_domaine_formation");

                entity.HasOne(d => d.IdCodeNsfNavigation)
                    .WithMany(p => p.DomaineFormationCodeNsf)
                    .HasForeignKey(d => d.IdCodeNsf)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_code_NSF_domaine_formation_code_NSF");

                entity.HasOne(d => d.IdDomaineFormationNavigation)
                    .WithMany(p => p.DomaineFormationCodeNsf)
                    .HasForeignKey(d => d.IdDomaineFormation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_domaine_formation_domaine_formation_code_NSF");
            });

            modelBuilder.Entity<DomaineFormationCodeRome>(entity =>
            {
                entity.HasKey(e => e.IdDomaineFormationCodeRome)
                    .HasName("PK__DomaineF__BBA6C91E3738AEAD");

                entity.ToTable("DomaineFormationCodeROME");

                entity.Property(e => e.IdDomaineFormationCodeRome).HasColumnName("id_domaine_formation_code_ROME");

                entity.Property(e => e.IdCodeRome).HasColumnName("id_code_ROME");

                entity.Property(e => e.IdDomaineFormation).HasColumnName("id_domaine_formation");

                entity.HasOne(d => d.IdCodeRomeNavigation)
                    .WithMany(p => p.DomaineFormationCodeRome)
                    .HasForeignKey(d => d.IdCodeRome)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_code_ROME_domaine_formation_code_ROME");

                entity.HasOne(d => d.IdDomaineFormationNavigation)
                    .WithMany(p => p.DomaineFormationCodeRome)
                    .HasForeignKey(d => d.IdDomaineFormation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_domaine_formation_domaine_formation_code_ROME");
            });

            modelBuilder.Entity<EtatRecrutement>(entity =>
            {
                entity.HasKey(e => e.IdEtatRecrutement)
                    .HasName("PK__EtatRecr__1FAFCBDE36242827");

                entity.Property(e => e.IdEtatRecrutement).HasColumnName("id_etat_recrutement");

                entity.Property(e => e.Label)
                    .IsRequired()
                    .HasColumnName("label")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecrutementKey).HasColumnName("recrutement_key");
            });

            modelBuilder.Entity<Extra>(entity =>
            {
                entity.HasKey(e => e.IdExtra)
                    .HasName("PK__Extra__8875B27144CD35A1");

                entity.Property(e => e.IdExtra).HasColumnName("id_extra");

                entity.Property(e => e.Commentaire)
                    .IsRequired()
                    .HasColumnName("commentaire")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.IdAction).HasColumnName("id_action");

                entity.Property(e => e.IdAdresse).HasColumnName("id_adresse");

                entity.Property(e => e.IdAdresseInscription).HasColumnName("id_adresse_inscription");

                entity.Property(e => e.IdCertification).HasColumnName("id_certification");

                entity.Property(e => e.IdContactFormateur).HasColumnName("id_contact_formateur");

                entity.Property(e => e.IdContactFormation).HasColumnName("id_contact_formation");

                entity.Property(e => e.IdContactOrganisme).HasColumnName("id_contact_organisme");

                entity.Property(e => e.IdCoordonnee).HasColumnName("id_coordonnee");

                entity.Property(e => e.IdCoordonneeOrganisme).HasColumnName("id_coordonnee_organisme");

                entity.Property(e => e.IdDateInformation).HasColumnName("id_date_information");

                entity.Property(e => e.IdDomaineFormation).HasColumnName("id_domaine_formation");

                entity.Property(e => e.IdFormation).HasColumnName("id_formation");

                entity.Property(e => e.IdGeolocalisation).HasColumnName("id_geolocalisation");

                entity.Property(e => e.IdLheo).HasColumnName("id_lheo");

                entity.Property(e => e.IdModule).HasColumnName("id_module");

                entity.Property(e => e.IdModulesPrerequis).HasColumnName("id_modules_prerequis");

                entity.Property(e => e.IdOffre).HasColumnName("id_offre");

                entity.Property(e => e.IdOrganismeFinanceur).HasColumnName("id_organisme_financeur");

                entity.Property(e => e.IdOrganismeFormateur).HasColumnName("id_organisme_formateur");

                entity.Property(e => e.IdOrganismeFormationResponsable).HasColumnName("id_organisme_formation_responsable");

                entity.Property(e => e.IdPeriode).HasColumnName("id_periode");

                entity.Property(e => e.IdPotentiel).HasColumnName("id_potentiel");

                entity.Property(e => e.IdSiret).HasColumnName("id_SIRET");

                entity.Property(e => e.IdSousModule).HasColumnName("id_sous_module");

                entity.Property(e => e.IdUrlAction).HasColumnName("id_url_action");

                entity.Property(e => e.IdWeb).HasColumnName("id_web");

                entity.HasOne(d => d.IdActionNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdAction)
                    .HasConstraintName("fk_id_action_extra");

                entity.HasOne(d => d.IdAdresseNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdAdresse)
                    .HasConstraintName("fk_id_adresse_extra");

                entity.HasOne(d => d.IdAdresseInscriptionNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdAdresseInscription)
                    .HasConstraintName("fk_id_adresse_inscription_extra");

                entity.HasOne(d => d.IdCertificationNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdCertification)
                    .HasConstraintName("fk_id_certification_extra");

                entity.HasOne(d => d.IdContactFormateurNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdContactFormateur)
                    .HasConstraintName("fk_id_contact_formateur_extra");

                entity.HasOne(d => d.IdContactFormationNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdContactFormation)
                    .HasConstraintName("fk_id_contact_formation_extra");

                entity.HasOne(d => d.IdContactOrganismeNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdContactOrganisme)
                    .HasConstraintName("fk_id_contact_organisme_extra");

                entity.HasOne(d => d.IdCoordonneeNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdCoordonnee)
                    .HasConstraintName("fk_id_coordonnee_extra");

                entity.HasOne(d => d.IdCoordonneeOrganismeNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdCoordonneeOrganisme)
                    .HasConstraintName("fk_id_coordonnee_organisme_extra");

                entity.HasOne(d => d.IdDateInformationNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdDateInformation)
                    .HasConstraintName("fk_id_date_information_extra");

                entity.HasOne(d => d.IdDomaineFormationNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdDomaineFormation)
                    .HasConstraintName("fk_id_domaine_formation_extra");

                entity.HasOne(d => d.IdFormationNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdFormation)
                    .HasConstraintName("fk_id_formation_extra");

                entity.HasOne(d => d.IdGeolocalisationNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdGeolocalisation)
                    .HasConstraintName("fk_id_geolocalisation_extra");

                entity.HasOne(d => d.IdLheoNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdLheo)
                    .HasConstraintName("fk_id_lheo_extra");

                entity.HasOne(d => d.IdModuleNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdModule)
                    .HasConstraintName("fk_id_module_extra");

                entity.HasOne(d => d.IdModulesPrerequisNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdModulesPrerequis)
                    .HasConstraintName("fk_id_modules_prerequis_extra");

                entity.HasOne(d => d.IdOffreNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdOffre)
                    .HasConstraintName("fk_id_offre_extra");

                entity.HasOne(d => d.IdOrganismeFinanceurNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdOrganismeFinanceur)
                    .HasConstraintName("fk_id_organisme_financeur_extra");

                entity.HasOne(d => d.IdOrganismeFormateurNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdOrganismeFormateur)
                    .HasConstraintName("fk_id_organisme_formateur_extra");

                entity.HasOne(d => d.IdOrganismeFormationResponsableNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdOrganismeFormationResponsable)
                    .HasConstraintName("fk_id_organisme_formation_responsable_extra");

                entity.HasOne(d => d.IdPeriodeNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdPeriode)
                    .HasConstraintName("fk_id_periode_extra");

                entity.HasOne(d => d.IdPotentielNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdPotentiel)
                    .HasConstraintName("fk_id_potentiel_extra");

                entity.HasOne(d => d.IdSiretNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdSiret)
                    .HasConstraintName("fk_id_SIRET_extra");

                entity.HasOne(d => d.IdSousModuleNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdSousModule)
                    .HasConstraintName("fk_id_sous_module_extra");

                entity.HasOne(d => d.IdUrlActionNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdUrlAction)
                    .HasConstraintName("fk_id_url_action_extra");

                entity.HasOne(d => d.IdWebNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdWeb)
                    .HasConstraintName("fk_id_web_extra");
            });

            modelBuilder.Entity<Formation>(entity =>
            {
                entity.HasKey(e => e.IdFormation)
                    .HasName("PK__Formatio__0B5751331F0EB9DA");

                entity.Property(e => e.IdFormation).HasColumnName("id_formation");

                entity.Property(e => e.Certifiante).HasColumnName("certifiante");

                entity.Property(e => e.ContenuFormation)
                    .IsRequired()
                    .HasColumnName("contenu_formation")
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.Property(e => e.CreditsEcts).HasColumnName("credits_ects");

                entity.Property(e => e.EligibiliteCpf)
                    .IsRequired()
                    .HasColumnName("eligibilite_cpf")
                    .HasMaxLength(600)
                    .IsUnicode(false);

                entity.Property(e => e.IdCodeNiveauSortie).HasColumnName("id_code_niveau_sortie");

                entity.Property(e => e.IdContactFormation).HasColumnName("id_contact_formation");

                entity.Property(e => e.IdDomaineFormation).HasColumnName("id_domaine_formation");

                entity.Property(e => e.IdModulesPrerequis).HasColumnName("id_modules_prerequis");

                entity.Property(e => e.IdNiveauxEntree).HasColumnName("id_niveaux_entree");

                entity.Property(e => e.IdObjectifGeneralFormation).HasColumnName("id_objectif_general_formation");

                entity.Property(e => e.IdOrganismeFormationResponsable).HasColumnName("id_organisme_formation_responsable");

                entity.Property(e => e.IdParcoursDeFormation).HasColumnName("id_parcours_de_formation");

                entity.Property(e => e.IdPositionnement).HasColumnName("id_positionnement");

                entity.Property(e => e.IdSousModule).HasColumnName("id_sous_module");

                entity.Property(e => e.IdUrlFormation).HasColumnName("id_url_formation");

                entity.Property(e => e.IdentifiantModule)
                    .HasColumnName("identifiant_module")
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.Property(e => e.IntituleFormation)
                    .IsRequired()
                    .HasColumnName("intitule_formation")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ObjectifFormation)
                    .IsRequired()
                    .HasColumnName("objectif_formation")
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.Property(e => e.ResultatsAttendus)
                    .IsRequired()
                    .HasColumnName("resultats_attendus")
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.Property(e => e.Validations)
                    .IsRequired()
                    .HasColumnName("validations")
                    .HasMaxLength(600)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdCodeNiveauSortieNavigation)
                    .WithMany(p => p.FormationIdCodeNiveauSortieNavigation)
                    .HasForeignKey(d => d.IdCodeNiveauSortie)
                    .HasConstraintName("fk_id_code_niveau_sortie_formation");

                entity.HasOne(d => d.IdContactFormationNavigation)
                    .WithMany(p => p.Formation)
                    .HasForeignKey(d => d.IdContactFormation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_contact_formation_formation");

                entity.HasOne(d => d.IdDomaineFormationNavigation)
                    .WithMany(p => p.Formation)
                    .HasForeignKey(d => d.IdDomaineFormation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_domaine_formation_formation");

                entity.HasOne(d => d.IdModulesPrerequisNavigation)
                    .WithMany(p => p.Formation)
                    .HasForeignKey(d => d.IdModulesPrerequis)
                    .HasConstraintName("fk_id_modules_prerequis_formation");

                entity.HasOne(d => d.IdNiveauxEntreeNavigation)
                    .WithMany(p => p.FormationIdNiveauxEntreeNavigation)
                    .HasForeignKey(d => d.IdNiveauxEntree)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_niveaux_entree_formation");

                entity.HasOne(d => d.IdObjectifGeneralFormationNavigation)
                    .WithMany(p => p.Formation)
                    .HasForeignKey(d => d.IdObjectifGeneralFormation)
                    .HasConstraintName("fk_id_objectif_general_formation");

                entity.HasOne(d => d.IdOrganismeFormationResponsableNavigation)
                    .WithMany(p => p.Formation)
                    .HasForeignKey(d => d.IdOrganismeFormationResponsable)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_organisme_formation_responsable_formation");

                entity.HasOne(d => d.IdParcoursDeFormationNavigation)
                    .WithMany(p => p.Formation)
                    .HasForeignKey(d => d.IdParcoursDeFormation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_parcours_de_formation_formation");

                entity.HasOne(d => d.IdPositionnementNavigation)
                    .WithMany(p => p.Formation)
                    .HasForeignKey(d => d.IdPositionnement)
                    .HasConstraintName("fk_id_positionnement_formation");

                entity.HasOne(d => d.IdSousModuleNavigation)
                    .WithMany(p => p.Formation)
                    .HasForeignKey(d => d.IdSousModule)
                    .HasConstraintName("fk_id_sous_module_formation");

                entity.HasOne(d => d.IdUrlFormationNavigation)
                    .WithMany(p => p.Formation)
                    .HasForeignKey(d => d.IdUrlFormation)
                    .HasConstraintName("fk_id_url_formation_formation");
            });

            modelBuilder.Entity<FormationAction>(entity =>
            {
                entity.HasKey(e => e.IdFormationAction)
                    .HasName("PK__Formatio__AFE2284C6150F7FC");

                entity.Property(e => e.IdFormationAction).HasColumnName("id_formation_action");

                entity.Property(e => e.IdAction).HasColumnName("id_action");

                entity.Property(e => e.IdFormation).HasColumnName("id_formation");

                entity.HasOne(d => d.IdActionNavigation)
                    .WithMany(p => p.FormationAction)
                    .HasForeignKey(d => d.IdAction)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_action_formation_action");

                entity.HasOne(d => d.IdFormationNavigation)
                    .WithMany(p => p.FormationAction)
                    .HasForeignKey(d => d.IdFormation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_formation_formation_action");
            });

            modelBuilder.Entity<FormationCertification>(entity =>
            {
                entity.HasKey(e => e.IdFormationCertification)
                    .HasName("PK__Formatio__452A018E0373D2DE");

                entity.Property(e => e.IdFormationCertification).HasColumnName("id_formation_certification");

                entity.Property(e => e.IdCertification).HasColumnName("id_certification");

                entity.Property(e => e.IdFormation).HasColumnName("id_formation");

                entity.HasOne(d => d.IdCertificationNavigation)
                    .WithMany(p => p.FormationCertification)
                    .HasForeignKey(d => d.IdCertification)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_certification_formation_certification");

                entity.HasOne(d => d.IdFormationNavigation)
                    .WithMany(p => p.FormationCertification)
                    .HasForeignKey(d => d.IdFormation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_formation_formation_certification");
            });

            modelBuilder.Entity<Geolocalisation>(entity =>
            {
                entity.HasKey(e => e.IdGeolocalisation)
                    .HasName("PK__Geolocal__4EB547FEA8A3CF96");

                entity.Property(e => e.IdGeolocalisation).HasColumnName("id_geolocalisation");

                entity.Property(e => e.Latitude)
                    .IsRequired()
                    .HasColumnName("latitude")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Longitude)
                    .IsRequired()
                    .HasColumnName("longitude")
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Lheo>(entity =>
            {
                entity.HasKey(e => e.IdLheo)
                    .HasName("PK__Lheo__99C21B20BF43CBA8");

                entity.Property(e => e.IdLheo).HasColumnName("id_lheo");

                entity.Property(e => e.IdOffre).HasColumnName("id_offre");

                entity.HasOne(d => d.IdOffreNavigation)
                    .WithMany(p => p.Lheo)
                    .HasForeignKey(d => d.IdOffre)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_offre_lheo");
            });

            modelBuilder.Entity<LieuDeFormation>(entity =>
            {
                entity.HasKey(e => e.IdLieuDeFormation)
                    .HasName("PK__LieuDeFo__8B17F49BD6B7B0C3");

                entity.Property(e => e.IdLieuDeFormation).HasColumnName("id_lieu_de_formation");

                entity.Property(e => e.IdCoordonnee).HasColumnName("id_coordonnee");

                entity.HasOne(d => d.IdCoordonneeNavigation)
                    .WithMany(p => p.LieuDeFormation)
                    .HasForeignKey(d => d.IdCoordonnee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_coordonnee_lieu_de_formation");
            });

            modelBuilder.Entity<Ligne>(entity =>
            {
                entity.HasKey(e => e.IdLigne)
                    .HasName("PK__Ligne__E1D60C0CB48E0740");

                entity.Property(e => e.IdLigne).HasColumnName("id_ligne");

                entity.Property(e => e.Ligne1)
                    .IsRequired()
                    .HasColumnName("ligne")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ModaliteEnseignement>(entity =>
            {
                entity.HasKey(e => e.IdModaliteEnseignement)
                    .HasName("PK__Modalite__3FC9D032D272CBC8");

                entity.Property(e => e.IdModaliteEnseignement).HasColumnName("id_modalite_enseignement");

                entity.Property(e => e.Label)
                    .IsRequired()
                    .HasColumnName("label")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModaliteKey).HasColumnName("modalite_key");
            });

            modelBuilder.Entity<ModalitePedagogique>(entity =>
            {
                entity.HasKey(e => e.IdModalitePedagogique)
                    .HasName("PK__Modalite__D4F06B3A5F28BE04");

                entity.Property(e => e.IdModalitePedagogique).HasColumnName("id_modalite_pedagogique");

                entity.Property(e => e.IdAction).HasColumnName("id_action");

                entity.Property(e => e.Label)
                    .IsRequired()
                    .HasColumnName("label")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdActionNavigation)
                    .WithMany(p => p.ModalitePedagogique)
                    .HasForeignKey(d => d.IdAction)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_action_modalite_information");
            });

            modelBuilder.Entity<Module>(entity =>
            {
                entity.HasKey(e => e.IdModule)
                    .HasName("PK__Module__B2584DEA0339E564");

                entity.Property(e => e.IdModule).HasColumnName("id_module");

                entity.Property(e => e.IdTypeModule).HasColumnName("id_type_module");

                entity.Property(e => e.ReferenceModule)
                    .IsRequired()
                    .HasColumnName("reference_module")
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdTypeModuleNavigation)
                    .WithMany(p => p.Module)
                    .HasForeignKey(d => d.IdTypeModule)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_type_module_module");
            });

            modelBuilder.Entity<ModulePrerequis>(entity =>
            {
                entity.HasKey(e => e.IdModulesPrerequis)
                    .HasName("PK__ModulePr__59CA7BEDB89CAE01");

                entity.Property(e => e.IdModulesPrerequis).HasColumnName("id_modules_prerequis");
            });

            modelBuilder.Entity<ModulePrerequisReferenceModule>(entity =>
            {
                entity.HasKey(e => e.IdModulePrerequisReferenceModule)
                    .HasName("PK__ModulePr__A2C115D00AAD1F91");

                entity.Property(e => e.IdModulePrerequisReferenceModule).HasColumnName("id_module_prerequis_reference_module");

                entity.Property(e => e.IdModulesPrerequis).HasColumnName("id_modules_prerequis");

                entity.Property(e => e.IdReferenceModule).HasColumnName("id_reference_module");

                entity.HasOne(d => d.IdModulesPrerequisNavigation)
                    .WithMany(p => p.ModulePrerequisReferenceModule)
                    .HasForeignKey(d => d.IdModulesPrerequis)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_modules_prerequis_module_prerequis_reference_module");

                entity.HasOne(d => d.IdReferenceModuleNavigation)
                    .WithMany(p => p.ModulePrerequisReferenceModule)
                    .HasForeignKey(d => d.IdReferenceModule)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_reference_module_module_prerequis_reference_module");
            });

            modelBuilder.Entity<Niveaux>(entity =>
            {
                entity.HasKey(e => e.IdNiveaux)
                    .HasName("PK__Niveaux__E209DB1585572B1B");

                entity.Property(e => e.IdNiveaux).HasColumnName("id_niveaux");

                entity.Property(e => e.Libele)
                    .IsRequired()
                    .HasColumnName("libele")
                    .HasMaxLength(65)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Numtel>(entity =>
            {
                entity.HasKey(e => e.IdNumtel)
                    .HasName("PK__Numtel__FDE233E733FF21E3");

                entity.Property(e => e.IdNumtel).HasColumnName("id_numtel");

                entity.Property(e => e.Numero)
                    .IsRequired()
                    .HasColumnName("numero")
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ObjectifGeneralFormation>(entity =>
            {
                entity.HasKey(e => e.IdObjectifGeneralFormation)
                    .HasName("PK__Objectif__52EC2FA750D23524");

                entity.Property(e => e.IdObjectifGeneralFormation).HasColumnName("id_objectif_general_formation");

                entity.Property(e => e.Libele)
                    .IsRequired()
                    .HasColumnName("libele")
                    .HasMaxLength(70)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Offre>(entity =>
            {
                entity.HasKey(e => e.IdOffre)
                    .HasName("PK__Offre__DB2D20AB29DEBF42");

                entity.Property(e => e.IdOffre).HasColumnName("id_offre");
            });

            modelBuilder.Entity<OffreFormation>(entity =>
            {
                entity.HasKey(e => e.IdOffreFormation)
                    .HasName("PK__OffreFor__9B7739A15152A970");

                entity.Property(e => e.IdOffreFormation).HasColumnName("id_offre_formation");

                entity.Property(e => e.IdFormation).HasColumnName("id_formation");

                entity.Property(e => e.IdOffre).HasColumnName("id_offre");

                entity.HasOne(d => d.IdFormationNavigation)
                    .WithMany(p => p.OffreFormation)
                    .HasForeignKey(d => d.IdFormation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_formation_offre_formation");

                entity.HasOne(d => d.IdOffreNavigation)
                    .WithMany(p => p.OffreFormation)
                    .HasForeignKey(d => d.IdOffre)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_offre_offre_formation");
            });

            modelBuilder.Entity<OrganismeFinanceur>(entity =>
            {
                entity.HasKey(e => e.IdOrganismeFinanceur)
                    .HasName("PK__Organism__21215E51101122DE");

                entity.Property(e => e.IdOrganismeFinanceur).HasColumnName("id_organisme_financeur");

                entity.Property(e => e.CodeFinanceur).HasColumnName("code_financeur");

                entity.Property(e => e.NbPlaceFinancees).HasColumnName("nb_place_financees");
            });

            modelBuilder.Entity<OrganismeFormateur>(entity =>
            {
                entity.HasKey(e => e.IdOrganismeFormateur)
                    .HasName("PK__Organism__3A2DAB39BCD0FAB6");

                entity.Property(e => e.IdOrganismeFormateur).HasColumnName("id_organisme_formateur");

                entity.Property(e => e.IdContactFormateur).HasColumnName("id_contact_formateur");

                entity.Property(e => e.IdPotentiel).HasColumnName("id_potentiel");

                entity.Property(e => e.IdSiretFormation).HasColumnName("id_siret_formation");

                entity.Property(e => e.RaisonSocialeFormateur)
                    .IsRequired()
                    .HasColumnName("raison_sociale_formateur")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdContactFormateurNavigation)
                    .WithMany(p => p.OrganismeFormateur)
                    .HasForeignKey(d => d.IdContactFormateur)
                    .HasConstraintName("fk_id_contact_formateur_organisme_formateur");

                entity.HasOne(d => d.IdPotentielNavigation)
                    .WithMany(p => p.OrganismeFormateur)
                    .HasForeignKey(d => d.IdPotentiel)
                    .HasConstraintName("fk_id_potentiel_organisme_formateur");

                entity.HasOne(d => d.IdSiretFormationNavigation)
                    .WithMany(p => p.OrganismeFormateur)
                    .HasForeignKey(d => d.IdSiretFormation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_organisme_formateur");
            });

            modelBuilder.Entity<OrganismeFormationResponsable>(entity =>
            {
                entity.HasKey(e => e.IdOrganismeFormationResponsable)
                    .HasName("PK__Organism__0D8786F961B2F2AE");

                entity.Property(e => e.IdOrganismeFormationResponsable).HasColumnName("id_organisme_formation_responsable");

                entity.Property(e => e.AgreementDatadock)
                    .IsRequired()
                    .HasColumnName("agreement_datadock")
                    .HasMaxLength(600)
                    .IsUnicode(false);

                entity.Property(e => e.IdContactOrganisme).HasColumnName("id_contact_organisme");

                entity.Property(e => e.IdCoordonneeOrganisme).HasColumnName("id_coordonnee_organisme");

                entity.Property(e => e.IdPotentiel).HasColumnName("id_potentiel");

                entity.Property(e => e.IdSiret).HasColumnName("id_SIRET");

                entity.Property(e => e.NomOrganisme)
                    .IsRequired()
                    .HasColumnName("nom_organisme")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.NumeroActivite)
                    .IsRequired()
                    .HasColumnName("numero_activite")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.RaisonSociale)
                    .IsRequired()
                    .HasColumnName("raison_sociale")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.RenseignementsSpecifiques)
                    .HasColumnName("renseignements_specifiques")
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdContactOrganismeNavigation)
                    .WithMany(p => p.OrganismeFormationResponsable)
                    .HasForeignKey(d => d.IdContactOrganisme)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_contact_organisme_organisme_formation_responsable");

                entity.HasOne(d => d.IdCoordonneeOrganismeNavigation)
                    .WithMany(p => p.OrganismeFormationResponsable)
                    .HasForeignKey(d => d.IdCoordonneeOrganisme)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_coordonnee_organisme_organisme_formation_responsable");

                entity.HasOne(d => d.IdPotentielNavigation)
                    .WithMany(p => p.OrganismeFormationResponsable)
                    .HasForeignKey(d => d.IdPotentiel)
                    .HasConstraintName("fk_id_potentiel_organisme_formation_responsable");

                entity.HasOne(d => d.IdSiretNavigation)
                    .WithMany(p => p.OrganismeFormationResponsable)
                    .HasForeignKey(d => d.IdSiret)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_SIRET_organisme_formation_responsable");
            });

            modelBuilder.Entity<ParcoursDeFormation>(entity =>
            {
                entity.HasKey(e => e.IdParcoursDeFormation)
                    .HasName("PK__Parcours__DEB444C6DBCFEFF8");

                entity.Property(e => e.IdParcoursDeFormation).HasColumnName("id_parcours_de_formation");

                entity.Property(e => e.Libele)
                    .IsRequired()
                    .HasColumnName("libele")
                    .HasMaxLength(35)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PerimetreRecrutement>(entity =>
            {
                entity.HasKey(e => e.IdPerimetreRecrutement)
                    .HasName("PK__Perimetr__53DB88644F1BB6E0");

                entity.HasIndex(e => e.Cle)
                    .HasName("UK_perimetre_recrutement_cle")
                    .IsUnique();

                entity.Property(e => e.IdPerimetreRecrutement).HasColumnName("id_perimetre_recrutement");

                entity.Property(e => e.Cle).HasColumnName("cle");

                entity.Property(e => e.Label)
                    .IsRequired()
                    .HasColumnName("label")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Periode>(entity =>
            {
                entity.HasKey(e => e.IdPeriode)
                    .HasName("PK__Periode__801188A1DE460E7D");

                entity.Property(e => e.IdPeriode).HasColumnName("id_periode");

                entity.Property(e => e.Debut)
                    .HasColumnName("debut")
                    .HasColumnType("date");

                entity.Property(e => e.Fin)
                    .HasColumnName("fin")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<Positionnement>(entity =>
            {
                entity.HasKey(e => e.IdPositionnement)
                    .HasName("PK__Position__3AE93FE6C2D537A8");

                entity.Property(e => e.IdPositionnement).HasColumnName("id_positionnement");

                entity.Property(e => e.Libele)
                    .HasColumnName("libele")
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Potentiel>(entity =>
            {
                entity.HasKey(e => e.IdPotentiel)
                    .HasName("PK__Potentie__E604E18737F6E377");

                entity.Property(e => e.IdPotentiel).HasColumnName("id_potentiel");
            });

            modelBuilder.Entity<PotentielCodeFormacode>(entity =>
            {
                entity.HasKey(e => e.IdPotentielCodeFormacode)
                    .HasName("PK__Potentie__8976D8796AE60EF5");

                entity.ToTable("PotentielCodeFORMACODE");

                entity.Property(e => e.IdPotentielCodeFormacode).HasColumnName("id_potentiel_code_FORMACODE");

                entity.Property(e => e.IdCodeFormacode).HasColumnName("id_code_FORMACODE");

                entity.Property(e => e.IdPotentiel).HasColumnName("id_potentiel");

                entity.HasOne(d => d.IdCodeFormacodeNavigation)
                    .WithMany(p => p.PotentielCodeFormacode)
                    .HasForeignKey(d => d.IdCodeFormacode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_code_FORMACODE_potentiel_code_FORMACODE");

                entity.HasOne(d => d.IdPotentielNavigation)
                    .WithMany(p => p.PotentielCodeFormacode)
                    .HasForeignKey(d => d.IdPotentiel)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_potentiel_potentiel_code_FORMACODE");
            });

            modelBuilder.Entity<ReferenceModule>(entity =>
            {
                entity.HasKey(e => e.IdReferenceModule)
                    .HasName("PK__Referenc__FC51A4874AAF5D70");

                entity.Property(e => e.IdReferenceModule).HasColumnName("id_reference_module");

                entity.Property(e => e.Reference)
                    .IsRequired()
                    .HasColumnName("reference")
                    .HasMaxLength(3000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Session>(entity =>
            {
                entity.HasKey(e => e.IdSession)
                    .HasName("PK___Session__A9E494D0CA63B626");

                entity.ToTable("_Session");

                entity.Property(e => e.IdSession).HasColumnName("id_session");

                entity.Property(e => e.IdAction).HasColumnName("id_action");

                entity.Property(e => e.IdAdresseInscription).HasColumnName("id_adresse_inscription");

                entity.Property(e => e.IdEtatRecrutement).HasColumnName("id_etat_recrutement");

                entity.Property(e => e.IdPeriode).HasColumnName("id_periode");

                entity.Property(e => e.IdPeriodeInscription).HasColumnName("id_periode_inscription");

                entity.Property(e => e.ModaliteInscription)
                    .IsRequired()
                    .HasColumnName("modalite_inscription")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdActionNavigation)
                    .WithMany(p => p.Session)
                    .HasForeignKey(d => d.IdAction)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_action_session");

                entity.HasOne(d => d.IdAdresseInscriptionNavigation)
                    .WithMany(p => p.Session)
                    .HasForeignKey(d => d.IdAdresseInscription)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_adresse_inscription_session");

                entity.HasOne(d => d.IdEtatRecrutementNavigation)
                    .WithMany(p => p.Session)
                    .HasForeignKey(d => d.IdEtatRecrutement)
                    .HasConstraintName("fk_id_etat_recrutement_session");

                entity.HasOne(d => d.IdPeriodeNavigation)
                    .WithMany(p => p.SessionIdPeriodeNavigation)
                    .HasForeignKey(d => d.IdPeriode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_periode_session");

                entity.HasOne(d => d.IdPeriodeInscriptionNavigation)
                    .WithMany(p => p.SessionIdPeriodeInscriptionNavigation)
                    .HasForeignKey(d => d.IdPeriodeInscription)
                    .HasConstraintName("fk_id_periode_inscription_session");
            });

            modelBuilder.Entity<Siret>(entity =>
            {
                entity.HasKey(e => e.IdSiret)
                    .HasName("PK__SIRET__1FA6B60D38A163A0");

                entity.ToTable("SIRET");

                entity.Property(e => e.IdSiret).HasColumnName("id_SIRET");

                entity.Property(e => e.Siret1)
                    .IsRequired()
                    .HasColumnName("SIRET")
                    .HasMaxLength(14)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SousModule>(entity =>
            {
                entity.HasKey(e => e.IdSousModule)
                    .HasName("PK__SousModu__6262B1334AA3BB25");

                entity.Property(e => e.IdSousModule).HasColumnName("id_sous_module");
            });

            modelBuilder.Entity<SousModuleModule>(entity =>
            {
                entity.HasKey(e => e.IdSousModuleModule)
                    .HasName("PK__SousModu__6580E4F55453929A");

                entity.Property(e => e.IdSousModuleModule).HasColumnName("id_sous_module_module");

                entity.Property(e => e.IdModule).HasColumnName("id_module");

                entity.Property(e => e.IdSousModule).HasColumnName("id_sous_module");

                entity.HasOne(d => d.IdModuleNavigation)
                    .WithMany(p => p.SousModuleModule)
                    .HasForeignKey(d => d.IdModule)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_module_sous_module_module");

                entity.HasOne(d => d.IdSousModuleNavigation)
                    .WithMany(p => p.SousModuleModule)
                    .HasForeignKey(d => d.IdSousModule)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_sous_module_sous_module_module");
            });

            modelBuilder.Entity<TypeModule>(entity =>
            {
                entity.HasKey(e => e.IdTypeModule)
                    .HasName("PK__TypeModu__5C092E22C4EE1D2B");

                entity.Property(e => e.IdTypeModule).HasColumnName("id_type_module");

                entity.Property(e => e.Libele)
                    .HasColumnName("libele")
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UrlAction>(entity =>
            {
                entity.HasKey(e => e.IdUrlAction)
                    .HasName("PK__UrlActio__001AE5C6F4E0E81F");

                entity.Property(e => e.IdUrlAction).HasColumnName("id_url_action");
            });

            modelBuilder.Entity<UrlActionUrlWeb>(entity =>
            {
                entity.HasKey(e => e.IdUrlActionUrlWeb)
                    .HasName("PK__UrlActio__2EF5B4AB1267E34D");

                entity.Property(e => e.IdUrlActionUrlWeb).HasColumnName("id_url_action_url_web");

                entity.Property(e => e.IdUrlAction).HasColumnName("id_url_action");

                entity.Property(e => e.IdUrlWeb).HasColumnName("id_url_web");

                entity.HasOne(d => d.IdUrlActionNavigation)
                    .WithMany(p => p.UrlActionUrlWeb)
                    .HasForeignKey(d => d.IdUrlAction)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_url_action_url_action_url_web");

                entity.HasOne(d => d.IdUrlWebNavigation)
                    .WithMany(p => p.UrlActionUrlWeb)
                    .HasForeignKey(d => d.IdUrlWeb)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_url_web_url_action_url_web");
            });

            modelBuilder.Entity<UrlFormation>(entity =>
            {
                entity.HasKey(e => e.IdUrlFormation)
                    .HasName("PK__UrlForma__5D1FEBB2F0674108");

                entity.Property(e => e.IdUrlFormation).HasColumnName("id_url_formation");
            });

            modelBuilder.Entity<UrlFormationUrlWeb>(entity =>
            {
                entity.HasKey(e => e.IdUrlFormationUrlWeb)
                    .HasName("PK__UrlForma__C19DC4C4D7E2A508");

                entity.Property(e => e.IdUrlFormationUrlWeb).HasColumnName("id_url_formation_url_web");

                entity.Property(e => e.IdUrlFormation).HasColumnName("id_url_formation");

                entity.Property(e => e.IdUrlWeb).HasColumnName("id_url_web");

                entity.HasOne(d => d.IdUrlFormationNavigation)
                    .WithMany(p => p.UrlFormationUrlWeb)
                    .HasForeignKey(d => d.IdUrlFormation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_url_formation_url_formation_url_web");

                entity.HasOne(d => d.IdUrlWebNavigation)
                    .WithMany(p => p.UrlFormationUrlWeb)
                    .HasForeignKey(d => d.IdUrlWeb)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_url_web_url_formation_url_web");
            });

            modelBuilder.Entity<UrlWeb>(entity =>
            {
                entity.HasKey(e => e.IdUrlWeb)
                    .HasName("PK__UrlWeb__3961222CAA6895C8");

                entity.Property(e => e.IdUrlWeb).HasColumnName("id_url_web");

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasColumnName("url")
                    .HasMaxLength(400)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Web>(entity =>
            {
                entity.HasKey(e => e.IdWeb)
                    .HasName("PK__Web__6C6DA992846C5BF6");

                entity.Property(e => e.IdWeb).HasColumnName("id_web");
            });

            modelBuilder.Entity<WebUrlWeb>(entity =>
            {
                entity.HasKey(e => e.IdWebUrlWeb)
                    .HasName("PK__WebUrlWe__E196487BC74A34E9");

                entity.Property(e => e.IdWebUrlWeb).HasColumnName("id_web_url_web");

                entity.Property(e => e.IdUrlWeb).HasColumnName("id_url_web");

                entity.Property(e => e.IdWeb).HasColumnName("id_web");

                entity.HasOne(d => d.IdUrlWebNavigation)
                    .WithMany(p => p.WebUrlWeb)
                    .HasForeignKey(d => d.IdUrlWeb)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_url_web_web_url_web");

                entity.HasOne(d => d.IdWebNavigation)
                    .WithMany(p => p.WebUrlWeb)
                    .HasForeignKey(d => d.IdWeb)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_web_web_url_web");
            });
        }
    }
}
