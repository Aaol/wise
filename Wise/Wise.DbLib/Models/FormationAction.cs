﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class FormationAction
    {
        public int IdFormationAction { get; set; }
        public int IdFormation { get; set; }
        public int IdAction { get; set; }

        public virtual Action IdActionNavigation { get; set; }
        public virtual Formation IdFormationNavigation { get; set; }
    }
}
