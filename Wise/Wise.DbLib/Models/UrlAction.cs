﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class UrlAction
    {
        public UrlAction()
        {
            Extra = new HashSet<Extra>();
            UrlActionUrlWeb = new HashSet<UrlActionUrlWeb>();
        }

        public int IdUrlAction { get; set; }

        public virtual ICollection<Extra> Extra { get; set; }
        public virtual ICollection<UrlActionUrlWeb> UrlActionUrlWeb { get; set; }
    }
}
