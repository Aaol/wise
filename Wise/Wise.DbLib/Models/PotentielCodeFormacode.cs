﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class PotentielCodeFormacode
    {
        public int IdPotentielCodeFormacode { get; set; }
        public int IdPotentiel { get; set; }
        public int IdCodeFormacode { get; set; }

        public virtual CodeFormacode IdCodeFormacodeNavigation { get; set; }
        public virtual Potentiel IdPotentielNavigation { get; set; }
    }
}
