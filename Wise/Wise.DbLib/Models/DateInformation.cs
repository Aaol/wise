﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class DateInformation
    {
        public DateInformation()
        {
            Extra = new HashSet<Extra>();
        }

        public int IdDateInformation { get; set; }
        public DateTime DateInformation1 { get; set; }
        public int IdAction { get; set; }

        public virtual Action IdActionNavigation { get; set; }
        public virtual ICollection<Extra> Extra { get; set; }
    }
}
