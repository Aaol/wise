﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Web
    {
        public Web()
        {
            Action = new HashSet<Action>();
            Coordonnee = new HashSet<Coordonnee>();
            Extra = new HashSet<Extra>();
            WebUrlWeb = new HashSet<WebUrlWeb>();
        }

        public int IdWeb { get; set; }

        public virtual ICollection<Action> Action { get; set; }
        public virtual ICollection<Coordonnee> Coordonnee { get; set; }
        public virtual ICollection<Extra> Extra { get; set; }
        public virtual ICollection<WebUrlWeb> WebUrlWeb { get; set; }
    }
}
