﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class OffreFormation
    {
        public int IdOffreFormation { get; set; }
        public int IdOffre { get; set; }
        public int IdFormation { get; set; }

        public virtual Formation IdFormationNavigation { get; set; }
        public virtual Offre IdOffreNavigation { get; set; }
    }
}
