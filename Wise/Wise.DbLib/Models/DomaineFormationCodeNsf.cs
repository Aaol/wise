﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class DomaineFormationCodeNsf
    {
        public int IdDomaineFormationCodeNsf { get; set; }
        public int IdDomaineFormation { get; set; }
        public int IdCodeNsf { get; set; }

        public virtual CodeNsf IdCodeNsfNavigation { get; set; }
        public virtual DomaineFormation IdDomaineFormationNavigation { get; set; }
    }
}
