﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Session
    {
        public int IdSession { get; set; }
        public int IdAction { get; set; }
        public int IdPeriode { get; set; }
        public int IdAdresseInscription { get; set; }
        public string ModaliteInscription { get; set; }
        public int? IdPeriodeInscription { get; set; }
        public int? IdEtatRecrutement { get; set; }

        public virtual Action IdActionNavigation { get; set; }
        public virtual Adresse IdAdresseInscriptionNavigation { get; set; }
        public virtual EtatRecrutement IdEtatRecrutementNavigation { get; set; }
        public virtual Periode IdPeriodeInscriptionNavigation { get; set; }
        public virtual Periode IdPeriodeNavigation { get; set; }
    }
}
