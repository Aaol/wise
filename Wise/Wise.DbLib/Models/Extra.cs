﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Extra
    {
        public int IdExtra { get; set; }
        public int? IdLheo { get; set; }
        public int? IdOffre { get; set; }
        public int? IdFormation { get; set; }
        public int? IdDomaineFormation { get; set; }
        public int? IdContactFormation { get; set; }
        public int? IdCoordonnee { get; set; }
        public int? IdAdresse { get; set; }
        public int? IdGeolocalisation { get; set; }
        public int? IdWeb { get; set; }
        public int? IdCertification { get; set; }
        public int? IdSousModule { get; set; }
        public int? IdModule { get; set; }
        public int? IdModulesPrerequis { get; set; }
        public int? IdOrganismeFormationResponsable { get; set; }
        public int? IdSiret { get; set; }
        public int? IdCoordonneeOrganisme { get; set; }
        public int? IdContactOrganisme { get; set; }
        public int? IdPotentiel { get; set; }
        public int? IdAction { get; set; }
        public int? IdUrlAction { get; set; }
        public int? IdPeriode { get; set; }
        public int? IdAdresseInscription { get; set; }
        public int? IdDateInformation { get; set; }
        public int? IdOrganismeFormateur { get; set; }
        public int? IdContactFormateur { get; set; }
        public int? IdOrganismeFinanceur { get; set; }
        public string Commentaire { get; set; }

        public virtual Action IdActionNavigation { get; set; }
        public virtual AdresseInscription IdAdresseInscriptionNavigation { get; set; }
        public virtual Adresse IdAdresseNavigation { get; set; }
        public virtual Certification IdCertificationNavigation { get; set; }
        public virtual ContactFormateur IdContactFormateurNavigation { get; set; }
        public virtual ContactFormation IdContactFormationNavigation { get; set; }
        public virtual ContactOrganisme IdContactOrganismeNavigation { get; set; }
        public virtual Coordonnee IdCoordonneeNavigation { get; set; }
        public virtual CoordonneeOrganisme IdCoordonneeOrganismeNavigation { get; set; }
        public virtual DateInformation IdDateInformationNavigation { get; set; }
        public virtual DomaineFormation IdDomaineFormationNavigation { get; set; }
        public virtual Formation IdFormationNavigation { get; set; }
        public virtual Geolocalisation IdGeolocalisationNavigation { get; set; }
        public virtual Lheo IdLheoNavigation { get; set; }
        public virtual Module IdModuleNavigation { get; set; }
        public virtual ModulePrerequis IdModulesPrerequisNavigation { get; set; }
        public virtual Offre IdOffreNavigation { get; set; }
        public virtual OrganismeFinanceur IdOrganismeFinanceurNavigation { get; set; }
        public virtual OrganismeFormateur IdOrganismeFormateurNavigation { get; set; }
        public virtual OrganismeFormationResponsable IdOrganismeFormationResponsableNavigation { get; set; }
        public virtual Periode IdPeriodeNavigation { get; set; }
        public virtual Potentiel IdPotentielNavigation { get; set; }
        public virtual Siret IdSiretNavigation { get; set; }
        public virtual SousModule IdSousModuleNavigation { get; set; }
        public virtual UrlAction IdUrlActionNavigation { get; set; }
        public virtual Web IdWebNavigation { get; set; }
    }
}
