﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class CodeRome
    {
        public CodeRome()
        {
            DomaineFormationCodeRome = new HashSet<DomaineFormationCodeRome>();
        }

        public int IdCodeRome { get; set; }
        public string Code { get; set; }

        public virtual ICollection<DomaineFormationCodeRome> DomaineFormationCodeRome { get; set; }
    }
}
