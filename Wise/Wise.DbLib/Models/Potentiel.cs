﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Potentiel
    {
        public Potentiel()
        {
            Extra = new HashSet<Extra>();
            OrganismeFormateur = new HashSet<OrganismeFormateur>();
            OrganismeFormationResponsable = new HashSet<OrganismeFormationResponsable>();
            PotentielCodeFormacode = new HashSet<PotentielCodeFormacode>();
        }

        public int IdPotentiel { get; set; }

        public virtual ICollection<Extra> Extra { get; set; }
        public virtual ICollection<OrganismeFormateur> OrganismeFormateur { get; set; }
        public virtual ICollection<OrganismeFormationResponsable> OrganismeFormationResponsable { get; set; }
        public virtual ICollection<PotentielCodeFormacode> PotentielCodeFormacode { get; set; }
    }
}
