﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class PerimetreRecrutement
    {
        public PerimetreRecrutement()
        {
            Action = new HashSet<Action>();
        }

        public int IdPerimetreRecrutement { get; set; }
        public string Label { get; set; }
        public int Cle { get; set; }

        public virtual ICollection<Action> Action { get; set; }
    }
}
