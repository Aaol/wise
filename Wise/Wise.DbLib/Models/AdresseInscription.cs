﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class AdresseInscription
    {
        public AdresseInscription()
        {
            Extra = new HashSet<Extra>();
        }

        public int IdAdresseInscription { get; set; }
        public int IdAdresse { get; set; }

        public virtual Adresse IdAdresseNavigation { get; set; }
        public virtual ICollection<Extra> Extra { get; set; }
    }
}
