﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Numtel
    {
        public Numtel()
        {
            ContactTel = new HashSet<ContactTel>();
        }

        public int IdNumtel { get; set; }
        public string Numero { get; set; }

        public virtual ICollection<ContactTel> ContactTel { get; set; }
    }
}
