﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Formation
    {
        public Formation()
        {
            Extra = new HashSet<Extra>();
            FormationAction = new HashSet<FormationAction>();
            FormationCertification = new HashSet<FormationCertification>();
            OffreFormation = new HashSet<OffreFormation>();
        }

        public int IdFormation { get; set; }
        public int IdDomaineFormation { get; set; }
        public string IntituleFormation { get; set; }
        public string ObjectifFormation { get; set; }
        public string ResultatsAttendus { get; set; }
        public string ContenuFormation { get; set; }
        public bool Certifiante { get; set; }
        public int IdContactFormation { get; set; }
        public int IdParcoursDeFormation { get; set; }
        public int IdNiveauxEntree { get; set; }
        public int? IdObjectifGeneralFormation { get; set; }
        public int? IdCodeNiveauSortie { get; set; }
        public int? IdUrlFormation { get; set; }
        public int IdOrganismeFormationResponsable { get; set; }
        public string IdentifiantModule { get; set; }
        public int? IdPositionnement { get; set; }
        public int? IdSousModule { get; set; }
        public int? IdModulesPrerequis { get; set; }
        public int? CreditsEcts { get; set; }
        public string EligibiliteCpf { get; set; }
        public string Validations { get; set; }

        public virtual Niveaux IdCodeNiveauSortieNavigation { get; set; }
        public virtual ContactFormation IdContactFormationNavigation { get; set; }
        public virtual DomaineFormation IdDomaineFormationNavigation { get; set; }
        public virtual ModulePrerequis IdModulesPrerequisNavigation { get; set; }
        public virtual Niveaux IdNiveauxEntreeNavigation { get; set; }
        public virtual ObjectifGeneralFormation IdObjectifGeneralFormationNavigation { get; set; }
        public virtual OrganismeFormationResponsable IdOrganismeFormationResponsableNavigation { get; set; }
        public virtual ParcoursDeFormation IdParcoursDeFormationNavigation { get; set; }
        public virtual Positionnement IdPositionnementNavigation { get; set; }
        public virtual SousModule IdSousModuleNavigation { get; set; }
        public virtual UrlFormation IdUrlFormationNavigation { get; set; }
        public virtual ICollection<Extra> Extra { get; set; }
        public virtual ICollection<FormationAction> FormationAction { get; set; }
        public virtual ICollection<FormationCertification> FormationCertification { get; set; }
        public virtual ICollection<OffreFormation> OffreFormation { get; set; }
    }
}
