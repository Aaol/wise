﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class ActionOrganismeFinanceur
    {
        public int IdActionOrganismeFinanceur { get; set; }
        public int IdAction { get; set; }
        public int IdOrganismeFinanceur { get; set; }

        public virtual Action IdActionNavigation { get; set; }
        public virtual OrganismeFinanceur IdOrganismeFinanceurNavigation { get; set; }
    }
}
