using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Wise.DbLib.Models
{
    /// <summary>
    ///     Classe définissant un utilisateur
    /// </summary>
    public class User
    {
        #region Properties

        /// <summary>
        ///     Identifiant de l'utilisateur
        /// </summary>
        [Key]
        public int IdUser { get; set; }
        
        /// <summary>
        ///     Address Email de l'utilisateur
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///     Hash du mot de passe de l'utilisateur
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        ///     Rôle de l'utilisateur
        /// </summary>
        public string Role { get; set; }

        /// <summary>
        ///     Sel de l'utilisateur utilisé pour hasher le mot de passe.
        /// </summary>
        public string Salt { get; set; }

        /// <summary>
        ///     Indique si l'utilisateur est validé ou non.
        /// </summary>
        public bool Validated { get; set; }

        #endregion
    }
}
