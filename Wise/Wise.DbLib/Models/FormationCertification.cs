﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class FormationCertification
    {
        public int IdFormationCertification { get; set; }
        public int IdFormation { get; set; }
        public int IdCertification { get; set; }

        public virtual Certification IdCertificationNavigation { get; set; }
        public virtual Formation IdFormationNavigation { get; set; }
    }
}
