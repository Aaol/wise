﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class ObjectifGeneralFormation
    {
        public ObjectifGeneralFormation()
        {
            Formation = new HashSet<Formation>();
        }

        public int IdObjectifGeneralFormation { get; set; }
        public string Libele { get; set; }

        public virtual ICollection<Formation> Formation { get; set; }
    }
}
