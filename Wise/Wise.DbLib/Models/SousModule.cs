﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class SousModule
    {
        public SousModule()
        {
            Extra = new HashSet<Extra>();
            Formation = new HashSet<Formation>();
            SousModuleModule = new HashSet<SousModuleModule>();
        }

        public int IdSousModule { get; set; }

        public virtual ICollection<Extra> Extra { get; set; }
        public virtual ICollection<Formation> Formation { get; set; }
        public virtual ICollection<SousModuleModule> SousModuleModule { get; set; }
    }
}
