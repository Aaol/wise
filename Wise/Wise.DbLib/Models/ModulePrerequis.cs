﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class ModulePrerequis
    {
        public ModulePrerequis()
        {
            Extra = new HashSet<Extra>();
            Formation = new HashSet<Formation>();
            ModulePrerequisReferenceModule = new HashSet<ModulePrerequisReferenceModule>();
        }

        public int IdModulesPrerequis { get; set; }

        public virtual ICollection<Extra> Extra { get; set; }
        public virtual ICollection<Formation> Formation { get; set; }
        public virtual ICollection<ModulePrerequisReferenceModule> ModulePrerequisReferenceModule { get; set; }
    }
}
