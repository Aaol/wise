﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class SousModuleModule
    {
        public int IdSousModuleModule { get; set; }
        public int IdSousModule { get; set; }
        public int IdModule { get; set; }

        public virtual Module IdModuleNavigation { get; set; }
        public virtual SousModule IdSousModuleNavigation { get; set; }
    }
}
