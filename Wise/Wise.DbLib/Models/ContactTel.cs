﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class ContactTel
    {
        public ContactTel()
        {
            CoordonneeIdFaxNavigation = new HashSet<Coordonnee>();
            CoordonneeIdPortableNavigation = new HashSet<Coordonnee>();
            CoordonneeIdTelfixeNavigation = new HashSet<Coordonnee>();
        }

        public int IdContactTel { get; set; }
        public int IdNumtel { get; set; }

        public virtual Numtel IdNumtelNavigation { get; set; }
        public virtual ICollection<Coordonnee> CoordonneeIdFaxNavigation { get; set; }
        public virtual ICollection<Coordonnee> CoordonneeIdPortableNavigation { get; set; }
        public virtual ICollection<Coordonnee> CoordonneeIdTelfixeNavigation { get; set; }
    }
}
