﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Offre
    {
        public Offre()
        {
            Extra = new HashSet<Extra>();
            Lheo = new HashSet<Lheo>();
            OffreFormation = new HashSet<OffreFormation>();
        }

        public int IdOffre { get; set; }

        public virtual ICollection<Extra> Extra { get; set; }
        public virtual ICollection<Lheo> Lheo { get; set; }
        public virtual ICollection<OffreFormation> OffreFormation { get; set; }
    }
}
