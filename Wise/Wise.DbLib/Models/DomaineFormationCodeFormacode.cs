﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class DomaineFormationCodeFormacode
    {
        public int IdDomaineFormationCodeFormacode { get; set; }
        public int IdDomaineFormation { get; set; }
        public int IdCodeFormacode { get; set; }

        public virtual CodeFormacode IdCodeFormacodeNavigation { get; set; }
        public virtual DomaineFormation IdDomaineFormationNavigation { get; set; }
    }
}
