﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class UrlFormationUrlWeb
    {
        public int IdUrlFormationUrlWeb { get; set; }
        public int IdUrlFormation { get; set; }
        public int IdUrlWeb { get; set; }

        public virtual UrlFormation IdUrlFormationNavigation { get; set; }
        public virtual UrlWeb IdUrlWebNavigation { get; set; }
    }
}
