﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class CodeFormacode
    {
        public CodeFormacode()
        {
            DomaineFormationCodeFormacode = new HashSet<DomaineFormationCodeFormacode>();
            PotentielCodeFormacode = new HashSet<PotentielCodeFormacode>();
        }

        public int IdCodeFormacode { get; set; }
        public string Code { get; set; }

        public virtual ICollection<DomaineFormationCodeFormacode> DomaineFormationCodeFormacode { get; set; }
        public virtual ICollection<PotentielCodeFormacode> PotentielCodeFormacode { get; set; }
    }
}
