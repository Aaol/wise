﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Module
    {
        public Module()
        {
            Extra = new HashSet<Extra>();
            SousModuleModule = new HashSet<SousModuleModule>();
        }

        public int IdModule { get; set; }
        public string ReferenceModule { get; set; }
        public int IdTypeModule { get; set; }

        public virtual TypeModule IdTypeModuleNavigation { get; set; }
        public virtual ICollection<Extra> Extra { get; set; }
        public virtual ICollection<SousModuleModule> SousModuleModule { get; set; }
    }
}
