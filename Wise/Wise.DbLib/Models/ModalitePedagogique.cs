﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class ModalitePedagogique
    {
        public int IdModalitePedagogique { get; set; }
        public int IdAction { get; set; }
        public string Label { get; set; }

        public virtual Action IdActionNavigation { get; set; }
    }
}
