﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class UrlActionUrlWeb
    {
        public int IdUrlActionUrlWeb { get; set; }
        public int IdUrlAction { get; set; }
        public int IdUrlWeb { get; set; }

        public virtual UrlAction IdUrlActionNavigation { get; set; }
        public virtual UrlWeb IdUrlWebNavigation { get; set; }
    }
}
