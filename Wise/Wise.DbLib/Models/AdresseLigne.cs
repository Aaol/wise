﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class AdresseLigne
    {
        public int IdAdresseLigne { get; set; }
        public int IdLigne { get; set; }
        public int IdAdresse { get; set; }

        public virtual Adresse IdAdresseNavigation { get; set; }
        public virtual Ligne IdLigneNavigation { get; set; }
    }
}
