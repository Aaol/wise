﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class ReferenceModule
    {
        public ReferenceModule()
        {
            ModulePrerequisReferenceModule = new HashSet<ModulePrerequisReferenceModule>();
        }

        public int IdReferenceModule { get; set; }
        public string Reference { get; set; }

        public virtual ICollection<ModulePrerequisReferenceModule> ModulePrerequisReferenceModule { get; set; }
    }
}
