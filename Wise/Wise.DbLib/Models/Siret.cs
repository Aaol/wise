using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Siret
    {
        public Siret()
        {
            Extra = new HashSet<Extra>();
            OrganismeFormateur = new HashSet<OrganismeFormateur>();
            OrganismeFormationResponsable = new HashSet<OrganismeFormationResponsable>();
        }

        public int IdSiret { get; set; }
        public string Siret1 { get; set; }

        public virtual ICollection<Extra> Extra { get; set; }
        public virtual ICollection<OrganismeFormateur> OrganismeFormateur { get; set; }
        public virtual ICollection<OrganismeFormationResponsable> OrganismeFormationResponsable { get; set; }
    }
}
