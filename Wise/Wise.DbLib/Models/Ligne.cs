﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Ligne
    {
        public Ligne()
        {
            AdresseLigne = new HashSet<AdresseLigne>();
            CoordonneeLigne = new HashSet<CoordonneeLigne>();
        }

        public int IdLigne { get; set; }
        public string Ligne1 { get; set; }

        public virtual ICollection<AdresseLigne> AdresseLigne { get; set; }
        public virtual ICollection<CoordonneeLigne> CoordonneeLigne { get; set; }
    }
}
