﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class LieuDeFormation
    {
        public LieuDeFormation()
        {
            Action = new HashSet<Action>();
        }

        public int IdLieuDeFormation { get; set; }
        public int IdCoordonnee { get; set; }

        public virtual Coordonnee IdCoordonneeNavigation { get; set; }
        public virtual ICollection<Action> Action { get; set; }
    }
}
