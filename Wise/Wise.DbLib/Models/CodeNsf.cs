﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class CodeNsf
    {
        public CodeNsf()
        {
            DomaineFormationCodeNsf = new HashSet<DomaineFormationCodeNsf>();
        }

        public int IdCodeNsf { get; set; }
        public string Code { get; set; }

        public virtual ICollection<DomaineFormationCodeNsf> DomaineFormationCodeNsf { get; set; }
    }
}
