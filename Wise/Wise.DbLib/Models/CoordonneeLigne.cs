﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class CoordonneeLigne
    {
        public int IdCoordonneeLigne { get; set; }
        public int IdLigne { get; set; }
        public int IdCoordonnee { get; set; }

        public virtual Coordonnee IdCoordonneeNavigation { get; set; }
        public virtual Ligne IdLigneNavigation { get; set; }
    }
}
