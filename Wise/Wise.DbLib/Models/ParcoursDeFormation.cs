﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class ParcoursDeFormation
    {
        public ParcoursDeFormation()
        {
            Formation = new HashSet<Formation>();
        }

        public int IdParcoursDeFormation { get; set; }
        public string Libele { get; set; }

        public virtual ICollection<Formation> Formation { get; set; }
    }
}
