﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class WebUrlWeb
    {
        public int IdWebUrlWeb { get; set; }
        public int IdWeb { get; set; }
        public int IdUrlWeb { get; set; }

        public virtual UrlWeb IdUrlWebNavigation { get; set; }
        public virtual Web IdWebNavigation { get; set; }
    }
}
