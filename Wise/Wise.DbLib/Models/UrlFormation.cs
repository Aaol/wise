﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class UrlFormation
    {
        public UrlFormation()
        {
            Formation = new HashSet<Formation>();
            UrlFormationUrlWeb = new HashSet<UrlFormationUrlWeb>();
        }

        public int IdUrlFormation { get; set; }

        public virtual ICollection<Formation> Formation { get; set; }
        public virtual ICollection<UrlFormationUrlWeb> UrlFormationUrlWeb { get; set; }
    }
}
