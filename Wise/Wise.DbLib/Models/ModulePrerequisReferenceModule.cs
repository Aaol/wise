﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class ModulePrerequisReferenceModule
    {
        public int IdModulePrerequisReferenceModule { get; set; }
        public int IdModulesPrerequis { get; set; }
        public int IdReferenceModule { get; set; }

        public virtual ModulePrerequis IdModulesPrerequisNavigation { get; set; }
        public virtual ReferenceModule IdReferenceModuleNavigation { get; set; }
    }
}
