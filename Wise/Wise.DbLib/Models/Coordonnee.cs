﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Coordonnee
    {
        public Coordonnee()
        {
            Action = new HashSet<Action>();
            ContactFormateur = new HashSet<ContactFormateur>();
            ContactFormation = new HashSet<ContactFormation>();
            ContactOrganisme = new HashSet<ContactOrganisme>();
            CoordonneeLigne = new HashSet<CoordonneeLigne>();
            CoordonneeOrganisme = new HashSet<CoordonneeOrganisme>();
            Extra = new HashSet<Extra>();
            LieuDeFormation = new HashSet<LieuDeFormation>();
        }

        public int IdCoordonnee { get; set; }
        public string Civilite { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public int? IdAdresse { get; set; }
        public int IdTelfixe { get; set; }
        public int IdPortable { get; set; }
        public int IdFax { get; set; }
        public string Courriel { get; set; }
        public int? IdWeb { get; set; }

        public virtual Adresse IdAdresseNavigation { get; set; }
        public virtual ContactTel IdFaxNavigation { get; set; }
        public virtual ContactTel IdPortableNavigation { get; set; }
        public virtual ContactTel IdTelfixeNavigation { get; set; }
        public virtual Web IdWebNavigation { get; set; }
        public virtual ICollection<Action> Action { get; set; }
        public virtual ICollection<ContactFormateur> ContactFormateur { get; set; }
        public virtual ICollection<ContactFormation> ContactFormation { get; set; }
        public virtual ICollection<ContactOrganisme> ContactOrganisme { get; set; }
        public virtual ICollection<CoordonneeLigne> CoordonneeLigne { get; set; }
        public virtual ICollection<CoordonneeOrganisme> CoordonneeOrganisme { get; set; }
        public virtual ICollection<Extra> Extra { get; set; }
        public virtual ICollection<LieuDeFormation> LieuDeFormation { get; set; }
    }
}
