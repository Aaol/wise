﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class UrlWeb
    {
        public UrlWeb()
        {
            UrlActionUrlWeb = new HashSet<UrlActionUrlWeb>();
            UrlFormationUrlWeb = new HashSet<UrlFormationUrlWeb>();
            WebUrlWeb = new HashSet<WebUrlWeb>();
        }

        public int IdUrlWeb { get; set; }
        public string Url { get; set; }

        public virtual ICollection<UrlActionUrlWeb> UrlActionUrlWeb { get; set; }
        public virtual ICollection<UrlFormationUrlWeb> UrlFormationUrlWeb { get; set; }
        public virtual ICollection<WebUrlWeb> WebUrlWeb { get; set; }
    }
}
