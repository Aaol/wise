﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Wise.DbLib.Migrations
{
    public partial class CreateDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Certification",
                columns: table => new
                {
                    id_certification = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code_RNCP = table.Column<string>(unicode: false, maxLength: 6, nullable: true),
                    code_CERTIFINFO = table.Column<string>(unicode: false, maxLength: 6, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Certific__5476C1F4B1D65EC6", x => x.id_certification);
                });

            migrationBuilder.CreateTable(
                name: "CodeFORMACODE",
                columns: table => new
                {
                    id_code_FORMACODE = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(unicode: false, maxLength: 5, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__CodeFORM__BD446C4251A5980C", x => x.id_code_FORMACODE);
                });

            migrationBuilder.CreateTable(
                name: "CodeNSF",
                columns: table => new
                {
                    id_code_NSF = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(unicode: false, maxLength: 3, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__CodeNSF__C16CF64635FBB9B8", x => x.id_code_NSF);
                });

            migrationBuilder.CreateTable(
                name: "CodeROME",
                columns: table => new
                {
                    id_code_ROME = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(unicode: false, maxLength: 5, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__CodeROME__D6BFC2E41DCAB306", x => x.id_code_ROME);
                });

            migrationBuilder.CreateTable(
                name: "DomaineFormation",
                columns: table => new
                {
                    id_domaine_formation = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__DomaineF__3F492C6A5C76DCEC", x => x.id_domaine_formation);
                });

            migrationBuilder.CreateTable(
                name: "EtatRecrutement",
                columns: table => new
                {
                    id_etat_recrutement = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    label = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    recrutement_key = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__EtatRecr__1FAFCBDE36242827", x => x.id_etat_recrutement);
                });

            migrationBuilder.CreateTable(
                name: "Geolocalisation",
                columns: table => new
                {
                    id_geolocalisation = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    latitude = table.Column<string>(unicode: false, maxLength: 30, nullable: false),
                    longitude = table.Column<string>(unicode: false, maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Geolocal__4EB547FEA8A3CF96", x => x.id_geolocalisation);
                });

            migrationBuilder.CreateTable(
                name: "Ligne",
                columns: table => new
                {
                    id_ligne = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ligne = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Ligne__E1D60C0CB48E0740", x => x.id_ligne);
                });

            migrationBuilder.CreateTable(
                name: "ModaliteEnseignement",
                columns: table => new
                {
                    id_modalite_enseignement = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    label = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    modalite_key = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Modalite__3FC9D032D272CBC8", x => x.id_modalite_enseignement);
                });

            migrationBuilder.CreateTable(
                name: "ModulePrerequis",
                columns: table => new
                {
                    id_modules_prerequis = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__ModulePr__59CA7BEDB89CAE01", x => x.id_modules_prerequis);
                });

            migrationBuilder.CreateTable(
                name: "Niveaux",
                columns: table => new
                {
                    id_niveaux = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    libele = table.Column<string>(unicode: false, maxLength: 65, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Niveaux__E209DB1585572B1B", x => x.id_niveaux);
                });

            migrationBuilder.CreateTable(
                name: "Numtel",
                columns: table => new
                {
                    id_numtel = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    numero = table.Column<string>(unicode: false, maxLength: 25, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Numtel__FDE233E733FF21E3", x => x.id_numtel);
                });

            migrationBuilder.CreateTable(
                name: "ObjectifGeneralFormation",
                columns: table => new
                {
                    id_objectif_general_formation = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    libele = table.Column<string>(unicode: false, maxLength: 70, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Objectif__52EC2FA750D23524", x => x.id_objectif_general_formation);
                });

            migrationBuilder.CreateTable(
                name: "Offre",
                columns: table => new
                {
                    id_offre = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Offre__DB2D20AB29DEBF42", x => x.id_offre);
                });

            migrationBuilder.CreateTable(
                name: "OrganismeFinanceur",
                columns: table => new
                {
                    id_organisme_financeur = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code_financeur = table.Column<int>(nullable: false),
                    nb_place_financees = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Organism__21215E51101122DE", x => x.id_organisme_financeur);
                });

            migrationBuilder.CreateTable(
                name: "ParcoursDeFormation",
                columns: table => new
                {
                    id_parcours_de_formation = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    libele = table.Column<string>(unicode: false, maxLength: 35, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Parcours__DEB444C6DBCFEFF8", x => x.id_parcours_de_formation);
                });

            migrationBuilder.CreateTable(
                name: "PerimetreRecrutement",
                columns: table => new
                {
                    id_perimetre_recrutement = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    label = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    cle = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Perimetr__53DB88644F1BB6E0", x => x.id_perimetre_recrutement);
                    table.UniqueConstraint("AK_PerimetreRecrutement_cle", x => x.cle);
                });

            migrationBuilder.CreateTable(
                name: "Periode",
                columns: table => new
                {
                    id_periode = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    debut = table.Column<DateTime>(type: "date", nullable: false),
                    fin = table.Column<DateTime>(type: "date", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Periode__801188A1DE460E7D", x => x.id_periode);
                });

            migrationBuilder.CreateTable(
                name: "Positionnement",
                columns: table => new
                {
                    id_positionnement = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    libele = table.Column<string>(unicode: false, maxLength: 15, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Position__3AE93FE6C2D537A8", x => x.id_positionnement);
                });

            migrationBuilder.CreateTable(
                name: "Potentiel",
                columns: table => new
                {
                    id_potentiel = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Potentie__E604E18737F6E377", x => x.id_potentiel);
                });

            migrationBuilder.CreateTable(
                name: "ReferenceModule",
                columns: table => new
                {
                    id_reference_module = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    reference = table.Column<string>(unicode: false, maxLength: 3000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Referenc__FC51A4874AAF5D70", x => x.id_reference_module);
                });

            migrationBuilder.CreateTable(
                name: "SIRET",
                columns: table => new
                {
                    id_SIRET = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    SIRET = table.Column<string>(unicode: false, maxLength: 14, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__SIRET__1FA6B60D38A163A0", x => x.id_SIRET);
                });

            migrationBuilder.CreateTable(
                name: "SousModule",
                columns: table => new
                {
                    id_sous_module = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__SousModu__6262B1334AA3BB25", x => x.id_sous_module);
                });

            migrationBuilder.CreateTable(
                name: "TypeModule",
                columns: table => new
                {
                    id_type_module = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    libele = table.Column<string>(unicode: false, maxLength: 25, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__TypeModu__5C092E22C4EE1D2B", x => x.id_type_module);
                });

            migrationBuilder.CreateTable(
                name: "UrlAction",
                columns: table => new
                {
                    id_url_action = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__UrlActio__001AE5C6F4E0E81F", x => x.id_url_action);
                });

            migrationBuilder.CreateTable(
                name: "UrlFormation",
                columns: table => new
                {
                    id_url_formation = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__UrlForma__5D1FEBB2F0674108", x => x.id_url_formation);
                });

            migrationBuilder.CreateTable(
                name: "UrlWeb",
                columns: table => new
                {
                    id_url_web = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    url = table.Column<string>(unicode: false, maxLength: 400, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__UrlWeb__3961222CAA6895C8", x => x.id_url_web);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    IdUser = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Email = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Role = table.Column<string>(nullable: true),
                    Salt = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.IdUser);
                });

            migrationBuilder.CreateTable(
                name: "Web",
                columns: table => new
                {
                    id_web = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Web__6C6DA992846C5BF6", x => x.id_web);
                });

            migrationBuilder.CreateTable(
                name: "DomaineFormationCodeFORMACODE",
                columns: table => new
                {
                    id_domaine_formation_code_FORMACODE = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_domaine_formation = table.Column<int>(nullable: false),
                    id_code_FORMACODE = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__DomaineF__089B4C1C894CF4F4", x => x.id_domaine_formation_code_FORMACODE);
                    table.ForeignKey(
                        name: "fk_id_code_FORMACODE_domaine_formation_code_FORMACODE",
                        column: x => x.id_code_FORMACODE,
                        principalTable: "CodeFORMACODE",
                        principalColumn: "id_code_FORMACODE",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_domaine_formation_domaine_formation_code_FORMACODE",
                        column: x => x.id_domaine_formation,
                        principalTable: "DomaineFormation",
                        principalColumn: "id_domaine_formation",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DomaineFormationCodeNSF",
                columns: table => new
                {
                    id_domaine_formation_code_NSF = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_domaine_formation = table.Column<int>(nullable: false),
                    id_code_NSF = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__DomaineF__A612B75B2AD25A3E", x => x.id_domaine_formation_code_NSF);
                    table.ForeignKey(
                        name: "fk_id_code_NSF_domaine_formation_code_NSF",
                        column: x => x.id_code_NSF,
                        principalTable: "CodeNSF",
                        principalColumn: "id_code_NSF",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_domaine_formation_domaine_formation_code_NSF",
                        column: x => x.id_domaine_formation,
                        principalTable: "DomaineFormation",
                        principalColumn: "id_domaine_formation",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DomaineFormationCodeROME",
                columns: table => new
                {
                    id_domaine_formation_code_ROME = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_domaine_formation = table.Column<int>(nullable: false),
                    id_code_ROME = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__DomaineF__BBA6C91E3738AEAD", x => x.id_domaine_formation_code_ROME);
                    table.ForeignKey(
                        name: "fk_id_code_ROME_domaine_formation_code_ROME",
                        column: x => x.id_code_ROME,
                        principalTable: "CodeROME",
                        principalColumn: "id_code_ROME",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_domaine_formation_domaine_formation_code_ROME",
                        column: x => x.id_domaine_formation,
                        principalTable: "DomaineFormation",
                        principalColumn: "id_domaine_formation",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Adresse",
                columns: table => new
                {
                    id_adresse = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    codepostal = table.Column<string>(unicode: false, maxLength: 5, nullable: false),
                    ville = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    departement = table.Column<string>(unicode: false, maxLength: 3, nullable: true),
                    code_INSEE_commune = table.Column<string>(unicode: false, maxLength: 5, nullable: true),
                    code_INSEE_canton = table.Column<string>(unicode: false, maxLength: 5, nullable: true),
                    region = table.Column<string>(unicode: false, maxLength: 2, nullable: true),
                    pays = table.Column<string>(unicode: false, maxLength: 2, nullable: true),
                    id_geolocalisation = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Adresse__05B3E6DC4344A34C", x => x.id_adresse);
                    table.ForeignKey(
                        name: "fk_id_geolocalisation_adresse",
                        column: x => x.id_geolocalisation,
                        principalTable: "Geolocalisation",
                        principalColumn: "id_geolocalisation",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContactTel",
                columns: table => new
                {
                    id_contact_tel = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_numtel = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__ContactT__E8D745748BC770D2", x => x.id_contact_tel);
                    table.ForeignKey(
                        name: "fk_id_numtel_contact_tel",
                        column: x => x.id_numtel,
                        principalTable: "Numtel",
                        principalColumn: "id_numtel",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Lheo",
                columns: table => new
                {
                    id_lheo = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_offre = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Lheo__99C21B20BF43CBA8", x => x.id_lheo);
                    table.ForeignKey(
                        name: "fk_id_offre_lheo",
                        column: x => x.id_offre,
                        principalTable: "Offre",
                        principalColumn: "id_offre",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PotentielCodeFORMACODE",
                columns: table => new
                {
                    id_potentiel_code_FORMACODE = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_potentiel = table.Column<int>(nullable: false),
                    id_code_FORMACODE = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Potentie__8976D8796AE60EF5", x => x.id_potentiel_code_FORMACODE);
                    table.ForeignKey(
                        name: "fk_id_code_FORMACODE_potentiel_code_FORMACODE",
                        column: x => x.id_code_FORMACODE,
                        principalTable: "CodeFORMACODE",
                        principalColumn: "id_code_FORMACODE",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_potentiel_potentiel_code_FORMACODE",
                        column: x => x.id_potentiel,
                        principalTable: "Potentiel",
                        principalColumn: "id_potentiel",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ModulePrerequisReferenceModule",
                columns: table => new
                {
                    id_module_prerequis_reference_module = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_modules_prerequis = table.Column<int>(nullable: false),
                    id_reference_module = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__ModulePr__A2C115D00AAD1F91", x => x.id_module_prerequis_reference_module);
                    table.ForeignKey(
                        name: "fk_id_modules_prerequis_module_prerequis_reference_module",
                        column: x => x.id_modules_prerequis,
                        principalTable: "ModulePrerequis",
                        principalColumn: "id_modules_prerequis",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_reference_module_module_prerequis_reference_module",
                        column: x => x.id_reference_module,
                        principalTable: "ReferenceModule",
                        principalColumn: "id_reference_module",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Module",
                columns: table => new
                {
                    id_module = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    reference_module = table.Column<string>(unicode: false, maxLength: 3000, nullable: false),
                    id_type_module = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Module__B2584DEA0339E564", x => x.id_module);
                    table.ForeignKey(
                        name: "fk_id_type_module_module",
                        column: x => x.id_type_module,
                        principalTable: "TypeModule",
                        principalColumn: "id_type_module",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UrlActionUrlWeb",
                columns: table => new
                {
                    id_url_action_url_web = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_url_action = table.Column<int>(nullable: false),
                    id_url_web = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__UrlActio__2EF5B4AB1267E34D", x => x.id_url_action_url_web);
                    table.ForeignKey(
                        name: "fk_id_url_action_url_action_url_web",
                        column: x => x.id_url_action,
                        principalTable: "UrlAction",
                        principalColumn: "id_url_action",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_url_web_url_action_url_web",
                        column: x => x.id_url_web,
                        principalTable: "UrlWeb",
                        principalColumn: "id_url_web",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UrlFormationUrlWeb",
                columns: table => new
                {
                    id_url_formation_url_web = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_url_formation = table.Column<int>(nullable: false),
                    id_url_web = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__UrlForma__C19DC4C4D7E2A508", x => x.id_url_formation_url_web);
                    table.ForeignKey(
                        name: "fk_id_url_formation_url_formation_url_web",
                        column: x => x.id_url_formation,
                        principalTable: "UrlFormation",
                        principalColumn: "id_url_formation",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_url_web_url_formation_url_web",
                        column: x => x.id_url_web,
                        principalTable: "UrlWeb",
                        principalColumn: "id_url_web",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WebUrlWeb",
                columns: table => new
                {
                    id_web_url_web = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_web = table.Column<int>(nullable: false),
                    id_url_web = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__WebUrlWe__E196487BC74A34E9", x => x.id_web_url_web);
                    table.ForeignKey(
                        name: "fk_id_url_web_web_url_web",
                        column: x => x.id_url_web,
                        principalTable: "UrlWeb",
                        principalColumn: "id_url_web",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_web_web_url_web",
                        column: x => x.id_web,
                        principalTable: "Web",
                        principalColumn: "id_web",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AdresseInformation",
                columns: table => new
                {
                    id_adresse_information = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_adresse = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__AdresseI__C61EEE43EA77F9F7", x => x.id_adresse_information);
                    table.ForeignKey(
                        name: "fk_adresse_information_id_adresse",
                        column: x => x.id_adresse,
                        principalTable: "Adresse",
                        principalColumn: "id_adresse",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AdresseInscription",
                columns: table => new
                {
                    id_adresse_inscription = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_adresse = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__AdresseI__4A1FF2CF6DEFAF0F", x => x.id_adresse_inscription);
                    table.ForeignKey(
                        name: "fk_id_adresse_adresse_inscription",
                        column: x => x.id_adresse,
                        principalTable: "Adresse",
                        principalColumn: "id_adresse",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AdresseLigne",
                columns: table => new
                {
                    id_adresse_ligne = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_ligne = table.Column<int>(nullable: false),
                    id_adresse = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__AdresseL__C9625507715965F7", x => x.id_adresse_ligne);
                    table.ForeignKey(
                        name: "fk_id_adresse_adresse_ligne",
                        column: x => x.id_adresse,
                        principalTable: "Adresse",
                        principalColumn: "id_adresse",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_ligne_adresse_ligne",
                        column: x => x.id_ligne,
                        principalTable: "Ligne",
                        principalColumn: "id_ligne",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Coordonnee",
                columns: table => new
                {
                    id_coordonnee = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    civilite = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    nom = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    prenom = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    id_adresse = table.Column<int>(nullable: true),
                    id_telfixe = table.Column<int>(nullable: false),
                    id_portable = table.Column<int>(nullable: false),
                    id_fax = table.Column<int>(nullable: false),
                    courriel = table.Column<string>(unicode: false, maxLength: 160, nullable: true),
                    id_web = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Coordonn__DD52265B6316F225", x => x.id_coordonnee);
                    table.ForeignKey(
                        name: "fk_id_adresse_coordonnee",
                        column: x => x.id_adresse,
                        principalTable: "Adresse",
                        principalColumn: "id_adresse",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_fax_coordonnee",
                        column: x => x.id_fax,
                        principalTable: "ContactTel",
                        principalColumn: "id_contact_tel",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_portable_coordonnee",
                        column: x => x.id_portable,
                        principalTable: "ContactTel",
                        principalColumn: "id_contact_tel",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_telfixe_coordonnee",
                        column: x => x.id_telfixe,
                        principalTable: "ContactTel",
                        principalColumn: "id_contact_tel",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_web_coordonnee",
                        column: x => x.id_web,
                        principalTable: "Web",
                        principalColumn: "id_web",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SousModuleModule",
                columns: table => new
                {
                    id_sous_module_module = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_sous_module = table.Column<int>(nullable: false),
                    id_module = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__SousModu__6580E4F55453929A", x => x.id_sous_module_module);
                    table.ForeignKey(
                        name: "fk_id_module_sous_module_module",
                        column: x => x.id_module,
                        principalTable: "Module",
                        principalColumn: "id_module",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_sous_module_sous_module_module",
                        column: x => x.id_sous_module,
                        principalTable: "SousModule",
                        principalColumn: "id_sous_module",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContactFormateur",
                columns: table => new
                {
                    id_contact_formateur = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_coordonnee = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__ContactF__13BA644DA6F04B2F", x => x.id_contact_formateur);
                    table.ForeignKey(
                        name: "fk_id_coordonnee_contact_formateur",
                        column: x => x.id_coordonnee,
                        principalTable: "Coordonnee",
                        principalColumn: "id_coordonnee",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContactFormation",
                columns: table => new
                {
                    id_contact_formation = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_coordonnee = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__ContactF__18C00F21D3DDC2B7", x => x.id_contact_formation);
                    table.ForeignKey(
                        name: "fk_id_coordonnee_contact_formation",
                        column: x => x.id_coordonnee,
                        principalTable: "Coordonnee",
                        principalColumn: "id_coordonnee",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContactOrganisme",
                columns: table => new
                {
                    id_contact_organisme = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_coordonnee = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__ContactO__7530B76D04F6FF92", x => x.id_contact_organisme);
                    table.ForeignKey(
                        name: "fk_id_coordonnee_contact_organisme",
                        column: x => x.id_coordonnee,
                        principalTable: "Coordonnee",
                        principalColumn: "id_coordonnee",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CoordonneeLigne",
                columns: table => new
                {
                    id_coordonnee_ligne = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_ligne = table.Column<int>(nullable: false),
                    id_coordonnee = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Coordonn__A9E5D85929C2A2B1", x => x.id_coordonnee_ligne);
                    table.ForeignKey(
                        name: "fk_id_coordonnee_coordonnee_ligne",
                        column: x => x.id_coordonnee,
                        principalTable: "Coordonnee",
                        principalColumn: "id_coordonnee",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_ligne_coordonnee_ligne",
                        column: x => x.id_ligne,
                        principalTable: "Ligne",
                        principalColumn: "id_ligne",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CoordonneeOrganisme",
                columns: table => new
                {
                    id_coordonnee_organisme = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_coordonnee = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Coordonn__2193A858D1444CD5", x => x.id_coordonnee_organisme);
                    table.ForeignKey(
                        name: "fk_id_coordonnee_coordonnee_organisme_coordonne",
                        column: x => x.id_coordonnee,
                        principalTable: "Coordonnee",
                        principalColumn: "id_coordonnee",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LieuDeFormation",
                columns: table => new
                {
                    id_lieu_de_formation = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_coordonnee = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__LieuDeFo__8B17F49BD6B7B0C3", x => x.id_lieu_de_formation);
                    table.ForeignKey(
                        name: "fk_id_coordonnee_lieu_de_formation",
                        column: x => x.id_coordonnee,
                        principalTable: "Coordonnee",
                        principalColumn: "id_coordonnee",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OrganismeFormateur",
                columns: table => new
                {
                    id_organisme_formateur = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_siret_formation = table.Column<int>(nullable: false),
                    raison_sociale_formateur = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    id_contact_formateur = table.Column<int>(nullable: true),
                    id_potentiel = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Organism__3A2DAB39BCD0FAB6", x => x.id_organisme_formateur);
                    table.ForeignKey(
                        name: "fk_id_contact_formateur_organisme_formateur",
                        column: x => x.id_contact_formateur,
                        principalTable: "ContactFormateur",
                        principalColumn: "id_contact_formateur",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_potentiel_organisme_formateur",
                        column: x => x.id_potentiel,
                        principalTable: "Potentiel",
                        principalColumn: "id_potentiel",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_organisme_formateur",
                        column: x => x.id_siret_formation,
                        principalTable: "SIRET",
                        principalColumn: "id_SIRET",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OrganismeFormationResponsable",
                columns: table => new
                {
                    id_organisme_formation_responsable = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    numero_activite = table.Column<string>(unicode: false, maxLength: 11, nullable: false),
                    id_SIRET = table.Column<int>(nullable: false),
                    nom_organisme = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    raison_sociale = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    id_coordonnee_organisme = table.Column<int>(nullable: false),
                    id_contact_organisme = table.Column<int>(nullable: false),
                    renseignements_specifiques = table.Column<string>(unicode: false, maxLength: 3000, nullable: true),
                    id_potentiel = table.Column<int>(nullable: true),
                    agreement_datadock = table.Column<string>(unicode: false, maxLength: 600, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Organism__0D8786F961B2F2AE", x => x.id_organisme_formation_responsable);
                    table.ForeignKey(
                        name: "fk_id_contact_organisme_organisme_formation_responsable",
                        column: x => x.id_contact_organisme,
                        principalTable: "ContactOrganisme",
                        principalColumn: "id_contact_organisme",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_coordonnee_organisme_organisme_formation_responsable",
                        column: x => x.id_coordonnee_organisme,
                        principalTable: "CoordonneeOrganisme",
                        principalColumn: "id_coordonnee_organisme",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_potentiel_organisme_formation_responsable",
                        column: x => x.id_potentiel,
                        principalTable: "Potentiel",
                        principalColumn: "id_potentiel",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_SIRET_organisme_formation_responsable",
                        column: x => x.id_SIRET,
                        principalTable: "SIRET",
                        principalColumn: "id_SIRET",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "_Action",
                columns: table => new
                {
                    id_action = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    rythme_formation = table.Column<string>(unicode: false, maxLength: 3000, nullable: false),
                    info_public_vise = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    niveau_entree_obligatoire = table.Column<bool>(nullable: false),
                    modalite_alternance = table.Column<string>(unicode: false, maxLength: 3000, nullable: true),
                    id_modalite_enseignement = table.Column<int>(nullable: false),
                    condition_specifiques = table.Column<string>(unicode: false, maxLength: 3000, nullable: false),
                    prise_en_charge_frais_possible = table.Column<bool>(nullable: false),
                    id_lieu_de_formation = table.Column<int>(nullable: false),
                    modalites_entrees_sorties = table.Column<bool>(nullable: false),
                    id_url_action = table.Column<int>(nullable: true),
                    id_adresse_information = table.Column<int>(nullable: true),
                    restauration = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    hebergement = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    transport = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    acces_handicape = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    lange_formation = table.Column<string>(unicode: false, maxLength: 2, nullable: true),
                    modalite_recrutement = table.Column<string>(unicode: false, maxLength: 3000, nullable: true),
                    modalite_pedagogiques = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    frais_restant = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    code_perimetre_recrutement = table.Column<int>(nullable: true),
                    info_perimetre_recrutement = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    prix_horaire_ttc = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    prix_total_ttc = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    duree_indicative = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    nombre_heures_centre = table.Column<int>(nullable: true),
                    nombre_heures_enterprise = table.Column<int>(nullable: true),
                    nombre_heures_total = table.Column<int>(nullable: true),
                    detail_condition_prise_en_charge = table.Column<string>(unicode: false, maxLength: 600, nullable: true),
                    conventionnement = table.Column<bool>(nullable: true),
                    duree_conventionee = table.Column<int>(nullable: true),
                    id_organisme_formateur = table.Column<int>(nullable: true),
                    financement_formation = table.Column<string>(unicode: false, maxLength: 600, nullable: false),
                    nb_places = table.Column<int>(nullable: true),
                    moyens_pedagogiques = table.Column<string>(unicode: false, maxLength: 600, nullable: false),
                    responsable_enseignement = table.Column<int>(nullable: true),
                    nombre_heures_cm = table.Column<int>(nullable: true),
                    nombre_heures_td = table.Column<int>(nullable: true),
                    nombre_heures_tp_tuteure = table.Column<int>(nullable: true),
                    nombre_heures_tp_non_tuteure = table.Column<int>(nullable: true),
                    nombre_heures_personnel = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK___Action__7887BBB843695383", x => x.id_action);
                    table.ForeignKey(
                        name: "fk_code_perimetre_recrutement_action",
                        column: x => x.code_perimetre_recrutement,
                        principalTable: "PerimetreRecrutement",
                        principalColumn: "cle",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_action_id_adresse_information",
                        column: x => x.id_adresse_information,
                        principalTable: "AdresseInformation",
                        principalColumn: "id_adresse_information",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_lieu_de_formation_action",
                        column: x => x.id_lieu_de_formation,
                        principalTable: "LieuDeFormation",
                        principalColumn: "id_lieu_de_formation",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_modalite_enseignement_action",
                        column: x => x.id_modalite_enseignement,
                        principalTable: "ModaliteEnseignement",
                        principalColumn: "id_modalite_enseignement",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_organisme_formateur_action",
                        column: x => x.id_organisme_formateur,
                        principalTable: "OrganismeFormateur",
                        principalColumn: "id_organisme_formateur",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_url_action_action",
                        column: x => x.id_url_action,
                        principalTable: "Web",
                        principalColumn: "id_web",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_responsable_enseignement_action",
                        column: x => x.responsable_enseignement,
                        principalTable: "Coordonnee",
                        principalColumn: "id_coordonnee",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Formation",
                columns: table => new
                {
                    id_formation = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_domaine_formation = table.Column<int>(nullable: false),
                    intitule_formation = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    objectif_formation = table.Column<string>(unicode: false, maxLength: 3000, nullable: false),
                    resultats_attendus = table.Column<string>(unicode: false, maxLength: 3000, nullable: false),
                    contenu_formation = table.Column<string>(unicode: false, maxLength: 3000, nullable: false),
                    certifiante = table.Column<bool>(nullable: false),
                    id_contact_formation = table.Column<int>(nullable: false),
                    id_parcours_de_formation = table.Column<int>(nullable: false),
                    id_niveaux_entree = table.Column<int>(nullable: false),
                    id_objectif_general_formation = table.Column<int>(nullable: true),
                    id_code_niveau_sortie = table.Column<int>(nullable: true),
                    id_url_formation = table.Column<int>(nullable: true),
                    id_organisme_formation_responsable = table.Column<int>(nullable: false),
                    identifiant_module = table.Column<string>(unicode: false, maxLength: 3000, nullable: true),
                    id_positionnement = table.Column<int>(nullable: true),
                    id_sous_module = table.Column<int>(nullable: true),
                    id_modules_prerequis = table.Column<int>(nullable: true),
                    credits_ects = table.Column<int>(nullable: true),
                    eligibilite_cpf = table.Column<string>(unicode: false, maxLength: 600, nullable: false),
                    validations = table.Column<string>(unicode: false, maxLength: 600, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Formatio__0B5751331F0EB9DA", x => x.id_formation);
                    table.ForeignKey(
                        name: "fk_id_code_niveau_sortie_formation",
                        column: x => x.id_code_niveau_sortie,
                        principalTable: "Niveaux",
                        principalColumn: "id_niveaux",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_contact_formation_formation",
                        column: x => x.id_contact_formation,
                        principalTable: "ContactFormation",
                        principalColumn: "id_contact_formation",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_domaine_formation_formation",
                        column: x => x.id_domaine_formation,
                        principalTable: "DomaineFormation",
                        principalColumn: "id_domaine_formation",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_modules_prerequis_formation",
                        column: x => x.id_modules_prerequis,
                        principalTable: "ModulePrerequis",
                        principalColumn: "id_modules_prerequis",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_niveaux_entree_formation",
                        column: x => x.id_niveaux_entree,
                        principalTable: "Niveaux",
                        principalColumn: "id_niveaux",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_objectif_general_formation",
                        column: x => x.id_objectif_general_formation,
                        principalTable: "ObjectifGeneralFormation",
                        principalColumn: "id_objectif_general_formation",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_organisme_formation_responsable_formation",
                        column: x => x.id_organisme_formation_responsable,
                        principalTable: "OrganismeFormationResponsable",
                        principalColumn: "id_organisme_formation_responsable",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_parcours_de_formation_formation",
                        column: x => x.id_parcours_de_formation,
                        principalTable: "ParcoursDeFormation",
                        principalColumn: "id_parcours_de_formation",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_positionnement_formation",
                        column: x => x.id_positionnement,
                        principalTable: "Positionnement",
                        principalColumn: "id_positionnement",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_sous_module_formation",
                        column: x => x.id_sous_module,
                        principalTable: "SousModule",
                        principalColumn: "id_sous_module",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_url_formation_formation",
                        column: x => x.id_url_formation,
                        principalTable: "UrlFormation",
                        principalColumn: "id_url_formation",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "_Session",
                columns: table => new
                {
                    id_session = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_action = table.Column<int>(nullable: false),
                    id_periode = table.Column<int>(nullable: false),
                    id_adresse_inscription = table.Column<int>(nullable: false),
                    modalite_inscription = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    id_periode_inscription = table.Column<int>(nullable: true),
                    id_etat_recrutement = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK___Session__A9E494D0CA63B626", x => x.id_session);
                    table.ForeignKey(
                        name: "fk_id_action_session",
                        column: x => x.id_action,
                        principalTable: "_Action",
                        principalColumn: "id_action",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_adresse_inscription_session",
                        column: x => x.id_adresse_inscription,
                        principalTable: "Adresse",
                        principalColumn: "id_adresse",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_etat_recrutement_session",
                        column: x => x.id_etat_recrutement,
                        principalTable: "EtatRecrutement",
                        principalColumn: "id_etat_recrutement",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_periode_session",
                        column: x => x.id_periode,
                        principalTable: "Periode",
                        principalColumn: "id_periode",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_periode_inscription_session",
                        column: x => x.id_periode_inscription,
                        principalTable: "Periode",
                        principalColumn: "id_periode",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ActionOrganismeFinanceur",
                columns: table => new
                {
                    id_action_organisme_financeur = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_action = table.Column<int>(nullable: false),
                    id_organisme_financeur = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__ActionOr__DFCF008991F752BB", x => x.id_action_organisme_financeur);
                    table.ForeignKey(
                        name: "fk_action_id_action",
                        column: x => x.id_action,
                        principalTable: "_Action",
                        principalColumn: "id_action",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_action_id_organisme_financeur",
                        column: x => x.id_organisme_financeur,
                        principalTable: "OrganismeFinanceur",
                        principalColumn: "id_organisme_financeur",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CodePublicVise",
                columns: table => new
                {
                    id_code_public_vise = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_action = table.Column<int>(nullable: false),
                    code = table.Column<string>(unicode: false, maxLength: 5, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__CodePubl__2C6CB22F65A60F31", x => x.id_code_public_vise);
                    table.ForeignKey(
                        name: "fk_code_public_vise_id_action",
                        column: x => x.id_action,
                        principalTable: "_Action",
                        principalColumn: "id_action",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DateInformation",
                columns: table => new
                {
                    id_date_information = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    date_information = table.Column<DateTime>(type: "date", nullable: false),
                    id_action = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__DateInfo__86DD22D550A15AA7", x => x.id_date_information);
                    table.ForeignKey(
                        name: "fk_date_information_id_action",
                        column: x => x.id_action,
                        principalTable: "_Action",
                        principalColumn: "id_action",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ModalitePedagogique",
                columns: table => new
                {
                    id_modalite_pedagogique = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_action = table.Column<int>(nullable: false),
                    label = table.Column<string>(unicode: false, maxLength: 5, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Modalite__D4F06B3A5F28BE04", x => x.id_modalite_pedagogique);
                    table.ForeignKey(
                        name: "fk_id_action_modalite_information",
                        column: x => x.id_action,
                        principalTable: "_Action",
                        principalColumn: "id_action",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FormationAction",
                columns: table => new
                {
                    id_formation_action = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_formation = table.Column<int>(nullable: false),
                    id_action = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Formatio__AFE2284C6150F7FC", x => x.id_formation_action);
                    table.ForeignKey(
                        name: "fk_id_action_formation_action",
                        column: x => x.id_action,
                        principalTable: "_Action",
                        principalColumn: "id_action",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_formation_formation_action",
                        column: x => x.id_formation,
                        principalTable: "Formation",
                        principalColumn: "id_formation",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FormationCertification",
                columns: table => new
                {
                    id_formation_certification = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_formation = table.Column<int>(nullable: false),
                    id_certification = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Formatio__452A018E0373D2DE", x => x.id_formation_certification);
                    table.ForeignKey(
                        name: "fk_id_certification_formation_certification",
                        column: x => x.id_certification,
                        principalTable: "Certification",
                        principalColumn: "id_certification",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_formation_formation_certification",
                        column: x => x.id_formation,
                        principalTable: "Formation",
                        principalColumn: "id_formation",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OffreFormation",
                columns: table => new
                {
                    id_offre_formation = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_offre = table.Column<int>(nullable: false),
                    id_formation = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__OffreFor__9B7739A15152A970", x => x.id_offre_formation);
                    table.ForeignKey(
                        name: "fk_id_formation_offre_formation",
                        column: x => x.id_formation,
                        principalTable: "Formation",
                        principalColumn: "id_formation",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_offre_offre_formation",
                        column: x => x.id_offre,
                        principalTable: "Offre",
                        principalColumn: "id_offre",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Extra",
                columns: table => new
                {
                    id_extra = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_lheo = table.Column<int>(nullable: true),
                    id_offre = table.Column<int>(nullable: true),
                    id_formation = table.Column<int>(nullable: true),
                    id_domaine_formation = table.Column<int>(nullable: true),
                    id_contact_formation = table.Column<int>(nullable: true),
                    id_coordonnee = table.Column<int>(nullable: true),
                    id_adresse = table.Column<int>(nullable: true),
                    id_geolocalisation = table.Column<int>(nullable: true),
                    id_web = table.Column<int>(nullable: true),
                    id_certification = table.Column<int>(nullable: true),
                    id_sous_module = table.Column<int>(nullable: true),
                    id_module = table.Column<int>(nullable: true),
                    id_modules_prerequis = table.Column<int>(nullable: true),
                    id_organisme_formation_responsable = table.Column<int>(nullable: true),
                    id_SIRET = table.Column<int>(nullable: true),
                    id_coordonnee_organisme = table.Column<int>(nullable: true),
                    id_contact_organisme = table.Column<int>(nullable: true),
                    id_potentiel = table.Column<int>(nullable: true),
                    id_action = table.Column<int>(nullable: true),
                    id_url_action = table.Column<int>(nullable: true),
                    id_periode = table.Column<int>(nullable: true),
                    id_adresse_inscription = table.Column<int>(nullable: true),
                    id_date_information = table.Column<int>(nullable: true),
                    id_organisme_formateur = table.Column<int>(nullable: true),
                    id_contact_formateur = table.Column<int>(nullable: true),
                    id_organisme_financeur = table.Column<int>(nullable: true),
                    commentaire = table.Column<string>(unicode: false, maxLength: 300, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Extra__8875B27144CD35A1", x => x.id_extra);
                    table.ForeignKey(
                        name: "fk_id_action_extra",
                        column: x => x.id_action,
                        principalTable: "_Action",
                        principalColumn: "id_action",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_adresse_extra",
                        column: x => x.id_adresse,
                        principalTable: "Adresse",
                        principalColumn: "id_adresse",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_adresse_inscription_extra",
                        column: x => x.id_adresse_inscription,
                        principalTable: "AdresseInscription",
                        principalColumn: "id_adresse_inscription",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_certification_extra",
                        column: x => x.id_certification,
                        principalTable: "Certification",
                        principalColumn: "id_certification",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_contact_formateur_extra",
                        column: x => x.id_contact_formateur,
                        principalTable: "ContactFormateur",
                        principalColumn: "id_contact_formateur",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_contact_formation_extra",
                        column: x => x.id_contact_formation,
                        principalTable: "ContactFormation",
                        principalColumn: "id_contact_formation",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_contact_organisme_extra",
                        column: x => x.id_contact_organisme,
                        principalTable: "ContactOrganisme",
                        principalColumn: "id_contact_organisme",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_coordonnee_extra",
                        column: x => x.id_coordonnee,
                        principalTable: "Coordonnee",
                        principalColumn: "id_coordonnee",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_coordonnee_organisme_extra",
                        column: x => x.id_coordonnee_organisme,
                        principalTable: "CoordonneeOrganisme",
                        principalColumn: "id_coordonnee_organisme",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_date_information_extra",
                        column: x => x.id_date_information,
                        principalTable: "DateInformation",
                        principalColumn: "id_date_information",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_domaine_formation_extra",
                        column: x => x.id_domaine_formation,
                        principalTable: "DomaineFormation",
                        principalColumn: "id_domaine_formation",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_formation_extra",
                        column: x => x.id_formation,
                        principalTable: "Formation",
                        principalColumn: "id_formation",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_geolocalisation_extra",
                        column: x => x.id_geolocalisation,
                        principalTable: "Geolocalisation",
                        principalColumn: "id_geolocalisation",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_lheo_extra",
                        column: x => x.id_lheo,
                        principalTable: "Lheo",
                        principalColumn: "id_lheo",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_module_extra",
                        column: x => x.id_module,
                        principalTable: "Module",
                        principalColumn: "id_module",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_modules_prerequis_extra",
                        column: x => x.id_modules_prerequis,
                        principalTable: "ModulePrerequis",
                        principalColumn: "id_modules_prerequis",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_offre_extra",
                        column: x => x.id_offre,
                        principalTable: "Offre",
                        principalColumn: "id_offre",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_organisme_financeur_extra",
                        column: x => x.id_organisme_financeur,
                        principalTable: "OrganismeFinanceur",
                        principalColumn: "id_organisme_financeur",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_organisme_formateur_extra",
                        column: x => x.id_organisme_formateur,
                        principalTable: "OrganismeFormateur",
                        principalColumn: "id_organisme_formateur",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_organisme_formation_responsable_extra",
                        column: x => x.id_organisme_formation_responsable,
                        principalTable: "OrganismeFormationResponsable",
                        principalColumn: "id_organisme_formation_responsable",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_periode_extra",
                        column: x => x.id_periode,
                        principalTable: "Periode",
                        principalColumn: "id_periode",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_potentiel_extra",
                        column: x => x.id_potentiel,
                        principalTable: "Potentiel",
                        principalColumn: "id_potentiel",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_SIRET_extra",
                        column: x => x.id_SIRET,
                        principalTable: "SIRET",
                        principalColumn: "id_SIRET",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_sous_module_extra",
                        column: x => x.id_sous_module,
                        principalTable: "SousModule",
                        principalColumn: "id_sous_module",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_url_action_extra",
                        column: x => x.id_url_action,
                        principalTable: "UrlAction",
                        principalColumn: "id_url_action",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_web_extra",
                        column: x => x.id_web,
                        principalTable: "Web",
                        principalColumn: "id_web",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX__Action_code_perimetre_recrutement",
                table: "_Action",
                column: "code_perimetre_recrutement");

            migrationBuilder.CreateIndex(
                name: "IX__Action_id_adresse_information",
                table: "_Action",
                column: "id_adresse_information");

            migrationBuilder.CreateIndex(
                name: "IX__Action_id_lieu_de_formation",
                table: "_Action",
                column: "id_lieu_de_formation");

            migrationBuilder.CreateIndex(
                name: "IX__Action_id_modalite_enseignement",
                table: "_Action",
                column: "id_modalite_enseignement");

            migrationBuilder.CreateIndex(
                name: "IX__Action_id_organisme_formateur",
                table: "_Action",
                column: "id_organisme_formateur");

            migrationBuilder.CreateIndex(
                name: "IX__Action_id_url_action",
                table: "_Action",
                column: "id_url_action");

            migrationBuilder.CreateIndex(
                name: "IX__Action_responsable_enseignement",
                table: "_Action",
                column: "responsable_enseignement");

            migrationBuilder.CreateIndex(
                name: "IX__Session_id_action",
                table: "_Session",
                column: "id_action");

            migrationBuilder.CreateIndex(
                name: "IX__Session_id_adresse_inscription",
                table: "_Session",
                column: "id_adresse_inscription");

            migrationBuilder.CreateIndex(
                name: "IX__Session_id_etat_recrutement",
                table: "_Session",
                column: "id_etat_recrutement");

            migrationBuilder.CreateIndex(
                name: "IX__Session_id_periode",
                table: "_Session",
                column: "id_periode");

            migrationBuilder.CreateIndex(
                name: "IX__Session_id_periode_inscription",
                table: "_Session",
                column: "id_periode_inscription");

            migrationBuilder.CreateIndex(
                name: "IX_ActionOrganismeFinanceur_id_action",
                table: "ActionOrganismeFinanceur",
                column: "id_action");

            migrationBuilder.CreateIndex(
                name: "IX_ActionOrganismeFinanceur_id_organisme_financeur",
                table: "ActionOrganismeFinanceur",
                column: "id_organisme_financeur");

            migrationBuilder.CreateIndex(
                name: "IX_Adresse_id_geolocalisation",
                table: "Adresse",
                column: "id_geolocalisation");

            migrationBuilder.CreateIndex(
                name: "IX_AdresseInformation_id_adresse",
                table: "AdresseInformation",
                column: "id_adresse");

            migrationBuilder.CreateIndex(
                name: "IX_AdresseInscription_id_adresse",
                table: "AdresseInscription",
                column: "id_adresse");

            migrationBuilder.CreateIndex(
                name: "IX_AdresseLigne_id_adresse",
                table: "AdresseLigne",
                column: "id_adresse");

            migrationBuilder.CreateIndex(
                name: "IX_AdresseLigne_id_ligne",
                table: "AdresseLigne",
                column: "id_ligne");

            migrationBuilder.CreateIndex(
                name: "IX_CodePublicVise_id_action",
                table: "CodePublicVise",
                column: "id_action");

            migrationBuilder.CreateIndex(
                name: "IX_ContactFormateur_id_coordonnee",
                table: "ContactFormateur",
                column: "id_coordonnee");

            migrationBuilder.CreateIndex(
                name: "IX_ContactFormation_id_coordonnee",
                table: "ContactFormation",
                column: "id_coordonnee");

            migrationBuilder.CreateIndex(
                name: "IX_ContactOrganisme_id_coordonnee",
                table: "ContactOrganisme",
                column: "id_coordonnee");

            migrationBuilder.CreateIndex(
                name: "IX_ContactTel_id_numtel",
                table: "ContactTel",
                column: "id_numtel");

            migrationBuilder.CreateIndex(
                name: "IX_Coordonnee_id_adresse",
                table: "Coordonnee",
                column: "id_adresse");

            migrationBuilder.CreateIndex(
                name: "IX_Coordonnee_id_fax",
                table: "Coordonnee",
                column: "id_fax");

            migrationBuilder.CreateIndex(
                name: "IX_Coordonnee_id_portable",
                table: "Coordonnee",
                column: "id_portable");

            migrationBuilder.CreateIndex(
                name: "IX_Coordonnee_id_telfixe",
                table: "Coordonnee",
                column: "id_telfixe");

            migrationBuilder.CreateIndex(
                name: "IX_Coordonnee_id_web",
                table: "Coordonnee",
                column: "id_web");

            migrationBuilder.CreateIndex(
                name: "IX_CoordonneeLigne_id_coordonnee",
                table: "CoordonneeLigne",
                column: "id_coordonnee");

            migrationBuilder.CreateIndex(
                name: "IX_CoordonneeLigne_id_ligne",
                table: "CoordonneeLigne",
                column: "id_ligne");

            migrationBuilder.CreateIndex(
                name: "IX_CoordonneeOrganisme_id_coordonnee",
                table: "CoordonneeOrganisme",
                column: "id_coordonnee");

            migrationBuilder.CreateIndex(
                name: "IX_DateInformation_id_action",
                table: "DateInformation",
                column: "id_action");

            migrationBuilder.CreateIndex(
                name: "IX_DomaineFormationCodeFORMACODE_id_code_FORMACODE",
                table: "DomaineFormationCodeFORMACODE",
                column: "id_code_FORMACODE");

            migrationBuilder.CreateIndex(
                name: "IX_DomaineFormationCodeFORMACODE_id_domaine_formation",
                table: "DomaineFormationCodeFORMACODE",
                column: "id_domaine_formation");

            migrationBuilder.CreateIndex(
                name: "IX_DomaineFormationCodeNSF_id_code_NSF",
                table: "DomaineFormationCodeNSF",
                column: "id_code_NSF");

            migrationBuilder.CreateIndex(
                name: "IX_DomaineFormationCodeNSF_id_domaine_formation",
                table: "DomaineFormationCodeNSF",
                column: "id_domaine_formation");

            migrationBuilder.CreateIndex(
                name: "IX_DomaineFormationCodeROME_id_code_ROME",
                table: "DomaineFormationCodeROME",
                column: "id_code_ROME");

            migrationBuilder.CreateIndex(
                name: "IX_DomaineFormationCodeROME_id_domaine_formation",
                table: "DomaineFormationCodeROME",
                column: "id_domaine_formation");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_action",
                table: "Extra",
                column: "id_action");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_adresse",
                table: "Extra",
                column: "id_adresse");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_adresse_inscription",
                table: "Extra",
                column: "id_adresse_inscription");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_certification",
                table: "Extra",
                column: "id_certification");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_contact_formateur",
                table: "Extra",
                column: "id_contact_formateur");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_contact_formation",
                table: "Extra",
                column: "id_contact_formation");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_contact_organisme",
                table: "Extra",
                column: "id_contact_organisme");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_coordonnee",
                table: "Extra",
                column: "id_coordonnee");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_coordonnee_organisme",
                table: "Extra",
                column: "id_coordonnee_organisme");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_date_information",
                table: "Extra",
                column: "id_date_information");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_domaine_formation",
                table: "Extra",
                column: "id_domaine_formation");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_formation",
                table: "Extra",
                column: "id_formation");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_geolocalisation",
                table: "Extra",
                column: "id_geolocalisation");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_lheo",
                table: "Extra",
                column: "id_lheo");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_module",
                table: "Extra",
                column: "id_module");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_modules_prerequis",
                table: "Extra",
                column: "id_modules_prerequis");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_offre",
                table: "Extra",
                column: "id_offre");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_organisme_financeur",
                table: "Extra",
                column: "id_organisme_financeur");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_organisme_formateur",
                table: "Extra",
                column: "id_organisme_formateur");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_organisme_formation_responsable",
                table: "Extra",
                column: "id_organisme_formation_responsable");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_periode",
                table: "Extra",
                column: "id_periode");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_potentiel",
                table: "Extra",
                column: "id_potentiel");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_SIRET",
                table: "Extra",
                column: "id_SIRET");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_sous_module",
                table: "Extra",
                column: "id_sous_module");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_url_action",
                table: "Extra",
                column: "id_url_action");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_web",
                table: "Extra",
                column: "id_web");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_code_niveau_sortie",
                table: "Formation",
                column: "id_code_niveau_sortie");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_contact_formation",
                table: "Formation",
                column: "id_contact_formation");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_domaine_formation",
                table: "Formation",
                column: "id_domaine_formation");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_modules_prerequis",
                table: "Formation",
                column: "id_modules_prerequis");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_niveaux_entree",
                table: "Formation",
                column: "id_niveaux_entree");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_objectif_general_formation",
                table: "Formation",
                column: "id_objectif_general_formation");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_organisme_formation_responsable",
                table: "Formation",
                column: "id_organisme_formation_responsable");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_parcours_de_formation",
                table: "Formation",
                column: "id_parcours_de_formation");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_positionnement",
                table: "Formation",
                column: "id_positionnement");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_sous_module",
                table: "Formation",
                column: "id_sous_module");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_url_formation",
                table: "Formation",
                column: "id_url_formation");

            migrationBuilder.CreateIndex(
                name: "IX_FormationAction_id_action",
                table: "FormationAction",
                column: "id_action");

            migrationBuilder.CreateIndex(
                name: "IX_FormationAction_id_formation",
                table: "FormationAction",
                column: "id_formation");

            migrationBuilder.CreateIndex(
                name: "IX_FormationCertification_id_certification",
                table: "FormationCertification",
                column: "id_certification");

            migrationBuilder.CreateIndex(
                name: "IX_FormationCertification_id_formation",
                table: "FormationCertification",
                column: "id_formation");

            migrationBuilder.CreateIndex(
                name: "IX_Lheo_id_offre",
                table: "Lheo",
                column: "id_offre");

            migrationBuilder.CreateIndex(
                name: "IX_LieuDeFormation_id_coordonnee",
                table: "LieuDeFormation",
                column: "id_coordonnee");

            migrationBuilder.CreateIndex(
                name: "IX_ModalitePedagogique_id_action",
                table: "ModalitePedagogique",
                column: "id_action");

            migrationBuilder.CreateIndex(
                name: "IX_Module_id_type_module",
                table: "Module",
                column: "id_type_module");

            migrationBuilder.CreateIndex(
                name: "IX_ModulePrerequisReferenceModule_id_modules_prerequis",
                table: "ModulePrerequisReferenceModule",
                column: "id_modules_prerequis");

            migrationBuilder.CreateIndex(
                name: "IX_ModulePrerequisReferenceModule_id_reference_module",
                table: "ModulePrerequisReferenceModule",
                column: "id_reference_module");

            migrationBuilder.CreateIndex(
                name: "IX_OffreFormation_id_formation",
                table: "OffreFormation",
                column: "id_formation");

            migrationBuilder.CreateIndex(
                name: "IX_OffreFormation_id_offre",
                table: "OffreFormation",
                column: "id_offre");

            migrationBuilder.CreateIndex(
                name: "IX_OrganismeFormateur_id_contact_formateur",
                table: "OrganismeFormateur",
                column: "id_contact_formateur");

            migrationBuilder.CreateIndex(
                name: "IX_OrganismeFormateur_id_potentiel",
                table: "OrganismeFormateur",
                column: "id_potentiel");

            migrationBuilder.CreateIndex(
                name: "IX_OrganismeFormateur_id_siret_formation",
                table: "OrganismeFormateur",
                column: "id_siret_formation");

            migrationBuilder.CreateIndex(
                name: "IX_OrganismeFormationResponsable_id_contact_organisme",
                table: "OrganismeFormationResponsable",
                column: "id_contact_organisme");

            migrationBuilder.CreateIndex(
                name: "IX_OrganismeFormationResponsable_id_coordonnee_organisme",
                table: "OrganismeFormationResponsable",
                column: "id_coordonnee_organisme");

            migrationBuilder.CreateIndex(
                name: "IX_OrganismeFormationResponsable_id_potentiel",
                table: "OrganismeFormationResponsable",
                column: "id_potentiel");

            migrationBuilder.CreateIndex(
                name: "IX_OrganismeFormationResponsable_id_SIRET",
                table: "OrganismeFormationResponsable",
                column: "id_SIRET");

            migrationBuilder.CreateIndex(
                name: "UK_perimetre_recrutement_cle",
                table: "PerimetreRecrutement",
                column: "cle",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PotentielCodeFORMACODE_id_code_FORMACODE",
                table: "PotentielCodeFORMACODE",
                column: "id_code_FORMACODE");

            migrationBuilder.CreateIndex(
                name: "IX_PotentielCodeFORMACODE_id_potentiel",
                table: "PotentielCodeFORMACODE",
                column: "id_potentiel");

            migrationBuilder.CreateIndex(
                name: "IX_SousModuleModule_id_module",
                table: "SousModuleModule",
                column: "id_module");

            migrationBuilder.CreateIndex(
                name: "IX_SousModuleModule_id_sous_module",
                table: "SousModuleModule",
                column: "id_sous_module");

            migrationBuilder.CreateIndex(
                name: "IX_UrlActionUrlWeb_id_url_action",
                table: "UrlActionUrlWeb",
                column: "id_url_action");

            migrationBuilder.CreateIndex(
                name: "IX_UrlActionUrlWeb_id_url_web",
                table: "UrlActionUrlWeb",
                column: "id_url_web");

            migrationBuilder.CreateIndex(
                name: "IX_UrlFormationUrlWeb_id_url_formation",
                table: "UrlFormationUrlWeb",
                column: "id_url_formation");

            migrationBuilder.CreateIndex(
                name: "IX_UrlFormationUrlWeb_id_url_web",
                table: "UrlFormationUrlWeb",
                column: "id_url_web");

            migrationBuilder.CreateIndex(
                name: "IX_WebUrlWeb_id_url_web",
                table: "WebUrlWeb",
                column: "id_url_web");

            migrationBuilder.CreateIndex(
                name: "IX_WebUrlWeb_id_web",
                table: "WebUrlWeb",
                column: "id_web");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "_Session");

            migrationBuilder.DropTable(
                name: "ActionOrganismeFinanceur");

            migrationBuilder.DropTable(
                name: "AdresseLigne");

            migrationBuilder.DropTable(
                name: "CodePublicVise");

            migrationBuilder.DropTable(
                name: "CoordonneeLigne");

            migrationBuilder.DropTable(
                name: "DomaineFormationCodeFORMACODE");

            migrationBuilder.DropTable(
                name: "DomaineFormationCodeNSF");

            migrationBuilder.DropTable(
                name: "DomaineFormationCodeROME");

            migrationBuilder.DropTable(
                name: "Extra");

            migrationBuilder.DropTable(
                name: "FormationAction");

            migrationBuilder.DropTable(
                name: "FormationCertification");

            migrationBuilder.DropTable(
                name: "ModalitePedagogique");

            migrationBuilder.DropTable(
                name: "ModulePrerequisReferenceModule");

            migrationBuilder.DropTable(
                name: "OffreFormation");

            migrationBuilder.DropTable(
                name: "PotentielCodeFORMACODE");

            migrationBuilder.DropTable(
                name: "SousModuleModule");

            migrationBuilder.DropTable(
                name: "UrlActionUrlWeb");

            migrationBuilder.DropTable(
                name: "UrlFormationUrlWeb");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "WebUrlWeb");

            migrationBuilder.DropTable(
                name: "EtatRecrutement");

            migrationBuilder.DropTable(
                name: "Ligne");

            migrationBuilder.DropTable(
                name: "CodeNSF");

            migrationBuilder.DropTable(
                name: "CodeROME");

            migrationBuilder.DropTable(
                name: "AdresseInscription");

            migrationBuilder.DropTable(
                name: "DateInformation");

            migrationBuilder.DropTable(
                name: "Lheo");

            migrationBuilder.DropTable(
                name: "OrganismeFinanceur");

            migrationBuilder.DropTable(
                name: "Periode");

            migrationBuilder.DropTable(
                name: "Certification");

            migrationBuilder.DropTable(
                name: "ReferenceModule");

            migrationBuilder.DropTable(
                name: "Formation");

            migrationBuilder.DropTable(
                name: "CodeFORMACODE");

            migrationBuilder.DropTable(
                name: "Module");

            migrationBuilder.DropTable(
                name: "UrlAction");

            migrationBuilder.DropTable(
                name: "UrlWeb");

            migrationBuilder.DropTable(
                name: "_Action");

            migrationBuilder.DropTable(
                name: "Offre");

            migrationBuilder.DropTable(
                name: "Niveaux");

            migrationBuilder.DropTable(
                name: "ContactFormation");

            migrationBuilder.DropTable(
                name: "DomaineFormation");

            migrationBuilder.DropTable(
                name: "ModulePrerequis");

            migrationBuilder.DropTable(
                name: "ObjectifGeneralFormation");

            migrationBuilder.DropTable(
                name: "OrganismeFormationResponsable");

            migrationBuilder.DropTable(
                name: "ParcoursDeFormation");

            migrationBuilder.DropTable(
                name: "Positionnement");

            migrationBuilder.DropTable(
                name: "SousModule");

            migrationBuilder.DropTable(
                name: "UrlFormation");

            migrationBuilder.DropTable(
                name: "TypeModule");

            migrationBuilder.DropTable(
                name: "PerimetreRecrutement");

            migrationBuilder.DropTable(
                name: "AdresseInformation");

            migrationBuilder.DropTable(
                name: "LieuDeFormation");

            migrationBuilder.DropTable(
                name: "ModaliteEnseignement");

            migrationBuilder.DropTable(
                name: "OrganismeFormateur");

            migrationBuilder.DropTable(
                name: "ContactOrganisme");

            migrationBuilder.DropTable(
                name: "CoordonneeOrganisme");

            migrationBuilder.DropTable(
                name: "ContactFormateur");

            migrationBuilder.DropTable(
                name: "Potentiel");

            migrationBuilder.DropTable(
                name: "SIRET");

            migrationBuilder.DropTable(
                name: "Coordonnee");

            migrationBuilder.DropTable(
                name: "Adresse");

            migrationBuilder.DropTable(
                name: "ContactTel");

            migrationBuilder.DropTable(
                name: "Web");

            migrationBuilder.DropTable(
                name: "Geolocalisation");

            migrationBuilder.DropTable(
                name: "Numtel");
        }
    }
}
