using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Wise.DbLib
{
    public class AppConfig
    {
        public readonly string _connectionString = string.Empty;
        public AppConfig()
        {
            var configurationBuilder = new ConfigurationBuilder();
#if DEBUG
            var path = Path.Combine(Directory.GetCurrentDirectory(), "../Wise.API/", "appsettings.Development.json");
#else
            var path = Path.Combine(Directory.GetCurrentDirectory(),"../Wise.API/" ,"appsettings.json");
#endif
            configurationBuilder.AddJsonFile(path, false);

            var root = configurationBuilder.Build();
            _connectionString = root.GetSection("ConnectionStrings").GetSection("WiseNetwork").Value;
        }
        public string ConnectionString
        {
            get => _connectionString;
        }

    }
}
