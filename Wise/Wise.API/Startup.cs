using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Wise.API.Config;
using Wise.API.Repositories;
using Wise.API.Services;
using Wise.DbLib.Models;

namespace Wise.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Récupération des informations JWT
            services.Configure<JwtKeyConfig>(Configuration.GetSection("JwtKeyConfig"));
            services.Configure<SMTPConfig>(Configuration.GetSection("SMTPConfig"));

            // Ajout configuration du token JWT
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = "JwtBearer";
                options.DefaultChallengeScheme = "JwtBearer";
            })
            .AddJwtBearer("JwtBearer", jwtBearerOptions =>
            {
                jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration.GetSection("JwtKeyConfig").GetChildren().First().Value)),

                    ValidateIssuer = true,
                    ValidIssuer = "WiseNetwork",

                    ValidateAudience = true,
                    ValidAudience = "The name of the audience",

                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero

                };
            });

            services.AddMvc()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //Ajout du contexte d'accès à la base de données
            services.AddDbContext<WiseNetworkContext>(options => options.UseMySql(Configuration.GetConnectionString("WiseNetwork")));

            //Ajout des services
            services.AddTransient<IMailService, MailService>();

            //Ajout des repository pour l'injection de dépendances
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<INiveauRepository, NiveauRepository>();
            services.AddTransient<IActionRepository, ActionRepository>();
            services.AddTransient<IActionOrganismeFinanceurRepository, ActionOrganismeFinanceurRepository>();
            services.AddTransient<IAdresseRepository, AdresseRepository>();
            services.AddTransient<IAdresseInformationRepository, AdresseInformationRepository>();
            services.AddTransient<IAdresseInscriptionRepository, AdresseInscriptionRepository>();
            services.AddTransient<IAdresseLigneRepository, AdresseLigneRepository>();
            services.AddTransient<ICertificationRepository, CertificationRepository>();
            services.AddTransient<ICodeFormacodeRepository, CodeFormacodeRepository>();
            services.AddTransient<ICodeNsfRepository, CodeNsfRepository>();
            services.AddTransient<ICodePublicViseRepository, CodePublicViseRepository>();
            services.AddTransient<IContactFormateurRepository, ContactFormateurRepository>();
            services.AddTransient<IContactFormationRepository, ContactFormationRepository>();
            services.AddTransient<IContactOrganismeRepository, ContactOrganismeRepository>();
            services.AddTransient<IContactTelRepository, ContactTelRepository>();
            services.AddTransient<ICoordonneeRepository, CoordonneeRepository>();
            services.AddTransient<ICoordonneeLigneRepository, CoordonneeLigneRepository>();
            services.AddTransient<ICoordonneeOrganismeRepository, CoordonneeOrganismeRepository>();
            services.AddTransient<IDateInformationRepository, DateInformationRepository>();
            services.AddTransient<IDomaineFormationRepository, DomaineFormationRepository>();
            services.AddTransient<IDomaineFormationCodeFormacodeRepository, DomaineFormationCodeFormacodeRepository>();
            services.AddTransient<IDomaineFormationCodeNsfRepository, DomaineFormationCodeNsfRepository>();
            services.AddTransient<IDomaineFormationCodeRomeRepository, DomaineFormationCodeRomeRepository>();
            services.AddTransient<IEtatRecrutementRepository, EtatRecrutementRepository>();
            services.AddTransient<IExtraRepository, ExtraRepository>();
            services.AddTransient<IFormationRepository, FormationRepository>();
            services.AddTransient<IFormationActionRepository, FormationActionRepository>();
            services.AddTransient<IFormationCertificationRepository, FormationCertificationRepository>();
            services.AddTransient<IGeolocalisationRepository, GeolocalisationRepository>();
            services.AddTransient<ILheoRepository, LheoRepository>();
            services.AddTransient<ILieuDeFormationRepository, LieuDeFormationRepository>();
            services.AddTransient<ILigneRepository, LigneRepository>();
            services.AddTransient<IModaliteEnseignementRepository, ModaliteEnseignementRepository>();
            services.AddTransient<IModalitePedagogiqueRepository, ModalitePedagogiqueRepository>();
            services.AddTransient<IModuleRepository, ModuleRepository>();           
            services.AddTransient<IModulePrerequisRepository, ModulePrerequisRepository>();
            services.AddTransient<IModulePrerequisReferenceModuleRepository, ModulePrerequisReferenceModuleRepository>();
            services.AddTransient<INumtelRepository, NumtelRepository>();
            services.AddTransient<IObjectifGeneralFormationRepository, ObjectifGeneralFormationRepository>();
            services.AddTransient<IOffreRepository, OffreRepository>();
            services.AddTransient<IOffreFormationRepository, OffreFormationRepository>();
            services.AddTransient<IOrganismeFinanceurRepository, OrganismeFinanceurRepository>();
            services.AddTransient<IOrganismeFormateurRepository, OrganismeFormateurRepository>();
            services.AddTransient<IOrganismeFormationResponsableRepository, OrganismeFormationResponsableRepository>();
            services.AddTransient<IParcoursDeFormationRepository, ParcoursDeFormationRepository>();
            services.AddTransient<IPerimetreRecrutementRepository, PerimetreRecrutementRepository>();
            services.AddTransient<IPeriodeRepository, PeriodeRepository>();
            services.AddTransient<IPotentielCodeFormacodeRepository, PotentielCodeFormacodeRepository>();
            services.AddTransient<IReferenceModuleRepository, ReferenceModuleRepository>();
            services.AddTransient<ISessionRepository, SessionRepository>();
            services.AddTransient<ISiretRepository, SiretRepository>();
            services.AddTransient<ISousModuleRepository, SousModuleRepository>();
            services.AddTransient<ISousModuleModuleRepository, SousModuleModuleRepository>();
            services.AddTransient<ITypeModuleRepository, TypeModuleRepository>();
            services.AddTransient<IUrlActionRepository, UrlActionRepository>();
            services.AddTransient<IUrlActionUrlWebRepository, UrlActionUrlWebRepository>();
            services.AddTransient<IUrlFormationRepository, UrlFormationRepository>();
            services.AddTransient<IUrlFormationUrlWebRepository, UrlFormationUrlWebRepository>();
            services.AddTransient<IUrlWebRepository, UrlWebRepository>();
            services.AddTransient<IWebRepository, WebRepository>();
            services.AddTransient<IWebUrlWebRepository, WebUrlWebRepository>();
            services.AddTransient<IWiseNetworkContextRepository, WiseNetworkContextRepository>();
            services.AddTransient<IAuthorizedDomainsRepository, AuthorizedDomainsRepository>();

            //Ajout du service Swagger
            services.AddSwaggerDocument(config =>
            {
                config.PostProcess = document =>
                {
                    document.Info.Version = "v1";
                    document.Info.Title = "Wise API";
                    document.Info.Description = "API utilisée pour l'application Wise";
                    document.Info.TermsOfService = "None";
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseAuthentication();
            app.UseHttpsRedirection();

            //Ajout génération du Swagger
            app.UseOpenApi();
            app.UseSwaggerUi3();


            app.UseMvc();
        }
    }
}

