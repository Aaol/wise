using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class DomaineFormationRepository : BaseRepository<DomaineFormation>, IDomaineFormationRepository
    {
        public DomaineFormationRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
