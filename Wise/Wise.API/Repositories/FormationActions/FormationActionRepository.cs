using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class FormationActionRepository : BaseRepository<FormationAction>, IFormationActionRepository
    {
        public FormationActionRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
