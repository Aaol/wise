using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;
namespace Wise.API.Repositories
{
    /// <summary>
    ///     Interface du repostiory pour les <see cref="Adresse"/>.
    /// </summary>
    public interface IAdresseRepository : IBaseRepository<Adresse>
    {

    }
}
