using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    /// <summary>
    ///     Classe de repository pour les <see cref="Adresse"/>.
    /// </summary>
    public class AdresseRepository : BaseRepository<Adresse>, IAdresseRepository
    {
        /// <summary>
        ///     Initialise une nouvelle instance de la classe <see cref="AdresseRepository"/>.
        /// </summary>
        /// <param name="context">Contexte d'accès à la base de données</param>
        public AdresseRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
