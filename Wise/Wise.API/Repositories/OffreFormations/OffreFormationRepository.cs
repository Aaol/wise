using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class OffreFormationRepository : BaseRepository<OffreFormation>, IOffreFormationRepository
    {
        public OffreFormationRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}