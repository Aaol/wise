using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class LigneRepository : BaseRepository<Ligne>, ILigneRepository
    {
        public LigneRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
