using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;
namespace Wise.API.Repositories
{
    /// <summary>
    ///     Interface de repository pour les <see cref="CodeRome"/>.
    /// </summary>
    public interface ICodeRomeRepository : IBaseRepository<CodeRome>
    {

    }
}
