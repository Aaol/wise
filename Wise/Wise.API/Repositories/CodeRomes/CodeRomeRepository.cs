using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    /// <summary>
    ///     Créé une nouvelle interface de repository pour les <see cref="CodeRome"/>.
    /// </summary>
    public class CodeRomeRepository : BaseRepository<CodeRome>, ICodeRomeRepository
    {
        /// <summary>
        ///     Initialise une nouvelle instance de la classe <see cref="CodeRomeRepository"/>.
        /// </summary>
        /// <param name="context">Contexte d'accès à la base de donénes</param>
        public CodeRomeRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
