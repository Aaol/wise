using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class NumtelRepository : BaseRepository<Numtel>, INumtelRepository
    {
        public NumtelRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}