using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wise.API.Repositories
{
    /// <summary>
    ///     Repository contenant toute les méthodes du BREAD pour un repository.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IBaseRepository<T>
    {
        /// <summary>
        ///     Récupère l'entité correspondant à l'identifiant fourni.
        /// </summary>
        /// <param name="id">Identifiant de l'entité</param>
        /// <returns>Entité demandée.</returns>
        T GetEntity(int id);

        /// <summary>
        ///     Récupère la liste des entités.
        /// </summary>
        /// <returns>Liste des entités.</returns>
        IQueryable<T> GetEntities();

        /// <summary>
        ///     Supprime l'entité correspondant à l'identifiant fourni.
        /// </summary>
        /// <param name="id">Identifiant de l'entité.</param>
        void DeleteEntity(int id);

        /// <summary>
        ///     Ajoute l'entité fournie à la base de données.
        /// </summary>
        /// <param name="entity">Element à ajouter à la base de données.</param>
        /// <returns>Entité après ajout.</returns>
        T AddEntity(T entity);

        /// <summary>
        ///     Met à jour l'entité en base de données.
        /// </summary>
        /// <param name="entity">Entité à mettre à jour.</param>
        /// <returns>Entité mise à jour.</returns>
        T UpdateEntity(T entity);

        /// <summary>
        ///     Récupère le nombre d'entités présente en base de données.
        /// </summary>
        /// <returns>Nombre d'éléments en base de données.</returns>
        int GetEntititiesCount();
    }
}
