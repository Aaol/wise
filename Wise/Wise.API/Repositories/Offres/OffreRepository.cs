using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class OffreRepository : BaseRepository<Offre>, IOffreRepository
    {
        public OffreRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}