using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class WiseNetworkContextRepository : BaseRepository<WiseNetworkContext>, IWiseNetworkContextRepository
    {
        public WiseNetworkContextRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}