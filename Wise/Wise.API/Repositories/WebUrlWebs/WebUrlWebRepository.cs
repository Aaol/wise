using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class WebUrlWebRepository : BaseRepository<WebUrlWeb>, IWebUrlWebRepository
    {
        public WebUrlWebRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}