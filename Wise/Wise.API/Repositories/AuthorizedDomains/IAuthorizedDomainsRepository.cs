using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    /// <summary>
    ///     Interface de repository pour les <see cref="AuthorizedDomains"/>.
    /// </summary>
    public interface IAuthorizedDomainsRepository : IBaseRepository<AuthorizedDomains>
    {
        /// <summary>
        ///     Indique si le domaine fourni en paramètre fait parti des domaines autorisés.
        /// </summary>
        /// <param name="domain">Domaine que l'on veut étudier.</param>
        /// <returns></returns>
        bool IsDomainAuthorized(string domain);
    }
}
