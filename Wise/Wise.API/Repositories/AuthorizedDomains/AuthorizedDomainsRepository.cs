using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    /// <summary>
    ///     Classe de repository pour les <see cref="AuthorizedDomainsRepository"/>.
    /// </summary>
    public class AuthorizedDomainsRepository : BaseRepository<AuthorizedDomains>, IAuthorizedDomainsRepository
    {
        /// <summary>
        ///     Initialise une nouvelle instance de la classe <see cref="AuthorizedDomainsRepository"/>
        /// </summary>
        /// <param name="context">Contexte d'accès à la base de données</param>
        public AuthorizedDomainsRepository(WiseNetworkContext context) : base(context)
        {
        }

        /// <summary>
        ///     Indique si le domaine fourni en paramètre fait parti des domaines autorisés.
        /// </summary>
        /// <param name="domain">Domaine que l'on veut étudier.</param>
        /// <returns></returns>
        public bool IsDomainAuthorized(string domain)
        {
            return this.Context.AuthorizedDomains.Any(authorizedDomain => authorizedDomain.Domain == domain);
        }
    }
}
