using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class OrganismeFormationResponsableRepository : BaseRepository<OrganismeFormationResponsable>, IOrganismeFormationResponsableRepository
    {
        public OrganismeFormationResponsableRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}