using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class PeriodeRepository : BaseRepository<Periode>, IPeriodeRepository
    {
        public PeriodeRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}