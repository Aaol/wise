using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class ObjectifGeneralFormationRepository : BaseRepository<ObjectifGeneralFormation>, IObjectifGeneralFormationRepository
    {
        public ObjectifGeneralFormationRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}