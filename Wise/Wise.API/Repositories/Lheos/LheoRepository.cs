using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    /// <summary>
    ///     Classe de repository pour les <see cref="Lheo"/>.
    /// </summary>
    public class LheoRepository : BaseRepository<Lheo>, ILheoRepository
    {
        public LheoRepository(WiseNetworkContext context) : base(context)
        {

        }

        /// <summary>
        ///     Récupère l'objet <see cref="Lheo"/> contenant toutes les offres de formations.
        /// </summary>
        /// <returns></returns>
        public Lheo GetLheoFullLoaded()
        {
            return this.Context.Lheo
                .Include(lheo => lheo.IdOffreNavigation)
                    .ThenInclude(offre => offre.OffreFormation)
                .First();
        }
    }
}
