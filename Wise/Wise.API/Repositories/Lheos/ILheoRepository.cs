using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;
namespace Wise.API.Repositories
{
    /// <summary>
    ///     Interface de repository pour les <see cref="Lheo"/>.
    /// </summary>
    public interface ILheoRepository : IBaseRepository<Lheo>
    {
        /// <summary>
        ///     Récupère l'objet <see cref="Lheo"/> contenant toutes les offres de formations.
        /// </summary>
        /// <returns></returns>
        Lheo GetLheoFullLoaded();
    }
}
