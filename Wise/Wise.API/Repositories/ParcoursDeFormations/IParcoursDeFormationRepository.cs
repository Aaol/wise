using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;
namespace Wise.API.Repositories
{
    public interface IParcoursDeFormationRepository : IBaseRepository<ParcoursDeFormation>
    {
    }
}