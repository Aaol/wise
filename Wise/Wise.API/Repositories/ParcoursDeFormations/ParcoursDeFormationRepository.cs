using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class ParcoursDeFormationRepository : BaseRepository<ParcoursDeFormation>, IParcoursDeFormationRepository
    {
        public ParcoursDeFormationRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}