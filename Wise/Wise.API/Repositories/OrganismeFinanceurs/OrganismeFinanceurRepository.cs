using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class OrganismeFinanceurRepository : BaseRepository<OrganismeFinanceur>, IOrganismeFinanceurRepository
    {
        public OrganismeFinanceurRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}