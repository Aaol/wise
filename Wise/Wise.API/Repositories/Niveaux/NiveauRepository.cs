﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class NiveauRepository : BaseRepository<Niveaux>, INiveauRepository
    {
        #region Constructors

        public NiveauRepository(WiseNetworkContext context) : base(context)
        {

        }

        #endregion
    }
}
