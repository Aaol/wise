using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class ContactFormateurRepository : BaseRepository<ContactFormateur>, IContactFormateurRepository
    {
        public ContactFormateurRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
