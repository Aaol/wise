using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class UrlWebRepository : BaseRepository<UrlWeb>, IUrlWebRepository
    {
        public UrlWebRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}