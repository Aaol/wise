using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    /// <summary>
    ///     Interface du repository pour les <see cref="User"/>.
    /// </summary>
    public interface IUserRepository : IBaseRepository<User>
    {
        /// <summary>
        ///     Récupère l'utilisateur en fonction de son Login.
        /// </summary>
        /// <param name="login">Login que l'on cherche</param>
        /// <returns>Utilisateur correspondant au Login</returns>
        User GetUserByLogin(string login);

        /// <summary>
        ///     Valide l'utilisateur fourni en paramètre.
        /// </summary>
        /// <param name="id">Identifiant de l'utilisateur.</param>
        void ValidateUser(int id);

        /// <summary>
        ///     Met à jour le rôle de l'utilisateur fourni en paramètre.
        /// </summary>
        /// <param name="login">Adresse email de l'utilisateur.</param>
        /// <param name="role">Nouveau rôle de l'utilisateur.</param>
        void UpdateUserRole(string login, string role);
    }
}
