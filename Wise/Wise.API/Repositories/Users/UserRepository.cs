using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    /// <summary>
    ///     Classe de Repository pour les <see cref="User"/>.
    /// </summary>
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        #region Constructors

        /// <summary>
        ///     Initialise une nouvelle instance de la classe <see cref="UserRepository"/>;
        /// </summary>
        /// <param name="context"></param>
        public UserRepository(WiseNetworkContext context) : base(context)
        {

        }


        #endregion

        #region Methods

        /// <summary>
        ///     Récupère l'utilisateur en fonction de son Login.
        /// </summary>
        /// <param name="login">Login que l'on cherche</param>
        /// <returns>Utilisateur correspondant au Login</returns>
        public User GetUserByLogin(string login)
        {
            return this.Context.Users
                      .First(u => u.Email == login);
        }

        /// <summary>
        ///     Valide l'utilisateur fourni en paramètre.
        /// </summary>
        /// <param name="id">Identifiant de l'utilisateur.</param>
        public void ValidateUser(int id)
        {
            User user = null;
            try
            {
                user = this.Context.Users.Find(id);
            }
            catch (Exception ex)
            {
                throw new Exception("Impossible de trouver l'utilisateur", ex);
            }
            user.Validated = true;
            this.Context.SaveChanges();
        }

        /// <summary>
        ///     Met à jour le rôle de l'utilisateur fourni en paramètre.
        /// </summary>
        /// <param name="login">Adresse email de l'utilisateur.</param>
        /// <param name="role">Nouveau rôle de l'utilisateur.</param>
        public void UpdateUserRole(string login, string role)
        {
            try
            {
                User user = null;
                try
                {
                    user = this.GetUserByLogin(login);
                }
                catch (Exception ex)
                {
                    throw new Exception("L'utilisateur n'existe pas en base de données", ex);
                }
                user.Role = role;
                this.Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Une erreur est survenue pendant l'enregistrement", ex);
            }
        }

        #endregion
    }
}
