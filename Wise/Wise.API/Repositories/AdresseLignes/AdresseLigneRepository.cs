using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    /// <summary>
    ///     Classe de repository pour les <see cref="AdresseLigne"/>%.
    /// </summary>
    public class AdresseLigneRepository : BaseRepository<AdresseLigne>, IAdresseLigneRepository
    {
        /// <summary>
        ///     Initialise une nouvelle instance de la classe <see cref="AdresseLigneRepository"/>.
        /// </summary>
        /// <param name="context">Contexte d'accès à la base de données.</param>
        public AdresseLigneRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
