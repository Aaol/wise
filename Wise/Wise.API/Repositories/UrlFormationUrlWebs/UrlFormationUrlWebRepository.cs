using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class UrlFormationUrlWebRepository : BaseRepository<UrlFormationUrlWeb>, IUrlFormationUrlWebRepository
    {
        public UrlFormationUrlWebRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}