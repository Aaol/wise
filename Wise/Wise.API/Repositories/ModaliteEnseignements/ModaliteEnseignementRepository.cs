using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class ModaliteEnseignementRepository : BaseRepository<ModaliteEnseignement>, IModaliteEnseignementRepository
    {
        public ModaliteEnseignementRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
