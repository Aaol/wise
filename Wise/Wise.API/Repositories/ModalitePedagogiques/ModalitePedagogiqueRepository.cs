using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class ModalitePedagogiqueRepository : BaseRepository<ModalitePedagogique>, IModalitePedagogiqueRepository
    {
        public ModalitePedagogiqueRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
