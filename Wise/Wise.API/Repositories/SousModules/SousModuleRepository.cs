using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class SousModuleRepository : BaseRepository<SousModule>, ISousModuleRepository
    {
        public SousModuleRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}