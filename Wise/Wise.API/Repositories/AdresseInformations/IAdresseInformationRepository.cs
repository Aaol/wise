using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;
namespace Wise.API.Repositories
{
    /// <summary>
    ///     Interface du Repository pour les <see cref="AdresseInformation"/>;
    /// </summary>
    public interface IAdresseInformationRepository : IBaseRepository<AdresseInformation>
    {
    }
}
