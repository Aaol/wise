using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    /// <summary>
    ///     Classe de repository pour les <see cref="AdresseInformation"/>.
    /// </summary>
    public class AdresseInformationRepository : BaseRepository<AdresseInformation>, IAdresseInformationRepository
    {
        /// <summary>
        ///     Initialise une nouvelle isntance de la classe <see cref="AdresseInformationRepository"/>
        /// </summary>
        /// <param name="context">Contexte d'accès à la base de données.</param>
        public AdresseInformationRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
