using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class FormationCertificationRepository : BaseRepository<FormationCertification>, IFormationCertificationRepository
    {
        public FormationCertificationRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
