using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    /// <summary>
    ///     Classe de base de repository pour les éléments de type <seealso cref="T"/>.
    /// </summary>
    /// <typeparam name="T">Modèle correspondant au Repository courant.</typeparam>
    public class BaseRepository<T> : IBaseRepository<T>
        where T: class
    {
        #region Properties

        /// <summary>
        ///     Contexte d'accès à la base de données
        /// </summary>
        protected WiseNetworkContext Context { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initialise une nouvelle instance de la classe <see cref="BaseRepository{T}"/>
        /// </summary>
        /// <param name="context">Contexte d'accès à la base de données.</param>
        public BaseRepository(WiseNetworkContext context)
        {
            this.Context = context;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Ajoute l'entité fournie à la base de données.
        /// </summary>
        /// <param name="entity">Element à ajouter à la base de données.</param>
        /// <returns>Entité après ajout.</returns>
        public T AddEntity(T entity)
        {
            this.Context.Set<T>()
                .Add(entity);
            this.Context.SaveChanges();
            return entity;
        }

        /// <summary>
        ///     Supprime l'entité correspondant à l'identifiant fourni.
        /// </summary>
        /// <param name="id">Identifiant de l'entité.</param>
        public void DeleteEntity(int id)
        {
            this.Context.Set<T>()
                .Remove(this.Context.Set<T>().Find(id));
            this.Context.SaveChanges();
        }


        /// <summary>
        ///     Récupère la liste des entités.
        /// </summary>
        /// <returns>Liste des entités.</returns>
        public IQueryable<T> GetEntities()
        {
            return this.Context.Set<T>();
        }

        /// <summary>
        ///     Récupère l'entité correspondant à l'identifiant fourni.
        /// </summary>
        /// <param name="id">Identifiant de l'entité</param>
        /// <returns>Entité demandée.</returns>
        public T GetEntity(int id)
        {
            return this.Context.Set<T>().Find(id);
        }

        /// <summary>
        ///     Met à jour l'entité en base de données.
        /// </summary>
        /// <param name="entity">Entité à mettre à jour.</param>
        /// <returns>Entité mise à jour.</returns>
        public T UpdateEntity(T entity)
        {
            this.Context.Update(entity);
            this.Context.SaveChanges();
            return entity;
        }

        /// <summary>
        ///     Récupère le nombre d'entités présente en base de données.
        /// </summary>
        /// <returns>Nombre d'éléments en base de données.</returns>
        public int GetEntititiesCount()
        {
            return this.Context.Set<T>().Count();
        }

        #endregion
    }
}
