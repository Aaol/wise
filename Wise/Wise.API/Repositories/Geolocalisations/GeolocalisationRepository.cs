using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class GeolocalisationRepository : BaseRepository<Geolocalisation>, IGeolocalisationRepository
    {
        public GeolocalisationRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
