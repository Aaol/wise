using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class EtatRecrutementRepository : BaseRepository<EtatRecrutement>, IEtatRecrutementRepository
    {
        public EtatRecrutementRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
