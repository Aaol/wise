using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    /// <summary>
    ///     Classe de repository pour les <see cref="CodeFormacode"/>.
    /// </summary>
    public class CodeFormacodeRepository : BaseRepository<CodeFormacode>, ICodeFormacodeRepository
    {
        /// <summary>
        ///     Initialise une nouvelle instance de la classe <see cref="CodeFormacodeRepository"/>.
        /// </summary>
        /// <param name="context">Cotnexte d'accès à la base de données</param>
        public CodeFormacodeRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
