using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class ModulePrerequisReferenceModuleRepository : BaseRepository<ModulePrerequisReferenceModule>, IModulePrerequisReferenceModuleRepository
    {
        public ModulePrerequisReferenceModuleRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}