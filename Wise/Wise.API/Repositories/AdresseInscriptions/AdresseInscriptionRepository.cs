using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    /// <summary>
    ///     Classe de repository pour les <see cref="AdresseInscription"/>.
    /// </summary>
    public class AdresseInscriptionRepository : BaseRepository<AdresseInscription>, IAdresseInscriptionRepository
    {
        /// <summary>
        ///     Initialise une nouvelle instance de la classe <see cref="AdresseInscriptionRepository"/>
        /// </summary>
        /// <param name="context">Contexte d'accès à la base de données.</param>
        public AdresseInscriptionRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
