using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;
namespace Wise.API.Repositories
{
    /// <summary>
    ///     Interface de repository pour les <see cref="AdresseInscription"/>.
    /// </summary>
    public interface IAdresseInscriptionRepository : IBaseRepository<AdresseInscription>
    {
    }
}
