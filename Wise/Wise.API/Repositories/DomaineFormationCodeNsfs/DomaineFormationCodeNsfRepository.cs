using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class DomaineFormationCodeNsfRepository : BaseRepository<DomaineFormationCodeNsf>, IDomaineFormationCodeNsfRepository
    {
        public DomaineFormationCodeNsfRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
