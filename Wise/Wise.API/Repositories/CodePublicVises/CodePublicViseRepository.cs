using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    /// <summary>
    ///     Classe de repository pour les <see cref="CodePublicVise"/>.
    /// </summary>
    public class CodePublicViseRepository : BaseRepository<CodePublicVise>, ICodePublicViseRepository
    {
        /// <summary>
        ///     Initialise une nouvelle instance de la classe <see cref="CodePublicViseRepository"/>.
        /// </summary>
        /// <param name="context">Contexte d'accès à la base de données.</param>
        public CodePublicViseRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
