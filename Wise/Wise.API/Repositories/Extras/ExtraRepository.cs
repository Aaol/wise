using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class ExtraRepository : BaseRepository<Extra>, IExtraRepository
    {
        public ExtraRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
