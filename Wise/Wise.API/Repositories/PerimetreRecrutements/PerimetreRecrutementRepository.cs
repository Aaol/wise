using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class PerimetreRecrutementRepository : BaseRepository<PerimetreRecrutement>, IPerimetreRecrutementRepository
    {
        public PerimetreRecrutementRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
