using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    /// <summary>
    ///     Classe de repository pour les <see cref="Certification"/>.
    /// </summary>
    public class CertificationRepository : BaseRepository<Certification>, ICertificationRepository
    {
        /// <summary>
        ///     Initialise une nouvelle isntance de la classe <see cref="CertificationRepository"/>.
        /// </summary>
        /// <param name="context">Contexte daccès à la base de données.</param>
        public CertificationRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
