using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;
namespace Wise.API.Repositories
{
    /// <summary>
    ///     Interface de repository pour les <see cref="Certification"/>.
    /// </summary>
    public interface ICertificationRepository : IBaseRepository<Certification>
    {
    }
}
