using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class SessionRepository : BaseRepository<Session>, ISessionRepository
    {
        public SessionRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}