using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class CoordonneeOrganismeRepository : BaseRepository<CoordonneeOrganisme>, ICoordonneeOrganismeRepository
    {
        public CoordonneeOrganismeRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
