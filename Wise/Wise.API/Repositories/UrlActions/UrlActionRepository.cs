using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class UrlActionRepository : BaseRepository<UrlAction>, IUrlActionRepository
    {
        public UrlActionRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}