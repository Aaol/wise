
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class SousModuleModuleRepository : BaseRepository<SousModuleModule>, ISousModuleModuleRepository
    {
        public SousModuleModuleRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}