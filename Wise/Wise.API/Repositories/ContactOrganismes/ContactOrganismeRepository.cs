using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class ContactOrganismeRepository : BaseRepository<ContactOrganisme>, IContactOrganismeRepository
    {
        public ContactOrganismeRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
