using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class ModulePrerequisRepository : BaseRepository<ModulePrerequis>, IModulePrerequisRepository
    {
        public ModulePrerequisRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}