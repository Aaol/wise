using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class WebRepository : BaseRepository<Web>, IWebRepository
    {
        public WebRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}