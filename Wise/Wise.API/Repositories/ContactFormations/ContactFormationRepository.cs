using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class ContactFormationRepository : BaseRepository<ContactFormation>, IContactFormationRepository
    {
        public ContactFormationRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
