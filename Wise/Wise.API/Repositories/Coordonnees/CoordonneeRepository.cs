using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class CoordonneeRepository : BaseRepository<Coordonnee>, ICoordonneeRepository
    {
        public CoordonneeRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
