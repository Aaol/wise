using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class PotentielCodeFormacodeRepository : BaseRepository<PotentielCodeFormacode>, IPotentielCodeFormacodeRepository
    {
        public PotentielCodeFormacodeRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}