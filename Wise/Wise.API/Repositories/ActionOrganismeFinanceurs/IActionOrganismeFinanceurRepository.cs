using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;
namespace Wise.API.Repositories
{
    /// <summary>
    ///     Interface du Repository pour les <see cref="ActionOrganismeFinanceur"/>.
    /// </summary>
    public interface IActionOrganismeFinanceurRepository : IBaseRepository<ActionOrganismeFinanceur>
    {
    }
}
