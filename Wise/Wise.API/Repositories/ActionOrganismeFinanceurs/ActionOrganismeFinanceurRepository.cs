using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    /// <summary>
    ///     Repository pour les <see cref="ActionOrganismeFinanceur"/>
    /// </summary>
    public class ActionOrganismeFinanceurRepository : BaseRepository<ActionOrganismeFinanceur>, IActionOrganismeFinanceurRepository
    {
        /// <summary>
        ///     Initialise une nouvelle isntance de la classe <see cref="ActionOrganismeFinanceurRepository"/>.
        /// </summary>
        /// <param name="context">Contexte d'accès à la base de données.</param>
        public ActionOrganismeFinanceurRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
