using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class UrlFormationRepository : BaseRepository<UrlFormation>, IUrlFormationRepository
    {
        public UrlFormationRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}