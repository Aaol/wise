using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class DomaineFormationCodeFormacodeRepository : BaseRepository<DomaineFormationCodeFormacode>, IDomaineFormationCodeFormacodeRepository
    {
        public DomaineFormationCodeFormacodeRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
