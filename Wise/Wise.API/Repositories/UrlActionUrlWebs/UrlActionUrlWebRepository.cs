using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class UrlActionUrlWebRepository : BaseRepository<UrlActionUrlWeb>, IUrlActionUrlWebRepository
    {
        public UrlActionUrlWebRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}