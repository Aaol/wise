using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;
namespace Wise.API.Repositories
{
    /// <summary>
    ///     Interface de repository pour les <see cref="CodeNsf"/>.
    /// </summary>
    public interface ICodeNsfRepository : IBaseRepository<CodeNsf>
    {

    }
}
