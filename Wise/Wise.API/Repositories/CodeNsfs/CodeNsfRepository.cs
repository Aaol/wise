using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    /// <summary>
    ///     Classe de repostiroy pour les <see cref="CodeNsf"/>.
    /// </summary>
    public class CodeNsfRepository : BaseRepository<CodeNsf>, ICodeNsfRepository
    {
        /// <summary>
        ///     Intiialsie une nouvelle instance de la classe <see cref="CodeNsfRepository"/>%.
        /// </summary>
        /// <param name="context">Contexte d'accès à la base de données.</param>
        public CodeNsfRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
