using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class DomaineFormationCodeRomeRepository : BaseRepository<DomaineFormationCodeRome>, IDomaineFormationCodeRomeRepository
    {
        public DomaineFormationCodeRomeRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
