using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class ReferenceModuleRepository : BaseRepository<ReferenceModule>, IReferenceModuleRepository
    {
        public ReferenceModuleRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}