using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;
namespace Wise.API.Repositories
{
    /// <summary>
    ///     Interface du Repository pour les  <see cref="Action"/>.
    /// </summary>
    public interface IActionRepository : IBaseRepository<Action>
    {
    }
}
