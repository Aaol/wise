using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    /// <summary>
    ///     Classe du repository pour les <see cref="Action"/>
    /// </summary>
    public class ActionRepository : BaseRepository<Action>, IActionRepository
    {
        /// <summary>
        ///     Initialise une nouvelle instance de la classe <see cref="ActionRepository"/>.
        /// </summary>
        /// <param name="context">Contexte d'accès à la base de données</param>
        public ActionRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
