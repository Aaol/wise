using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class LieuDeFormationRepository : BaseRepository<LieuDeFormation>, ILieuDeFormationRepository
    {
        public LieuDeFormationRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
