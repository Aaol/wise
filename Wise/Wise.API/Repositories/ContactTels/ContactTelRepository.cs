using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Repositories
{
    public class ContactTelRepository : BaseRepository<ContactTel>, IContactTelRepository
    {
        public ContactTelRepository(WiseNetworkContext context) : base(context)
        {

        }
    }
}
