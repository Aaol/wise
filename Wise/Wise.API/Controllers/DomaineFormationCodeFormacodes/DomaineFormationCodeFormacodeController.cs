using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.DomaineFormationCodeFormacodes
{
    [Route("api/[controller]")]
    [ApiController]
    public class DomaineFormationCodeFormacodeController : BaseController<IDomaineFormationCodeFormacodeRepository, DomaineFormationCodeFormacode>
    {
        public DomaineFormationCodeFormacodeController(IDomaineFormationCodeFormacodeRepository repository) : base(repository)
        {

        }
    }
}
