using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.Webs
{
    [Route("api/[controller]")]
    [ApiController]
    public class WebController : BaseController<IWebRepository, Web>
    {
        public WebController(IWebRepository repository) : base(repository)
        {

        }
    }
}