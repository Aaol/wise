using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.UrlFormationUrlWebs
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrlFormationUrlWebController : BaseController<IUrlFormationUrlWebRepository, UrlFormationUrlWeb>
    {
        public UrlFormationUrlWebController(IUrlFormationUrlWebRepository repository) : base(repository)
        {

        }
    }
}