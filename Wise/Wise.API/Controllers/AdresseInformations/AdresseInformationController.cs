using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.AdresseInformations
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdresseInformationController : BaseController<IAdresseInformationRepository, AdresseInformation>
    {
        public AdresseInformationController(IAdresseInformationRepository repository) : base(repository)
        {

        }
    }
}
