using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.DomaineFormations
{
    [Route("api/[controller]")]
    [ApiController]
    public class DomaineFormationController : BaseController<IDomaineFormationRepository, DomaineFormation>
    {
        public DomaineFormationController(IDomaineFormationRepository repository) : base(repository)
        {

        }
    }
}
