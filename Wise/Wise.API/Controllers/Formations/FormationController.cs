using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.Formations
{
    [Route("api/[controller]")]
    [ApiController]
    public class FormationController : BaseController<IFormationRepository, Formation>
    {
        public FormationController(IFormationRepository repository) : base(repository)
        {

        }
    }
}
