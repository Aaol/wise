using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers
{
    /// <summary>
    ///     Controlleur pour le <see cref="AuthorizedDomains"/>
    /// </summary>
    [Route("api/[controller]")]
    public class AuthorizedDomainController : BaseController<IAuthorizedDomainsRepository, AuthorizedDomains>
    {
        /// <summary>
        ///     Initialise une nouvelle instance de la classe <see cref="AuthorizedDomainController"/>
        /// </summary>
        /// <param name="repository"></param>
        public AuthorizedDomainController(IAuthorizedDomainsRepository repository) : base(repository)
        {
        }
    }
}
