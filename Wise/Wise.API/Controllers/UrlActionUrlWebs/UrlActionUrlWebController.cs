using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.UrlActionUrlWebs
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrlActionUrlWebController : BaseController<IUrlActionUrlWebRepository, UrlActionUrlWeb>
    {
        public UrlActionUrlWebController(IUrlActionUrlWebRepository repository) : base(repository)
        {

        }
    }
}