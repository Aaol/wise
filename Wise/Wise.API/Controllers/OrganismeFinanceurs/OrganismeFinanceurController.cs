using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.OrganismeFinanceurs
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrganismeFinanceurController : BaseController<IOrganismeFinanceurRepository, OrganismeFinanceur>
    {
        public OrganismeFinanceurController(IOrganismeFinanceurRepository repository) : base(repository)
        {

        }
    }
}