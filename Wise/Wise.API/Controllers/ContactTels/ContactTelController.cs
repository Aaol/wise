using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.ContactTels
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactTelController : BaseController<IContactTelRepository, ContactTel>
    {
        public ContactTelController(IContactTelRepository repository) : base(repository)
        {

        }
    }
}
