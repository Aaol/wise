using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.OrganismeFormationResponsables
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrganismeFormationResponsableController : BaseController<IOrganismeFormationResponsableRepository, OrganismeFormationResponsable>
    {
        public OrganismeFormationResponsableController(IOrganismeFormationResponsableRepository repository) : base(repository)
        {

        }
    }
}