using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.ActionOrganismeFinanceurs
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActionOrganismeFinanceurController : BaseController<IActionOrganismeFinanceurRepository, ActionOrganismeFinanceur>
    {
        public ActionOrganismeFinanceurController(IActionOrganismeFinanceurRepository repository) : base(repository)
        {

        }
    }
}
