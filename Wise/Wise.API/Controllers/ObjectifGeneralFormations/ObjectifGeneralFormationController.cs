using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.ObjectifGeneralFormations
{
    [Route("api/[controller]")]
    [ApiController]
    public class ObjectifGeneralFormationController : BaseController<IObjectifGeneralFormationRepository, ObjectifGeneralFormation>
    {
        public ObjectifGeneralFormationController(IObjectifGeneralFormationRepository repository) : base(repository)
        {

        }
    }
}