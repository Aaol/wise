using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.ParcoursDeFormations
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParcoursDeFormationController : BaseController<IParcoursDeFormationRepository, ParcoursDeFormation>
    {
        public ParcoursDeFormationController(IParcoursDeFormationRepository repository) : base(repository)
        {

        }
    }
}