using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.AdresseInscriptions
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdresseInscriptionController : BaseController<IAdresseInscriptionRepository, AdresseInscription>
    {
        public AdresseInscriptionController(IAdresseInscriptionRepository repository) : base(repository)
        {

        }
    }
}
