using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.CodePublicVises
{
    [Route("api/[controller]")]
    [ApiController]
    public class CodePublicViseController : BaseController<ICodePublicViseRepository, CodePublicVise>
    {
        public CodePublicViseController(ICodePublicViseRepository repository) : base(repository)
        {

        }
    }
}
