using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.CoordonneeOrganismes
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoordonneeOrganismeController : BaseController<ICoordonneeOrganismeRepository, CoordonneeOrganisme>
    {
        public CoordonneeOrganismeController(ICoordonneeOrganismeRepository repository) : base(repository)
        {

        }
    }
}
