using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.Offres
{
    [Route("api/[controller]")]
    [ApiController]
    public class OffreController : BaseController<IOffreRepository, Offre>
    {
        public OffreController(IOffreRepository repository) : base(repository)
        {

        }
    }
}