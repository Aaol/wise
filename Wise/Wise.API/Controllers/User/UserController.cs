using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Wise.API.Config;
using Wise.API.Helpers;
using Wise.API.Repositories;
using Wise.API.Services;
using Wise.DbLib.Models;

namespace Wise.API.Controllers
{
    /// <summary>
    ///     Contrôleur pour les utilisateurs
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        #region Properties

        /// <summary>
        ///     Utilisé pour la génération des utilisateurs
        /// </summary>
        private RNGCryptoServiceProvider RNGCryptoService { get; set; }

        /// <summary>
        ///     Configuration pour la génération du jeton JWT.
        /// </summary>
        private IOptions<JwtKeyConfig> JwtKeyConfig { get; set; }

        /// <summary>
        ///     Repository pour les domains autorisés
        /// </summary>
        private IAuthorizedDomainsRepository DomainsRepository { get; set; }

        /// <summary>
        ///     Service d'envoi de mail
        /// </summary>
        private IMailService MailService { get; set; }

        private IUserRepository Repository { get; set; }

        #endregion

        #region Constructors
        /// <summary>
        ///     Initialise une nouvelle instance de la classe <see cref="UserController"/>.
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="jwtKeyConfig"></param>
        /// <param name="domainsRepository"></param>
        /// <param name=""></param>
        public UserController(IUserRepository repository
            , IOptions<JwtKeyConfig> jwtKeyConfig
            , IAuthorizedDomainsRepository domainsRepository
            , IMailService mailService)
        {
            this.RNGCryptoService = new RNGCryptoServiceProvider();
            this.JwtKeyConfig = jwtKeyConfig;
            this.DomainsRepository = domainsRepository;
            this.MailService = mailService;
            this.Repository = repository;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Créé un nouveau <see cref="User"/>.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost(Order = -1)]
        public IActionResult CreateUser([FromBody] UserCreation user)
        {
            if (user.ConfirmPassword != user.Password)
            {
                return StatusCode(400, "Le mot de passe et la confirmation du mot de passe ne correspondent pas.");
            }

            if (!user.Email.IsValidEmail())
            {
                return StatusCode(400, "L'adresse email n'est pas valide");
            }
            // Création du nouvel utilisateur
            User newUser = new User()
            {
                Email = user.Email,
            };

            // Voir https://medium.com/@mehanix/lets-talk-security-salted-password-hashing-in-c-5460be5c3aae pour le hashage du mot de passe

            // Création du sel pour le hashage.
            byte[] salt;
            this.RNGCryptoService.GetBytes(salt = new byte[16]);

            Rfc2898DeriveBytes pdfkf2 = new Rfc2898DeriveBytes(user.Password, salt, 10000);

            byte[] hash = pdfkf2.GetBytes(20);

            byte[] hashBytes = new byte[36];

            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);

            string passwordHash = Convert.ToBase64String(hashBytes);
            string saltString = Convert.ToBase64String(salt);

            try
            {
                // On vient sauvegarder l'utilisateur courant.
                newUser.Validated = this.DomainsRepository.IsDomainAuthorized(newUser.Email.Split('@')[1]);
                newUser.Password = passwordHash;
                newUser.Salt = saltString;
                this.Repository.AddEntity(newUser);
            }
            catch (Exception)
            {
                return StatusCode(500, "Une erreur est survenue pendant l'ajout de l'utilisateur");
            }
            return StatusCode(201);
        }

        /// <summary>
        ///     Connexion de l'utilisateur
        /// </summary>
        /// <param name="login">Données de connexion de l'utilisateurs</param>
        /// <returns>Renvoie le token JWT</returns>
        [HttpPost("login")]
        public IActionResult Login([FromBody] UserLogin login)
        {
            User user;
            // Récupération de l'utilisateur en fonction de l'
            try
            {
                user = this.Repository.GetUserByLogin(login.Email);
            }
            catch (Exception)
            {
                return StatusCode(404);
            }

            try
            {
                byte[] hashBytes = Convert.FromBase64String(user.Password);

                byte[] salt = new byte[16];
                Array.Copy(hashBytes, 0, salt, 0, 16);
                Rfc2898DeriveBytes pdfkf2 = new Rfc2898DeriveBytes(login.Password, salt, 10000);

                byte[] hash = pdfkf2.GetBytes(20);


                for(int i = 0; i < 20; i++)
                {
                    if(hashBytes[i + 16] != hash[i])
                    {
                        return StatusCode(404);
                    }
                }
            }
            catch (Exception)
            {
                return StatusCode(500);
            }

            List<Claim> claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.Email)
                };

            if(user.Role !=  null)
            {
                claims.Add(new Claim(ClaimTypes.Role, user.Role));
            }

            DateTime issueDate = DateTime.Now;
            DateTime expireDate = issueDate.AddHours(2);
            //On affecte la date d'expiration du token et du refreshtoken en fonction du rôle de l'utilisateur
            
            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JwtKeyConfig.Value.JwtKey));

             JwtSecurityToken token = new JwtSecurityToken(new JwtHeader(new SigningCredentials(key, SecurityAlgorithms.HmacSha256))
                , new JwtPayload("WiseNetwork", "The name of the audience", claims, issueDate, expireDate));
            return Ok(new JwtSecurityTokenHandler().WriteToken(token));
        }

        /// <summary>
        ///     Action qui vient valider l'utilisateur.
        /// </summary>
        /// <param name="id">Identifiant de l'utilisateur</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrateur")]
        [HttpPut("{id}/validate")]
        public IActionResult ValidateUser([FromRoute]int id)
        {
            EntityResponse<bool> response = new EntityResponse<bool>();
            try
            {
                this.Repository.ValidateUser(id);
            }
            catch (Exception ex)
            {
                response.AddError(ex.Message);
                return StatusCode(400, response);
            }
            return Ok(response);
        }

        /// <summary>
        ///     Action utilissé pour envoyer un mail de mot de passe oublié lorsque l'utilisateur n'a plus son mot de passe.
        /// </summary>
        /// <param name="forgotten">Classe contenant le nom de l'utilisateur.</param>
        /// <returns></returns>
        [HttpPost("password/reset")]
        public IActionResult ResetPassword([FromBody] UserPasswordForgotten forgotten)
        {
            try
            {
                this.Repository.GetUserByLogin(forgotten.Username);
            }
            catch (Exception)
            {
                return StatusCode(404);
            }
            StringBuilder body = new StringBuilder();
            body.Append("Alors comme ça on se souvient plus de son mot de passe ?");

            try
            {
                this.MailService.SendMail("Mot de passe oublié", body.ToString(), forgotten.Username, "dev@wise.licorne");
            }
            catch (Exception)
            {

                throw;
            }
            return Ok();
        }

        /// <summary>
        ///     Met à jour le rôle de l'utilisateur avec le login et le rôle fourni.
        /// </summary>
        /// <param name="userRoleUpdate">Classe proxy contenant le rôle et l'utilisateur.</param>
        /// <returns></returns>
        [HttpPut("role")]
        [Authorize(Roles ="Administrateur")]
        public IActionResult EditUserRole([FromBody] UserRoleUpdate userRoleUpdate)
        {
            EntityResponse<bool> response = new EntityResponse<bool>();
            try
            {
                this.Repository.UpdateUserRole(userRoleUpdate.Username, userRoleUpdate.Role);
            }
            catch (Exception ex)
            {
                response.AddError(ex.Message);
                return StatusCode(400, ex);
            }
            return Ok(response);
        }

        #endregion
    }
}
