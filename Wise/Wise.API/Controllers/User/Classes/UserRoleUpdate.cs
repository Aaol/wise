using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wise.API.Controllers
{
    /// <summary>
    ///     Classe utilisé lors de la mise à jour du rôle de l'utilisateur fourni
    /// </summary>
    public class UserRoleUpdate
    {
        /// <summary>
        ///     Nom de l'utilisateur
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        ///     Nouveau rôle de l'utilisateur
        /// </summary>
        public string Role { get; set; }
    }
}
