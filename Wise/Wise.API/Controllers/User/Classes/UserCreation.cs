﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wise.API.Controllers
{
    /// <summary>
    ///     Classe utilisée pour la création des utilisateurs
    /// </summary>
    public class UserCreation
    {
        #region Properties

        /// <summary>
        ///     Email utilisé à la création
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///     Mot de passe.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        ///     Confirmation du mot de passe.
        /// </summary>
        public string ConfirmPassword { get; set; }
        
        #endregion
    }
}
