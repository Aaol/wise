using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wise.API.Controllers
{
    /// <summary>
    ///     Classe utilisée pour le mot de passe oublié
    /// </summary>
    public class UserPasswordForgotten
    {
        public string Username { get; set; }
    }
}
