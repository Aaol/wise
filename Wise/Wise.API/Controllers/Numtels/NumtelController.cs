using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.Numtels
{
    [Route("api/[controller]")]
    [ApiController]
    public class NumtelController : BaseController<INumtelRepository, Numtel>
    {
        public NumtelController(INumtelRepository repository) : base(repository)
        {

        }
    }
}