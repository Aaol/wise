using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.FormationCertifications
{
    [Route("api/[controller]")]
    [ApiController]
    public class FormationCertificationController : BaseController<IFormationCertificationRepository, FormationCertification>
    {
        public FormationCertificationController(IFormationCertificationRepository repository) : base(repository)
        {

        }
    }
}
