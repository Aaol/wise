using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.Sirets
{
    [Route("api/[controller]")]
    [ApiController]
    public class SiretController : BaseController<ISiretRepository, Siret>
    {
        public SiretController(ISiretRepository repository) : base(repository)
        {

        }
    }
}