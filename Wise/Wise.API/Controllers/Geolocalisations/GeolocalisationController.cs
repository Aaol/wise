using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.Geolocalisations
{
    [Route("api/[controller]")]
    [ApiController]
    public class GeolocalisationController : BaseController<IGeolocalisationRepository, Geolocalisation>
    {
        public GeolocalisationController(IGeolocalisationRepository repository) : base(repository)
        {

        }
    }
}
