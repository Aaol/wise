using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.OffreFormations
{
    [Route("api/[controller]")]
    [ApiController]
    public class OffreFormationController : BaseController<IOffreFormationRepository, OffreFormation>
    {
        public OffreFormationController(IOffreFormationRepository repository) : base(repository)
        {

        }
    }
}