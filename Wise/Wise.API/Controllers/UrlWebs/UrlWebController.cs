using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.UrlWebs
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrlWebController : BaseController<IUrlWebRepository, UrlWeb>
    {
        public UrlWebController(IUrlWebRepository repository) : base(repository)
        {

        }
    }
}