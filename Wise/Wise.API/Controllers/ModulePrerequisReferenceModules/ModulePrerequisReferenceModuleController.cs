using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.ModulePrerequisReferenceModules
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModulePrerequisReferenceModuleController : BaseController<IModulePrerequisReferenceModuleRepository, ModulePrerequisReferenceModule>
    {
        public ModulePrerequisReferenceModuleController(IModulePrerequisReferenceModuleRepository repository) : base(repository)
        {

        }
    }
}