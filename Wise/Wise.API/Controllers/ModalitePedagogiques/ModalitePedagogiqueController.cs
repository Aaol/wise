using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.ModalitePedagogiques
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModalitePedagogiqueController : BaseController<IModalitePedagogiqueRepository, ModalitePedagogique>
    {
        public ModalitePedagogiqueController(IModalitePedagogiqueRepository repository) : base(repository)
        {

        }
    }
}
