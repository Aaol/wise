using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.ModaliteEnseignements
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModaliteEnseignementController : BaseController<IModaliteEnseignementRepository, ModaliteEnseignement>
    {
        public ModaliteEnseignementController(IModaliteEnseignementRepository repository) : base(repository)
        {

        }
    }
}
