using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.Lheos
{
    [Route("api/[controller]")]
    [ApiController]
    public class LheoController : BaseController<ILheoRepository, Lheo>
    {
        public LheoController(ILheoRepository repository) : base(repository)
        {

        }

        #region Methods

        /// <summary>
        ///     Export de Lhéo
        /// </summary>
        /// <returns></returns>
        [HttpGet("export")]
        [Authorize(Roles = "Administrateur")]
        public IActionResult ExportLheo()
        {
            Lheo lheo = this.Repository.GetLheoFullLoaded();

            return Ok(lheo);
        }

        #endregion
    }
}
