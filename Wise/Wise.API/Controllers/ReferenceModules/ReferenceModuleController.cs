using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.ReferenceModules
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReferenceModuleController : BaseController<IReferenceModuleRepository, ReferenceModule>
    {
        public ReferenceModuleController(IReferenceModuleRepository repository) : base(repository)
        {

        }
    }
}