using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.CodeNsfs
{
    [Route("api/[controller]")]
    [ApiController]
    public class CodeNsfController : BaseController<ICodeNsfRepository, CodeNsf>
    {
        public CodeNsfController(ICodeNsfRepository repository) : base(repository)
        {

        }
    }
}
