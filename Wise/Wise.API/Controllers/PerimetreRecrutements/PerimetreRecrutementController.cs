using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.PerimetreRecrutements
{
    [Route("api/[controller]")]
    [ApiController]
    public class PerimetreRecrutementController : BaseController<IPerimetreRecrutementRepository, PerimetreRecrutement>
    {
        public PerimetreRecrutementController(IPerimetreRecrutementRepository repository) : base(repository)
        {

        }
    }
}