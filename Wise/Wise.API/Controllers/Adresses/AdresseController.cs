using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.Adresses
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdresseController : BaseController<IAdresseRepository, Adresse>
    {
        public AdresseController(IAdresseRepository repository) : base(repository)
        {

        }
    }
}
