using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.CodeFormacodes
{
    [Route("api/[controller]")]
    [ApiController]
    public class CodeFormacodeController : BaseController<ICodeFormacodeRepository, CodeFormacode>
    {
        public CodeFormacodeController(ICodeFormacodeRepository repository) : base(repository)
        {

        }
    }
}
