using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.Certifications
{
    [Route("api/[controller]")]
    [ApiController]
    public class CertificationController : BaseController<ICertificationRepository, Certification>
    {
        public CertificationController(ICertificationRepository repository) : base(repository)
        {

        }
    }
}
