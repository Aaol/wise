using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.WebUrlWebs
{
    [Route("api/[controller]")]
    [ApiController]
    public class WebUrlWebController : BaseController<IWebUrlWebRepository, WebUrlWeb>
    {
        public WebUrlWebController(IWebUrlWebRepository repository) : base(repository)
        {

        }
    }
}