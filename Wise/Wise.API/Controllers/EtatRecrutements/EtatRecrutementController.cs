using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.EtatRecrutements
{
    [Route("api/[controller]")]
    [ApiController]
    public class EtatRecrutementController : BaseController<IEtatRecrutementRepository, EtatRecrutement>
    {
        public EtatRecrutementController(IEtatRecrutementRepository repository) : base(repository)
        {

        }
    }
}
