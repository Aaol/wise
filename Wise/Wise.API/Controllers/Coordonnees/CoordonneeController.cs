using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.Coordonnees
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoordonneeController : BaseController<ICoordonneeRepository, Coordonnee>
    {
        public CoordonneeController(ICoordonneeRepository repository) : base(repository)
        {

        }
    }
}
