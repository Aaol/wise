﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.Actions
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActionController : BaseController<IActionRepository, Action>
    {
        public ActionController(IActionRepository repository) : base(repository)
        {

        }
    }
}
