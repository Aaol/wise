using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.FormationActions
{
    [Route("api/[controller]")]
    [ApiController]
    public class FormationActionController : BaseController<IFormationActionRepository, FormationAction>
    {
        public FormationActionController(IFormationActionRepository repository) : base(repository)
        {

        }
    }
}
