using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.CoordonneeLignes
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoordonneeLigneController : BaseController<ICoordonneeLigneRepository, CoordonneeLigne>
    {
        public CoordonneeLigneController(ICoordonneeLigneRepository repository) : base(repository)
        {

        }
    }
}
