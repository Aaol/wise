using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.AdresseLignes
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdresseLigneController : BaseController<IAdresseLigneRepository, AdresseLigne>
    {
        public AdresseLigneController(IAdresseLigneRepository repository) : base(repository)
        {

        }
    }
}
