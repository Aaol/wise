using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wise.API.Controllers
{
    /// <summary>
    ///     Classe de base pour l'envoi d'une réponse de l'API.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EntityResponse<T>
    {
        #region Properties

        /// <summary>
        ///     Index de départ des éléments fournis.
        /// </summary>
        public int? Start { get; set; }
        /// <summary>
        ///     Index de fin des éléments fournis.
        /// </summary>
        public int? End { get; set; }
        /// <summary>
        ///     Nombre maximum d'éléments possibles.
        /// </summary>
        public int? Max { get; set; }
        /// <summary>
        ///     Entités que l'on doit envoyer.
        /// </summary>
        public T Entries { get; set; }
        /// <summary>
        ///     Texte de l'erreur.
        /// </summary>
        public string Error { get; set; }
        /// <summary>
        ///     Indique si la requête s'est correctement déroulée.
        /// </summary>
        public bool Success { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initialise une nouvelle instance de la classe <see cref="EntityResponse{T}"/>
        /// </summary>
        public EntityResponse()
        {
            this.Success = true;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Méthode utilisée pour définir l'erreur dans la réponse.
        /// </summary>
        /// <param name="error"></param>
        public void AddError(string error)
        {
            this.Error = error;
            this.Success = false;
        }

        #endregion
    }
}
