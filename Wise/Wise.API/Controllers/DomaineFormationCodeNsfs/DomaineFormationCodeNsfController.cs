using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.DomaineFormationCodeNsfs
{
    [Route("api/[controller]")]
    [ApiController]
    public class DomaineFormationCodeNsfController : BaseController<IDomaineFormationCodeNsfRepository, DomaineFormationCodeNsf>
    {
        public DomaineFormationCodeNsfController(IDomaineFormationCodeNsfRepository repository) : base(repository)
        {

        }
    }
}
