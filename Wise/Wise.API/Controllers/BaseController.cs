using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;
using System.IO;
using Microsoft.AspNetCore.Authorization;

namespace Wise.API.Controllers
{
    /// <summary>
    ///     Classe de base pour les controleurs d'API Rest, contenant le BREAD classique pour la classe <see cref="U"/>.
    /// </summary>
    /// <typeparam name="T">Classe correspondant au Repository que l'on va utiliser. Le Repository doit implémenter l'interface <see cref="IBaseRepository{}"/></typeparam>
    /// <typeparam name="U">Classe correspondant au modèle que l'on va manipuler dans le controlleur.</typeparam>
    public abstract class BaseController<T, U> : ControllerBase
        where T : IBaseRepository<U>
        where U : new()
    {
        #region Properties

        /// <summary>
        ///     Repository pour l'entité du contrôleur.
        /// </summary>
        protected T Repository { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initialise une nouvelle instance de la classe <see cref="BaseController"/>.
        /// </summary>
        public BaseController(T repository)
        {
            this.Repository = repository;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Récupère l'entité de type <see cref="U"/>.
        /// </summary>
        /// <param name="id">Identifiant de l'entité.</param>
        /// <returns>Entité correspondant à l'identifiant fourni.</returns>
        [HttpGet("{id}")]
        public virtual ActionResult<EntityResponse<U>> GetEntity(int id)
        {
            EntityResponse<U> response = new EntityResponse<U>();
            try
            {
                response.Start = 0;
                response.End = 0;
                response.Entries = this.Repository.GetEntity(id);
            }
            catch (Exception ex)
            {
                response.AddError(ex.Message);
            }
            return SendResponse(response);
        }

        /// <summary>
        ///     Ajoute une entité de type <see cref="U"/> à la base de données.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost()]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
		[Authorize(Roles = "Gestionnaire, Administrateur")]
		public virtual ActionResult<U> AddEntity([FromBody]U entity)
        {
            try
            {
                U newEntity = this.Repository.AddEntity(entity);
                return StatusCode(201, newEntity);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        /// <summary>
        ///     Récupère une liste d'entité de type <see cref="U"/>.
        /// </summary>
        /// <param name="page">Numéro de la page.</param>
        /// <param name="number">Nombre d'éléments dans la page.</param>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult<EntityResponse<IEnumerable<U>>> GetEntities([FromQuery] int page = 1, [FromQuery] int number = 100)
        {
            EntityResponse<IEnumerable<U>> response = new EntityResponse<IEnumerable<U>>();
            try
            {
                if(number == -1)
                {
                    number = this.Repository.GetEntititiesCount();
                }
                response.Start = (page - 1) * number;
                response.Entries = this.Repository.GetEntities()
                    .Skip(response.Start.Value)
                    .Take(number)
                    .AsEnumerable();

                response.End = response.Entries.Count() - 1;
                response.Max = this.Repository.GetEntititiesCount();
            }
            catch (Exception ex)
            {
                response.AddError(ex.Message);
            }
            return SendResponse(response);
        }

        /// <summary>
        ///     Supprime l'entité sélectionné.s
        /// </summary>
        /// <param name="id">Identifiant de l'entité.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = "Gestionnaire, Administrateur")]
        public virtual ActionResult<EntityResponse<U>> DeleteEntity(int id)
        {
            EntityResponse<T> response = new EntityResponse<T>();
            try
            {
                this.Repository.DeleteEntity(id);
            }
            catch (Exception ex)
            {
                response.AddError(ex.Message);
            }
            return SendResponse(response);
        }

        /// <summary>
        ///     Met à jour l'entité fournie.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [Authorize(Roles = "Gestionnaire, Administrateur")]
        [HttpPut]
		public virtual ActionResult<EntityResponse<U>> UpdateEntity(U entity)
        {
            EntityResponse<U> response = new EntityResponse<U>();
            try
            {
                response.Entries = this.Repository.UpdateEntity(entity);
            }
            catch (Exception ex)
            {
                response.AddError(ex.Message);
            }
            return SendResponse(response);
        }

        /// <summary>
        ///     Méthode utilisé pour générer la réponse HTTP en fonction de l'objet <see cref="EntityResponse{T}"/>.
        /// </summary>
        /// <typeparam name="X">Type d'entité que l'on renvoie dans la réponse.</typeparam>
        /// <param name="response">Corps de la réponse.</param>
        /// <param name="errorStatusCode">Status code en cas d'erreur.</param>
        /// <param name="successStatusCode">Status code en cas de succès.</param>
        /// <returns></returns>
        protected ActionResult<EntityResponse<U>> SendResponse<X>(EntityResponse<X> response, int errorStatusCode = 400, int successStatusCode = 200)
        {
            if(!response.Success)
            {
                return StatusCode(errorStatusCode, response);
            }
            else
            {
                return StatusCode(successStatusCode, response);
            }
        }

        #endregion
    }
}
