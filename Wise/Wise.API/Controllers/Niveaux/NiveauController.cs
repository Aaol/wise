﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers
{
    [Route("api/niveaux")]
    [ApiController]
    public class NiveauController : BaseController<INiveauRepository, Niveaux>
    {
        public NiveauController(INiveauRepository repository) : base(repository)
        {
        }
    }
}
