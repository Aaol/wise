using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.WiseNetworkContexts
{
    [Route("api/[controller]")]
    [ApiController]
    public class WiseNetworkContextController : BaseController<IWiseNetworkContextRepository, WiseNetworkContext>
    {
        public WiseNetworkContextController(IWiseNetworkContextRepository repository) : base(repository)
        {

        }
    }
}