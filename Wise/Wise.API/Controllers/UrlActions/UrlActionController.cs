using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.UrlActions
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrlActionController : BaseController<IUrlActionRepository, UrlAction>
    {
        public UrlActionController(IUrlActionRepository repository) : base(repository)
        {

        }
    }
}