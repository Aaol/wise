using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.OrganismeFormateurs
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrganismeFormateurController : BaseController<IOrganismeFormateurRepository, OrganismeFormateur>
    {
        public OrganismeFormateurController(IOrganismeFormateurRepository repository) : base(repository)
        {

        }
    }
}