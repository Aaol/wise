using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.API.Repositories;
using Wise.DbLib.Models;

namespace Wise.API.Controllers.Extras
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExtraController : BaseController<IExtraRepository, Extra>
    {
        public ExtraController(IExtraRepository repository) : base(repository)
        {

        }
    }
}
