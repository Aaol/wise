using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Wise.API.Config;

namespace Wise.API.Services
{
    /// <summary>
    ///     Service d'envoi de mail
    /// </summary>
    public class MailService : IMailService
    {
        #region Properties

        public SMTPConfig Config { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initialise une nouvelle instance de la classe <see cref="MailService"/>
        /// </summary>
        /// <param name="option"></param>
        public MailService(IOptions<SMTPConfig> option)
        {
            this.Config = option.Value;
        }


        #endregion

        #region Methods

        public void SendMail(string subject, string body, string to, string from)
        {
            SmtpClient client = new SmtpClient(Config.Host, Config.Port);
            //client.UseDefaultCredentials = false;
            //client.Credentials = new NetworkCredential("username", "password");

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(from);
            mailMessage.To.Add(to);
            mailMessage.Body = body;
            mailMessage.Subject = subject;
            client.Send(mailMessage);
        }

        #endregion
    }
}
