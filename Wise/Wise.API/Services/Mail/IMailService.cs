using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wise.API.Services
{
    /// <summary>
    ///     Interface du service d'envoi de mail
    /// </summary>
    public interface IMailService
    {
        /// <summary>
        ///     Envoi d'un nouveau mail.
        /// </summary>
        /// <param name="subject">Sujet du mail</param>
        /// <param name="body">Corps du mail</param>
        /// <param name="to">Destinataire du mail</param>
        /// <param name="from">Expéditeur du mail</param>
        void SendMail(string subject, string body, string to, string from);
    }
}
