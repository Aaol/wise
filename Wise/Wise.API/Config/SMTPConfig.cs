using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wise.API.Config
{
    public class SMTPConfig
    {
        public string Host { get; set; }
        public int Port { get; set; }
    }
}
