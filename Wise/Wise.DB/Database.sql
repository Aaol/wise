
CREATE TABLE Offre
(
	id_offre int PRIMARY KEY Identity(1,1) NOT NULL
);

CREATE TABLE CodeNSF
(
	id_code_NSF int PRIMARY KEY Identity(1,1) NOT NULL,
	code char(3) NOT NULL
);

CREATE TABLE CodeFORMACODE
(
	id_code_FORMACODE int PRIMARY KEY Identity(1,1) NOT NULL,
	code char(5) NOT NULL
);

CREATE TABLE CodeROME
(
	id_code_ROME int PRIMARY KEY Identity(1,1) NOT NULL,
	code char(5) NOT NULL
);

CREATE TABLE Numtel
(
	id_numtel int PRIMARY KEY Identity(1,1) NOT NULL,
	numero Varchar(25) NOT NULL
);

CREATE TABLE Ligne
(
	id_ligne int PRIMARY KEY Identity(1,1) NOT NULL,
	ligne Varchar(50) NOT NULL
);

CREATE TABLE Geolocalisation
(
	id_geolocalisation int PRIMARY KEY Identity(1,1) NOT NULL,
	latitude Varchar(30) NOT NULL,
	longitude Varchar(30) NOT NULL
);

CREATE TABLE Web
(
	id_web int PRIMARY KEY Identity(1,1) NOT NULL
);

CREATE TABLE UrlFormation
(
	id_url_formation int PRIMARY KEY Identity(1,1) NOT NULL
);

CREATE TABLE UrlWeb
(
	id_url_web int PRIMARY KEY Identity(1,1) NOT NULL,
	url Varchar(400) NOT NULL
);

CREATE TABLE ParcoursDeFormation
(
	id_parcours_de_formation int PRIMARY KEY Identity(1,1) NOT NULL,
	libele Varchar(35) NOT NULL
);

CREATE TABLE Niveaux
(
	id_niveaux int PRIMARY KEY Identity(1,1) NOT NULL,
	libele Varchar(65) NOT NULL
);

CREATE TABLE ObjectifGeneralFormation
(
	id_objectif_general_formation int PRIMARY KEY Identity(1,1) NOT NULL,
	libele Varchar(70) NOT NULL
);

CREATE TABLE Certification
(
	id_certification int PRIMARY KEY Identity(1,1) NOT NULL,
	code_RNCP Varchar(6),
	code_CERTIFINFO Varchar(6)
);

CREATE TABLE Positionnement
(
	id_positionnement int PRIMARY KEY Identity(1,1) NOT NULL,
	libele Varchar(15)
);

CREATE TABLE SousModule
(
	id_sous_module int PRIMARY KEY Identity(1,1) NOT NULL
);

CREATE TABLE TypeModule
(
	id_type_module int PRIMARY KEY Identity(1,1) NOT NULL,
	libele Varchar(25)
);

CREATE TABLE ModulePrerequis
(
	id_modules_prerequis int PRIMARY KEY Identity(1,1) NOT NULL
);

CREATE TABLE ReferenceModule
(
	id_reference_module int PRIMARY KEY Identity(1,1) NOT NULL,
	reference Varchar(3000) NOT NULL
);

CREATE TABLE SIRET
(
	id_SIRET int PRIMARY KEY Identity(1,1) NOT NULL,
	SIRET char(14) NOT NULL
);

CREATE TABLE ModaliteEnseignement
(
  id_modalite_enseignement int PRIMARY KEY Identity(1,1) NOT NULL,
  label VARCHAR(50) NOT NULL,
  modalite_key INT NOT NULL
);

CREATE TABLE OrganismeFinanceur
(
  id_organisme_financeur int PRIMARY KEY Identity(1,1) NOT NULL,
  code_financeur int NOT NULL,
  nb_place_financees INT
);

CREATE TABLE UrlAction
(
  id_url_action int PRIMARY KEY Identity(1,1) NOT NULL
);

CREATE TABLE Periode
(
  id_periode INT PRIMARY KEY Identity(1,1) NOT NULL,
  debut DATE NOT NULL,
  fin DATE NOT NULL
);

CREATE TABLE EtatRecrutement
(
  id_etat_recrutement int PRIMARY KEY Identity(1,1) NOT NULL,
  label VARCHAR(50) NOT NULL,
  recrutement_key INT NOT NULL
);

CREATE TABLE Potentiel
(
	id_potentiel int PRIMARY KEY Identity(1,1) NOT NULL
);

CREATE TABLE Lheo
(
    id_lheo int PRIMARY KEY Identity(1,1) NOT NULL,
    id_offre int NOT NULL,
    CONSTRAINT fk_id_offre_lheo FOREIGN KEY (id_offre) REFERENCES Offre(id_offre)
);

CREATE TABLE DomaineFormation
(
	id_domaine_formation int PRIMARY KEY Identity(1,1) NOT NULL
);

CREATE TABLE Adresse
(
	id_adresse int PRIMARY KEY Identity(1,1) NOT NULL,
	codepostal char(5) NOT NULL,
	ville Varchar(50) NOT NULL,
	departement Varchar(3),
	code_INSEE_commune char(5),
	code_INSEE_canton Varchar(5),
	region char(2),
	pays char(2),
	id_geolocalisation int,
	CONSTRAINT fk_id_geolocalisation_adresse FOREIGN KEY (id_geolocalisation) REFERENCES Geolocalisation(id_geolocalisation)
);

CREATE TABLE ContactTel
(
	id_contact_tel int PRIMARY KEY Identity(1,1) NOT NULL,
	id_numtel int NOT NULL,
	CONSTRAINT fk_id_numtel_contact_tel FOREIGN KEY (id_numtel) REFERENCES Numtel(id_numtel)
);

CREATE TABLE Coordonnee
(
	id_coordonnee int PRIMARY KEY Identity(1,1) NOT NULL,
	civilite Varchar(50),
	nom Varchar(50),
	prenom Varchar(50),
	id_adresse int,
	CONSTRAINT fk_id_adresse_coordonnee FOREIGN KEY (id_adresse) REFERENCES Adresse(id_adresse),
	id_telfixe int NOT NULL,
	CONSTRAINT fk_id_telfixe_coordonnee FOREIGN KEY (id_telfixe) REFERENCES ContactTel(id_contact_tel),
	id_portable int NOT NULL,
	CONSTRAINT fk_id_portable_coordonnee FOREIGN KEY (id_portable) REFERENCES ContactTel(id_contact_tel),
	id_fax int NOT NULL,
	CONSTRAINT fk_id_fax_coordonnee FOREIGN KEY (id_fax) REFERENCES ContactTel(id_contact_tel),
	courriel Varchar(160),
	id_web int,
	CONSTRAINT fk_id_web_coordonnee FOREIGN KEY (id_web) REFERENCES Web(id_web)
);

CREATE TABLE ContactFormation
(
	id_contact_formation int PRIMARY KEY Identity(1,1) NOT NULL,
	id_coordonnee int NOT NULL,
	CONSTRAINT fk_id_coordonnee_contact_formation FOREIGN KEY (id_coordonnee) REFERENCES Coordonnee(id_coordonnee)
);


CREATE TABLE CoordonneeOrganisme
(
	id_coordonnee_organisme int PRIMARY KEY Identity(1,1) NOT NULL,
	id_coordonnee int NOT NULL,
	CONSTRAINT fk_id_coordonnee_coordonnee_organisme_coordonne FOREIGN KEY (id_coordonnee) REFERENCES Coordonnee(id_coordonnee)
);

CREATE TABLE ContactOrganisme
(
	id_contact_organisme int PRIMARY KEY Identity(1,1) NOT NULL,
	id_coordonnee int NOT NULL,
	CONSTRAINT fk_id_coordonnee_contact_organisme FOREIGN KEY (id_coordonnee) REFERENCES Coordonnee(id_coordonnee)
);

CREATE TABLE OrganismeFormationResponsable
(
	id_organisme_formation_responsable int PRIMARY KEY Identity(1,1) NOT NULL,
	numero_activite char(11) NOT NULL,
	id_SIRET int NOT NULL,
	CONSTRAINT fk_id_SIRET_organisme_formation_responsable FOREIGN KEY (id_SIRET) REFERENCES SIRET(id_SIRET),
	nom_organisme Varchar(250) NOT NULL,
	raison_sociale Varchar(250) NOT NULL,
	id_coordonnee_organisme int NOT NULL,
	CONSTRAINT fk_id_coordonnee_organisme_organisme_formation_responsable FOREIGN KEY (id_coordonnee_organisme) REFERENCES CoordonneeOrganisme(id_coordonnee_organisme),
	id_contact_organisme int NOT NULL,
	CONSTRAINT fk_id_contact_organisme_organisme_formation_responsable FOREIGN KEY (id_contact_organisme) REFERENCES ContactOrganisme(id_contact_organisme),
	renseignements_specifiques Varchar(3000),
	id_potentiel int,
	CONSTRAINT fk_id_potentiel_organisme_formation_responsable FOREIGN KEY (id_potentiel) REFERENCES Potentiel(id_potentiel)
);

CREATE TABLE Formation
(
	id_formation int PRIMARY KEY Identity(1,1) NOT NULL,
	id_domaine_formation int NOT NULL,
	CONSTRAINT fk_id_domaine_formation_formation FOREIGN KEY (id_domaine_formation) REFERENCES DomaineFormation(id_domaine_formation),
	intitule_formation Varchar(255) NOT NULL,
	objectif_formation Varchar(3000) NOT NULL,
	resultats_attendus Varchar(3000) NOT NULL,
	contenu_formation Varchar(3000) NOT NULL,
	certifiante bit NOT NULL,
	id_contact_formation int NOT NULL,
	CONSTRAINT fk_id_contact_formation_formation FOREIGN KEY (id_contact_formation) REFERENCES ContactFormation(id_contact_formation),
	id_parcours_de_formation int NOT NULL,
	CONSTRAINT fk_id_parcours_de_formation_formation FOREIGN KEY (id_parcours_de_formation) REFERENCES ParcoursDeFormation(id_parcours_de_formation),
	id_niveaux_entree int NOT NULL,
	CONSTRAINT fk_id_niveaux_entree_formation FOREIGN KEY (id_niveaux_entree) REFERENCES Niveaux(id_niveaux),
	id_objectif_general_formation int,
	CONSTRAINT fk_id_objectif_general_formation FOREIGN KEY (id_objectif_general_formation) REFERENCES ObjectifGeneralFormation(id_objectif_general_formation),
	id_code_niveau_sortie int,
	CONSTRAINT fk_id_code_niveau_sortie_formation FOREIGN KEY (id_code_niveau_sortie) REFERENCES Niveaux(id_niveaux),
	id_url_formation int,
	CONSTRAINT fk_id_url_formation_formation FOREIGN KEY (id_url_formation) REFERENCES UrlFormation(id_url_formation),
	id_organisme_formation_responsable int NOT NULL,
	CONSTRAINT fk_id_organisme_formation_responsable_formation FOREIGN KEY (id_organisme_formation_responsable) REFERENCES OrganismeFormationResponsable(id_organisme_formation_responsable),
	identifiant_module Varchar(3000),
	id_positionnement int,
	CONSTRAINT fk_id_positionnement_formation FOREIGN KEY (id_positionnement) REFERENCES Positionnement(id_positionnement),
	id_sous_module int,
	CONSTRAINT fk_id_sous_module_formation FOREIGN KEY (id_sous_module) REFERENCES SousModule(id_sous_module),
	id_modules_prerequis int,
	CONSTRAINT fk_id_modules_prerequis_formation FOREIGN KEY (id_modules_prerequis) REFERENCES ModulePrerequis(id_modules_prerequis)
);

CREATE TABLE OffreFormation
(
	id_offre int NOT NULL,
	CONSTRAINT fk_id_offre_offre_formation FOREIGN KEY (id_offre) REFERENCES Offre(id_offre),
	id_formation int NOT NULL,
	CONSTRAINT fk_id_formation_offre_formation FOREIGN KEY (id_formation) REFERENCES Formation(id_formation)
);

CREATE TABLE DomaineFormationCodeFORMACODE
(
	id_domaine_formation int NOT NULL,
	CONSTRAINT fk_id_domaine_formation_domaine_formation_code_FORMACODE FOREIGN KEY (id_domaine_formation) REFERENCES DomaineFormation(id_domaine_formation),
	id_code_FORMACODE int NOT NULL,
	CONSTRAINT fk_id_code_FORMACODE_domaine_formation_code_FORMACODE FOREIGN KEY (id_code_FORMACODE) REFERENCES CodeFORMACODE(id_code_FORMACODE)
);

CREATE TABLE DomaineFormationCodeNSF
(
	id_domaine_formation int NOT NULL,
	CONSTRAINT fk_id_domaine_formation_domaine_formation_code_NSF FOREIGN KEY (id_domaine_formation) REFERENCES DomaineFormation(id_domaine_formation),
	id_code_NSF int NOT NULL,
	CONSTRAINT fk_id_code_NSF_domaine_formation_code_NSF FOREIGN KEY (id_code_NSF) REFERENCES CodeNSF(id_code_NSF)
);

CREATE TABLE DomaineFormationCodeROME
(
	id_domaine_formation int NOT NULL,
	CONSTRAINT fk_id_domaine_formation_domaine_formation_code_ROME FOREIGN KEY (id_domaine_formation) REFERENCES DomaineFormation(id_domaine_formation),
	id_code_ROME int NOT NULL,
	CONSTRAINT fk_id_code_ROME_domaine_formation_code_ROME FOREIGN KEY (id_code_ROME) REFERENCES CodeROME(id_code_ROME)
);

CREATE TABLE CoordonneeLigne
(
	id_ligne int NOT NULL,
	CONSTRAINT fk_id_ligne_coordonnee_ligne FOREIGN KEY (id_ligne) REFERENCES Ligne(id_ligne),
	id_coordonnee int NOT NULL,
	CONSTRAINT fk_id_coordonnee_coordonnee_ligne FOREIGN KEY (id_coordonnee) REFERENCES Coordonnee(id_coordonnee)
);

CREATE TABLE AdresseLigne
(
	id_ligne int NOT NULL,
	CONSTRAINT fk_id_ligne_adresse_ligne FOREIGN KEY (id_ligne) REFERENCES Ligne(id_ligne),
	id_adresse int NOT NULL,
	CONSTRAINT fk_id_adresse_adresse_ligne FOREIGN KEY (id_adresse) REFERENCES Adresse(id_adresse)
);

CREATE TABLE WebUrlWeb
(
	id_web int NOT NULL,
	CONSTRAINT fk_id_web_web_url_web FOREIGN KEY (id_web) REFERENCES Web(id_web),
	id_url_web int NOT NULL,
	CONSTRAINT fk_id_url_web_web_url_web FOREIGN KEY (id_url_web) REFERENCES UrlWeb(id_url_web)
);

CREATE TABLE UrlFormationUrlWeb
(
	id_url_formation int NOT NULL,
	CONSTRAINT fk_id_url_formation_url_formation_url_web FOREIGN KEY (id_url_formation) REFERENCES UrlFormation(id_url_formation),
	id_url_web int NOT NULL,
	CONSTRAINT fk_id_url_web_url_formation_url_web FOREIGN KEY (id_url_web) REFERENCES UrlWeb(id_url_web)
);

CREATE TABLE FormationCertification
(
	id_formation int NOT NULL,
	CONSTRAINT fk_id_formation_formation_certification FOREIGN KEY (id_formation) REFERENCES Formation(id_formation),
	id_certification int NOT NULL,
	CONSTRAINT fk_id_certification_formation_certification FOREIGN KEY (id_certification) REFERENCES Certification(id_certification)
);

CREATE TABLE LieuDeFormation
(
  id_lieu_de_formation int PRIMARY KEY Identity(1,1) NOT NULL,
  id_coordonnee int NOT NULL,
  CONSTRAINT fk_id_coordonnee_lieu_de_formation FOREIGN KEY (id_coordonnee) REFERENCES Coordonnee(id_coordonnee)
);

CREATE TABLE AdresseInformation
(
  id_adresse_information INT PRIMARY KEY  Identity(1,1) NOT NULL,
  id_adresse INT NOT NULL,
  CONSTRAINT fk_adresse_information_id_adresse FOREIGN KEY (id_adresse) REFERENCES Adresse(id_adresse)
);

CREATE TABLE PerimetreRecrutement
(
  id_perimetre_recrutement INT PRIMARY KEY Identity(1,1) NOT NULL,
  label VARCHAR(100) NOT NULL,
  cle int NOT NULL,
  CONSTRAINT UK_perimetre_recrutement_cle UNIQUE (cle)
);

CREATE TABLE ContactFormateur
(
	id_contact_formateur int PRIMARY KEY Identity(1,1) NOT NULL,
	id_coordonnee int NOT NULL,
	CONSTRAINT fk_id_coordonnee_contact_formateur FOREIGN KEY (id_coordonnee) REFERENCES Coordonnee(id_coordonnee)
);

CREATE TABLE OrganismeFormateur
(
  id_organisme_formateur int PRIMARY KEY Identity(1,1) NOT NULL,
  id_siret_formation INT NOT NULL,
  CONSTRAINT fk_organisme_formateur FOREIGN KEY (id_siret_formation) REFERENCES SIRET(id_SIRET),
  raison_sociale_formateur VARCHAR(200) NOT NULL,
  id_contact_formateur int,
  CONSTRAINT fk_id_contact_formateur_organisme_formateur FOREIGN KEY (id_contact_formateur) REFERENCES ContactFormateur(id_contact_formateur),
  id_potentiel int,
  CONSTRAINT fk_id_potentiel_organisme_formateur FOREIGN KEY (id_potentiel) REFERENCES Potentiel(id_potentiel)
);

CREATE TABLE _Action
(
  id_action int PRIMARY KEY Identity(1,1) NOT NULL,
  rythme_formation VARCHAR(3000) NOT NULL,
  info_public_vise VARCHAR(250) NULL,
  niveau_entree_obligatoire BIT NOT NULL,
  modalite_alternance VARCHAR(3000),
  id_modalite_enseignement INT NOT NULL,
  CONSTRAINT fk_modalite_enseignement_action FOREIGN KEY (id_modalite_enseignement) REFERENCES ModaliteEnseignement(id_modalite_enseignement),
  condition_specifiques VARCHAR(3000) NOT NULL,
  prise_en_charge_frais_possible BIT NOT NULL,
  id_lieu_de_formation INT NOT NULL,
  CONSTRAINT fk_id_lieu_de_formation_action FOREIGN KEY (id_lieu_de_formation) REFERENCES LieuDeFormation(id_lieu_de_formation),
  modalites_entrees_sorties BIT NOT NULL,
  id_url_action INT NULL,
  CONSTRAINT fk_id_url_action_action FOREIGN KEY (id_url_action) REFERENCES Web(id_web),
	id_adresse_information int,
  CONSTRAINT fk_action_id_adresse_information FOREIGN KEY (id_adresse_information) REFERENCES AdresseInformation(id_adresse_information),
  restauration VARCHAR(250),
  hebergement VARCHAR(250),
  transport VARCHAR(250),
  acces_handicape VARCHAR(250),
  lange_formation CHAR(2),
  modalite_recrutement VARCHAR(3000),
  modalite_pedagogiques VARCHAR(200),
  frais_restant VARCHAR(200),
  code_perimetre_recrutement INT,
  CONSTRAINT fk_code_perimetre_recrutement_action FOREIGN KEY (code_perimetre_recrutement) REFERENCES PerimetreRecrutement(cle),
  info_perimetre_recrutement Varchar(50),
  prix_horaire_ttc DECIMAL(18,2),
  prix_total_ttc DECIMAL(18,2),
  duree_indicative VARCHAR(200),
  nombre_heures_centre INT,
  nombre_heures_enterprise INT,
  nombre_heures_total INT,
  detail_condition_prise_en_charge VARCHAR(600),
  conventionnement BIT,
  duree_conventionee INT,
  id_organisme_formateur INT,
  CONSTRAINT fk_id_organisme_formateur_action FOREIGN KEY (id_organisme_formateur) REFERENCES OrganismeFormateur(id_organisme_formateur)
);

CREATE TABLE FormationAction
(
	id_formation int NOT NULL,
	CONSTRAINT fk_id_formation_formation_action FOREIGN KEY (id_formation) REFERENCES Formation(id_formation),
	id_action int NOT NULL,
	CONSTRAINT fk_id_action_formation_action FOREIGN KEY (id_action) REFERENCES _Action(id_action)
);

CREATE TABLE Module
(
	id_module int PRIMARY KEY Identity(1,1) NOT NULL,
	reference_module Varchar(3000) NOT NULL,
	id_type_module int NOT NULL,
	CONSTRAINT fk_id_type_module_module FOREIGN KEY (id_type_module) REFERENCES TypeModule(id_type_module)
);

CREATE TABLE SousModuleModule
(
	id_sous_module int NOT NULL,
	CONSTRAINT fk_id_sous_module_sous_module_module FOREIGN KEY (id_sous_module) REFERENCES SousModule(id_sous_module),
	id_module int NOT NULL,
	CONSTRAINT fk_id_module_sous_module_module FOREIGN KEY (id_module) REFERENCES Module(id_module)
);

CREATE TABLE ModulePrerequisReferenceModule
(
	id_modules_prerequis int NOT NULL,
	CONSTRAINT fk_id_modules_prerequis_module_prerequis_reference_module FOREIGN KEY (id_modules_prerequis) REFERENCES ModulePrerequis(id_modules_prerequis),
	id_reference_module int NOT NULL,
	CONSTRAINT fk_id_reference_module_module_prerequis_reference_module FOREIGN KEY (id_reference_module) REFERENCES ReferenceModule(id_reference_module)
);

CREATE TABLE PotentielCodeFORMACODE
(
	id_potentiel int NOT NULL,
	CONSTRAINT fk_id_potentiel_potentiel_code_FORMACODE FOREIGN KEY (id_potentiel) REFERENCES Potentiel(id_potentiel),
	id_code_FORMACODE int NOT NULL,
	CONSTRAINT fk_id_code_FORMACODE_potentiel_code_FORMACODE FOREIGN KEY (id_code_FORMACODE) REFERENCES CodeFORMACODE(id_code_FORMACODE)
);

CREATE TABLE CodePublicVise
(
  id_code_public_vise int PRIMARY KEY Identity(1,1) NOT NULL,
  id_action INT NOT NULL,
  CONSTRAINT fk_code_public_vise_id_action FOREIGN KEY (id_action) REFERENCES _Action(id_action),
  code char(5) NOT NULL
);

CREATE TABLE UrlActionUrlWeb
(
	id_url_action int NOT NULL,
  	CONSTRAINT fk_id_url_action_url_action_url_web FOREIGN KEY (id_url_action) REFERENCES UrlAction(id_url_action),
	id_url_web int NOT NULL,
  	CONSTRAINT fk_id_url_web_url_action_url_web FOREIGN KEY (id_url_web) REFERENCES UrlWeb(id_url_web)
);

CREATE TABLE _Session
(
  id_session INT PRIMARY KEY Identity(1,1) NOT NULL,
  id_action int NOT NULL,
  CONSTRAINT fk_id_action_session FOREIGN KEY (id_action) REFERENCES _Action(id_action),
  id_periode INT NOT NULL,
  CONSTRAINT fk_id_periode_session FOREIGN KEY (id_periode) REFERENCES Periode(id_periode),
  id_adresse_inscription INT NOT NULL,
  CONSTRAINT fk_id_adresse_inscription_session FOREIGN KEY (id_adresse_inscription) REFERENCES Adresse(id_adresse),
  modalite_inscription VARCHAR(255) NOT NULL,
  id_periode_inscription INT,
  CONSTRAINT fk_id_periode_inscription_session FOREIGN KEY (id_periode_inscription) REFERENCES Periode(id_periode),
  id_etat_recrutement INT,
  CONSTRAINT fk_id_etat_recrutement_session FOREIGN KEY (id_etat_recrutement) REFERENCES EtatRecrutement(id_etat_recrutement)
);

CREATE TABLE AdresseInscription
(
  id_adresse_inscription INT PRIMARY KEY Identity(1,1) NOT NULL,
  id_adresse int NOT NULL,
  CONSTRAINT fk_id_adresse_adresse_inscription FOREIGN KEY (id_adresse) REFERENCES Adresse(id_adresse)
);

CREATE TABLE DateInformation
(
  id_date_information INT PRIMARY KEY Identity(1,1) NOT NULL,
  date_information DATE NOT NULL,
  id_action INT NOT NULL,
  CONSTRAINT fk_date_information_id_action FOREIGN KEY (id_action) REFERENCES _Action(id_action)
);

CREATE TABLE ModalitePedagogique
(
  id_modalite_pedagogique int PRIMARY KEY Identity(1,1) NOT NULL,
  id_action INT NOT NULL,
  CONSTRAINT fk_id_action_modalite_information FOREIGN KEY (id_action) REFERENCES _Action(id_action),
  label CHAR(5) NOT NULL
);

CREATE TABLE ActionOrganismeFinanceur
(
  id_action_organisme_financeur INT PRIMARY KEY Identity(1,1) NOT NULL,
  id_action INT NOT NULL,
  id_organisme_financeur INT NOT NULL,
  CONSTRAINT fk_action_id_organisme_financeur FOREIGN KEY (id_organisme_financeur) REFERENCES OrganismeFinanceur(id_organisme_financeur),
  CONSTRAINT fk_action_id_action FOREIGN KEY (id_action) REFERENCES _Action(id_action)
);

CREATE TABLE Extra
(
	id_extra int PRIMARY KEY Identity(1,1) NOT NULL,
	id_lheo int,
    CONSTRAINT fk_id_lheo_extra FOREIGN KEY (id_lheo) REFERENCES Lheo(id_lheo),
	id_offre int,
    CONSTRAINT fk_id_offre_extra FOREIGN KEY (id_offre) REFERENCES Offre(id_offre),
	id_formation int,
    CONSTRAINT fk_id_formation_extra FOREIGN KEY (id_formation) REFERENCES Formation(id_formation),
	id_domaine_formation int,
	CONSTRAINT fk_id_domaine_formation_extra FOREIGN KEY (id_domaine_formation) REFERENCES DomaineFormation(id_domaine_formation),
	id_contact_formation int,
	CONSTRAINT fk_id_contact_formation_extra FOREIGN KEY (id_contact_formation) REFERENCES ContactFormation(id_contact_formation),
	id_coordonnee int,
	CONSTRAINT fk_id_coordonnee_extra FOREIGN KEY (id_coordonnee) REFERENCES Coordonnee(id_coordonnee),
	id_adresse int,
	CONSTRAINT fk_id_adresse_extra FOREIGN KEY (id_adresse) REFERENCES Adresse(id_adresse),
	id_geolocalisation int,
	CONSTRAINT fk_id_geolocalisation_extra FOREIGN KEY (id_geolocalisation) REFERENCES Geolocalisation(id_geolocalisation),
	id_web int,
	CONSTRAINT fk_id_web_extra FOREIGN KEY (id_web) REFERENCES Web(id_web),
	id_certification int,
	CONSTRAINT fk_id_certification_extra FOREIGN KEY (id_certification) REFERENCES Certification(id_certification),
	id_sous_module int,
	CONSTRAINT fk_id_sous_module_extra FOREIGN KEY (id_sous_module) REFERENCES SousModule(id_sous_module),
	id_module int,
	CONSTRAINT fk_id_module_extra FOREIGN KEY (id_module) REFERENCES Module(id_module),
	id_modules_prerequis int,
	CONSTRAINT fk_id_modules_prerequis_extra FOREIGN KEY (id_modules_prerequis) REFERENCES ModulePrerequis(id_modules_prerequis),
	id_organisme_formation_responsable int,
	CONSTRAINT fk_id_organisme_formation_responsable_extra FOREIGN KEY (id_organisme_formation_responsable) REFERENCES OrganismeFormationResponsable(id_organisme_formation_responsable),
	id_SIRET int,
	CONSTRAINT fk_id_SIRET_extra FOREIGN KEY (id_SIRET) REFERENCES SIRET(id_SIRET),
	id_coordonnee_organisme int,
	CONSTRAINT fk_id_coordonnee_organisme_extra FOREIGN KEY (id_coordonnee_organisme) REFERENCES CoordonneeOrganisme(id_coordonnee_organisme),
	id_contact_organisme int,
	CONSTRAINT fk_id_contact_organisme_extra FOREIGN KEY (id_contact_organisme) REFERENCES ContactOrganisme(id_contact_organisme),
	id_potentiel int,
	CONSTRAINT fk_id_potentiel_extra FOREIGN KEY (id_potentiel) REFERENCES Potentiel(id_potentiel),
	id_action int,
	CONSTRAINT fk_id_action_extra FOREIGN KEY (id_action) REFERENCES _Action(id_action),
	id_url_action int,
	CONSTRAINT fk_id_url_action_extra FOREIGN KEY (id_url_action) REFERENCES UrlAction(id_url_action),
	id_periode int,
	CONSTRAINT fk_id_periode_extra FOREIGN KEY (id_periode) REFERENCES Periode(id_periode),
	id_adresse_inscription int,
	CONSTRAINT fk_id_adresse_inscription_extra FOREIGN KEY (id_adresse_inscription) REFERENCES AdresseInscription(id_adresse_inscription),
	id_date_information int,
	CONSTRAINT fk_id_date_information_extra FOREIGN KEY (id_date_information) REFERENCES DateInformation(id_date_information),
	id_organisme_formateur int,
	CONSTRAINT fk_id_organisme_formateur_extra FOREIGN KEY (id_organisme_formateur) REFERENCES OrganismeFormateur(id_organisme_formateur),
	id_contact_formateur int,
	CONSTRAINT fk_id_contact_formateur_extra FOREIGN KEY (id_contact_formateur) REFERENCES ContactFormateur(id_contact_formateur),
	id_organisme_financeur int,
	CONSTRAINT fk_id_organisme_financeur_extra FOREIGN KEY (id_organisme_financeur) REFERENCES OrganismeFinanceur(id_organisme_financeur),
	commentaire Varchar(300) NOT NULL
);







